<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../head.jsp" />
	<script src="<c:url value="/resources/js/application/employee.js"/>" type="text/javascript"></script>  
	<script src="<c:url value="/resources/js/application/applicationutils.js"/>" type="text/javascript"></script>  
</head>
	<body class="layout-fixed  ">
		<div id="wrapper">
			<jsp:include page="../header.jsp" />
		</div>
		<div class="content-page">
			<div class="container-fluid">		
				<div class="row">
					<div class="col-sm-12 col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">
						<h2 class="page-header">Confirmation</h2>	
						Agent has being successfully being assigned to Insured
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">
						<c:url value="/customer/viewProspectiveProfile"  var="prospectProfile">
							<c:param name="prospectiveInt" value="${prospectiveId }"/>	
						</c:url>
						<a href="${prospectProfile}" class="btn btn-standard">View Prospective's Profile</a>
						
					</div>
				</div>				
			</div>
		</div>		
	</body>
</html>
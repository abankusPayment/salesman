<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../head.jsp" />
	<script src="<c:url value="/resources/js/application/lead.js"/>" type="text/javascript"></script>
</head>
<body class="layout-fixed  ">
	<div id="wrapper">
		<jsp:include page="../header.jsp" />
		<div class="content-page">	
			<div class="page-header-container">
				<h2>Sales Lookup</h2>
			</div>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label title="Select the Current status of Prospect">Current State:</label>
								<select id="currentState" class="form-control">
									<option>Select status of Prospect</option>
									<option value="initiated" selected>Initiated/Saved</option>
									<option value="assigned">Assigned to Insurance Agent</option>
									<option value="lead">Lead</option>
									<option value="completed">Completed Contacted</option>
								</select>
							</div>
							<input type="submit" class="btn btn-primary" value="Search" id="btnSearchProspEmp"/>
						</div>
						<div class="col-md-8">
							<div class="table-responsive">
								<table class="table table-hover table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Prospective Full Name</th>
											<th>Address</th>
											<th>Contact Information</th>
											<th>Date Of Enquiry</th>
											<th>Action</th>
										</tr>
									</thead>
										<tbody id="salesDivContainer">
											<tr>
												
											</tr>
										</tbody>
								</table>
							</div>						
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="employeeId" id="employeeId" value="${employeeId }"/>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		$('#salesDivContainer').html(populateErrorMessage("No Results found"));
		$('#btnSearchProspEmp').on('click',function(){
			var empId = document.getElementById("employeeId").value;
			var currentState = document.getElementById("currentState").value;
			$('#salesDivContainer').text("Loading....");
			getProspectBySalesRep(empId,currentState);			
		});

	});
</script>
</html>
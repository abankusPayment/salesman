<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../head.jsp" />
script src="<c:url value="/resources/js/application/assignAgent.js"/>" type="text/javascript"></script>
</head>
<body class="layout-fixed  ">
	<div id="wrapper">
		<jsp:include page="../header.jsp" />
		<div class="content-page">	
			<div class="page-header-container">
				<h2>Prospective Customer</h2>
			</div>
			<div class="content">
				<div class="container-fluid">	
				<div class="row">
				<div class="portlet portlet-boxed">

            <div class="portlet-header">
              <h4 class="portlet-title">
                New Insurance Proposal
              </h4>
            </div> <!-- /.portlet-header -->

            <div class="portlet-body" style="min-height: 400px;">
					<div class="col-md-12 col-lg-10">
						
						<div id="lead-container"></div>
						<c:url value="/sales/saveNewProspect" var="startProposal" />
						<sf:form action="${startProposal}" method="post"
							id="newProposalForm" name="newProposalForm">
							<fieldset>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label for="typeOfInsurance"> Type of Insurance </label> <select
												id="typeOfInsurance" name="typeOfInsurance"
												class="form-control">
												<option value="">Make a Selection</option>
												<option value="life">Life Insurance</option>
												<option value="auto">Auto Insurance</option>
												<option value="home">Home Insurance</option>
											</select>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset>
								<legend>Personal Information</legend>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label for="firstname">First name:</label> <input type="text"
												name="txtfirstname" id="txtfirstname" class="form-control" />
										</div>
										<div class="col-md-6">
											<label for="lastname">Last name:</label> <input type="text"
												name="txtlastname" id="txtlastname" class="form-control" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label for="selGender">Gender</label> <select id="leadGender"
												name="leadGender" class="form-control">
												<option value="">Select a Gender</option>
												<option value="male">Male</option>
												<option value="female">Female</option>
											</select>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">		
										<fieldset>
											<legend>Mailing Address</legend>
												<div class="bold">
													<b>Is mailing address same as Residential? </b>
												</div>
												<label class="radio-inline"> 
													<input type="radio" name="isAddressSame" id="isAddressSame" value="yes" onClick="javascript:toggleHideShowDivContainer('residentialContainer','hide');">&nbsp;&nbsp;Yes 
												</label>
											
												<label class="radio-inline"> 
													<input type="radio" name="isAddressSame" id="isAddressSame" value="no" onClick="javascript:toggleHideShowDivContainer('residentialContainer','show');">&nbsp;&nbsp;No 
												</label>
											
											<input type="hidden" name="addressType" id="addressType" value="mailing" />
											<div class="form-group">
												<label for="firstname">Address 1:</label> <input type="text"
													name="txtaddress1" id="txtaddress1" class="form-control" /> <br>
												<label for="firstname">Address 2: <span
													class="help-text">Optional</span></label> <input type="text"
													name="txtaddress2" id="txtaddress1Optional" class="form-control" />
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-md-6">
														<label for="txtfirstname">City:</label> <input type="text"
															name="txtcity" id="txtcity1" class="form-control" />
													</div>
													<div class="col-md-6">
														<label for="txtregion">Region:</label>
														<script type="text/javascript">createRegionList();</script>
													</div>
												</div>
											</div>
										</fieldset>									
									</div>
									<div class="col-md-6 disable" id="residentialContainer">		
										<fieldset>
											<legend>Residence Address &nbsp;</legend>
											<input type="hidden" name="address2" id="address2" value="residential" />
											<span class="help-block">Do not accept P.O. Box</span>
											<div class="form-group">
												<label for="firstname">Address 1:</label> <input type="text"
													name="txtaddress1" id="txtaddress2" class="form-control resident" /> <br>
												<label for="firstname">Address 2: <span
													class="help-text">Optional</span></label> <input type="text"
													name="txtaddress2" id="txtaddress2Optional" class="form-control resident" />
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-md-6">
														<label for="txtfirstname">City:</label> <input type="text"
															name="txtcity" id="txtcity2" class="form-control resident" />
													</div>
													<div class="col-md-6">
														<label for="txtregion">Region:</label>
														<script type="text/javascript">createRegionList("region2");</script>
													</div>
												</div>
											</div>
										</fieldset>									
									</div>									
								</div>						

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12 col-md-6 col-lg-5">
											<div class="form-group  ">
											<label for="txtphoneNumber">Phone Number:</label> <input
												type="text" name="txtphoneNumber" id="txtphoneNumber"
												class="form-control" />
											</div>
											<div class="radio">
												<label> 
													<input type="radio" name="phoneType" id="phoneTypeHome" value="home">&nbsp;&nbsp;Home Phone 
												</label>
											</div>
												<div class="radio">
												 <label> 
													<input type="radio" name="phoneType" id="phoneTypeWork"
														value="work">&nbsp;&nbsp;Work Phone </label>
												</div>
												<div class="radio">
												<label for="phoneTypeMobile">
													<input type="radio" name="phoneType" 
														id="phoneTypeMobile" value="mobile">&nbsp;&nbsp; Mobile Phone </label>
												</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-sm-12 col-md-6 col-lg-5">
											<label for="txtemailAddress">Email Address:</label> <input
												type="email" name="txtEmailAddress" id="txtemailAddress"
												class="form-control" />
												<div id="root"></div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</fieldset>
							<div>
								<input type="hidden" name="navigator" value=""/>
								<input type="submit" class="btn btn-success" id="btnSubmitContinue"	value="Save and Continue" />
								<input type="submit" class="btn btn-success" id="btnSubmitClose"	value="Save and Close" />
							</div>
						</sf:form>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<script src="<c:url value="/resources/js/application/lead.js"/> "></script>

</body>
</html>
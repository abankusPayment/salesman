<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>

<jsp:include page="../head.jsp" />
	<script src="<c:url value="/resources/js/application/assignProspect.js"/>" type="text/javascript"></script>
</head>
<body class="layout-fixed  ">
	<div id="wrapper">
		<jsp:include page="../header.jsp" />
		<div class="content-page">
		    <div class="content">		
			    <div class="container">	
			     	<div class="portlet portlet-default">
			     	<c:choose>
						<c:when test="${not empty prospect && prospect.isHasAssignedAgent() eq false}">
				            <div class="portlet-header">
				              <h4 class="portlet-title">
				                <strong>${prospect.getFullname() }</strong> Information ${prospect.isHasAssignedAgent()}
				              </h4>
				            </div> <!-- /.portlet-header -->
				            
				            <div class="portlet-body">
						        <div class="row">
									<div class="col-sm-12 col-md-6 col-lg-6">	
						                <table class="table">
						                  <tbody>
						                    <tr>
						                      <th>Full Name</th>
						                      <td>${prospect.getFullname() }</td>
						                    </tr>
						
						                    <tr>
						                      <th>Insurance</th>
						                      <td>
												${prospect.getInsuranceType()}
											  </td>
						                    </tr>
						
						                    <tr>
						                      <th>Date Enquired</th>
						                      <td>${prospect.getDateEnquired()}</td>
						                    </tr>
						                  </tbody>
						                </table>					
						            </div>
									<div class="col-sm-12 col-md-6 col-lg-6">
						            	<div class="form-group">
						            		<label> Prospective Customer Location</label>
						            		<div id="searchTextBtn">
						            			<span class="help-block"><small>Enter the City and Region in Ghana</small></span>
						            			<input type="text" id="propsLocation" placeholder="Enter the City and Region in Ghana" name="propsLocation" class="form-control searchText" value="${prospect.getAddressLocation() }"/>
						            			<span class="btn btn-sm btn-success" class="searchBtn" id="locateBtn"><i class="glyphicon glyphicon-search"></i> Search</span>
						            		</div>
						            	</div>					
						            </div>					            					
						          </div>
						          <div class="clearfix"></div>	
						          <div class="col-md-12" id="search-container">
							          <div id="searchResult" class="searchResult">
							          
							          </div>
						          </div>					          			
						       </div> 			            	
				            </div>
			            </div>
			             <c:url value="${prospect.prospectiveId }" var="prospectiveId"/>
			             <c:url value="/sales/saveAssignProspect" var="saveSelectedAgent"/>
			            <sf:form method="post" action="${ saveSelectedAgent}" name="agentProspectiveForm" id="agentProspectiveForm">
			            	<input type="hidden" name="prospectiveId" id="prospectiveId" value="${prospectiveId }" />
			            	<input type="hidden" name="agentId" id="agentId" value="" />
			            </sf:form>
		            </c:when>
		            <c:when test="${not empty prospect && prospect.isHasAssignedAgent() eq true}">
			            <c:url value="/customer/viewProspectiveProfile/${prospect.getProspectiveUniqueId() }" var="viewProspectiveProfile"/>	   
			            
			            <div>
			            <p><strong>${prospect.getFullname() }</strong> has already being assigned an Insurance Agent</p>
			            <address>
			            	<strong>${prospect.getAssignedAgent().getAgentName() }</strong><br/>
			            	<strong>${prospect.getAssignedAgent() }</strong><br/>
			            	<strong>${prospect.getAssignedAgent().getAgentAddress().getAddress1() }</strong><br/>
			            	<strong>${prospect.getAssignedAgent().getAgentAddress().getCity() }</strong>&nbsp;<strong>${prospect.getAssignedAgent().getAgentAddress().getRegion() }</strong><br/>
			            	<abbr title="Phone">P:</abbr> (123) 456-7890
			            </address>
			            <p>Click here to view Customer Profile <a href="${viewProspectiveProfile }">${prospect.getFullname() }</a></p>
			            </div>   
		            </c:when>
		            </c:choose>
		            <c:if test="${not empty errorMessage }">
		            	${errorMessage }
		            
		            </c:if>
			    </div>
			    
		    </div>		
		</div>
		
</div>
<script>
	$(document).ready(function(){
		$('#locateBtn').click(function(){
			const location = $('#propsLocation').val();
			const result = retrieveAgentsByLocation(location,'${saveSelectedAgent}','${prospectiveId}');	
			//populateResults(result);
		});

	});
	
	
</script>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Kejetia Sales Pipeline - Activate Account</title>
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet"/>
<link href="<c:url value="/resources/css/mvpready-admin.css"/>" rel="stylesheet">
<script src="<c:url value="/resources/js/jquery.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />" type="text/javascript"></script>
<script	src="<c:url value="/resources/js/validation/jquery.validation.js" />" type="text/javascript"></script>

</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<c:url value="/salesman/index" />">
         Kejetia Sales Pipeline &reg;
      </a>
    </div>
  </div>
</nav>
	<div id="container" class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
				<div class="page-header">
					<h1>Activate your Account</h1>
				</div>
				<c:if test="${success_message  != null}">
					<div class="alert alert-success">
						${success_message}
					</div>
				</c:if>		
				<c:if test="${errorMessage != null}">
					<div class="platform-alert warning">
						${errorMessage}
					</div>
				</c:if>		
				<c:choose>
					<c:when test="${requestType eq 'activateEmployee'}" >
						<c:url value="/company/activateEmployee" var="activateEmployeeUrl" />
						<sf:form name="changepassword" id="changepassword" action="${activateEmployeeUrl}" method="post">	
							<div>	
								<label class="">Confirmation Code:</label>
								<input type="text" class="form-control width-100" id="authCode" autocomplete="off" name="authCode" />
							</div>		
							<div>	
								<label class="">Username:</label>
								<input type="text" class="form-control width-100" id="username"  autocomplete="off" name="username" />
							</div>								
							<div>
								<label class="">New Password:</label>
								<input type="password" class="form-control  width-100" id="newpasswd" name="newpasswd" />	
							</div>	
							<div>
								<label class="">Confirm New Password:</label>
								<input type="password" class="form-control  width-100" id="confirmpasswd" name="confirmpasswd" />	
							</div>										
						</sf:form>	
							<div class="clearfix"></div>
							<hr>
							<input type="submit" name="btnChangePassword" id="btnChangePassword"  class="btn btn-success moveR_20" value="Reset Password">
							<a href="<c:url value="/security/login"/>" id="btnCancel">Cancel</a>														
							</c:when>	
					<c:otherwise>
					<div>
						If your are seeing this message if means
					</div>										
					</c:otherwise>			
				</c:choose>
			</div>
		</div>
	</div>
</body>
<script>
$(document).ready(function(){
	$('#btnChangePassword').click(function(){
		if(validateForm()){
			document.changepassword.submit();
		}else{
			return false;
		}
	});
});

function validateForm(){
	try{
		$('form').validate({
			onsubmit:false,
			rules :{
				username: {
					required: true
				},
				newpasswd: {
					required: true,
				},
				confirmpasswd : {
					required: true,
					equalTo: '#newpasswd'
				},
				authCode: {
					required: true,
				}
			},
			messages: {
				username : {
					required: "Username is a required field."
				},
				newpasswd: {
					required: "Password is a required field."
				},
				authCode : {
					required: "Enter the confirmation code your recieved."
				},
				confirmpasswd : {
					required: "Confirm your Password.",
					equalTo: "Both password do not match"
				}
			}
		});
		if(!$('form').valid())
			 return false;
		 return true;
		 
		}catch(err){
			console.log(err)
		}
}
</script>
</html>
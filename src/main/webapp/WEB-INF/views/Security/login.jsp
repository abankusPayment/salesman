<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> Kejetia | Sales Pipeline :: Login</title>

    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/font-awesome/css/font-awesome.css"/>" rel="stylesheet">

    <link href="<c:url value="/resources/css/mvpready-admin.css"/>" rel="stylesheet">
    	<script src='<c:url value="/resources/js/jquery.min.js" />' type="text/javascript"></script>
    	<script src='<c:url value="/resources/js/bootstrap.min.js" />' type="text/javascript"></script>
	<script src='<c:url value="/resources/js/validation/bootstrapValidator.js" />' type="text/javascript"></script>
	<script src='<c:url value="/resources/js/application/login.js" />' type="text/javascript"></script>
</head>

<body  class="account-bg">
  <header class="navbar" role="banner">

    <div class="container">

      <div class="navbar-header">
	      <a class="navbar-brand" href="#">
	        ProSalesman
	      </a>     
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          ProSalesman
        </button>
      </div> <!-- /.navbar-header -->

    </div> <!-- /.container -->

  </header>


  <br class="xs-80">
  <div class="row">
  	<div class="col-md-6 col-md-offset-3">
  		<c:if test="${not empty accessdenied}">        
			<div class="alert-danger alert">
			<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
				${accessdenied }
			</div>
		</c:if>
		<c:if test="${param.logout != null}">       
			<p>
				You have been logged out.
			</p>
		</c:if>
  	</div>
  </div>
  <div class="account-wrapper">

    <div class="account-body">

      <h4>ProSalesman</h4>
		<c:url value="/login" var="loginUrl"/>
		<form  class="form account-form" name="loginForm" id="loginForm" action="${loginUrl}" method="post">       
        <div class="form-group">
          <label for="login-username" class="placeholder-hidden">Username</label>
          <input type="text" class="form-control" id="login-username" name="username" placeholder="Username" tabindex="1">
        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="login-password" class="placeholder-hidden">Password</label>
          <input type="password" class="form-control" id="login-password" name="password" placeholder="Password" tabindex="2">
        </div> <!-- /.form-group -->

        <div class="form-group clearfix">
          <div class="pull-left">
            <label class="checkbox-inline">
            <input type="checkbox" class="" value="" tabindex="3"> <small>Remember me</small>
            </label>
          </div>

          <div class="pull-right">
            <small><a href="./account-forgot.html">Forgot Password?</a></small>
          </div>
        </div> <!-- /.form-group -->

        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block btn-lg" id="loginFormBtn" tabindex="4">
            Signin &nbsp; <i class="fa fa-play-circle"></i>
          </button>
        </div> <!-- /.form-group -->
		<input type="hidden"  name="${_csrf.parameterName}" value="${_csrf.token}"/>
      </form>


    </div> <!-- /.account-body -->

  </div> <!-- /.account-wrapper -->
</body>

</html>
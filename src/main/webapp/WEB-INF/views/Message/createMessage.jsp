<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
	<jsp:include page="../head.jsp"/>
</head>

<body class="top-navigation">

<div id="wrapper">
	<div id="header"class="white-bg">
		<jsp:include page="../header.jsp"/>
        	<div class="content-page">  
			<div class="page-header-container">
				<h2>Compose Message</h2>
			</div>          	       	
            <div class="container">
				<div class="row">
		            <div class="col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">
		                <div class="">
			                <div class="">
			                    <form class="form-horizontal" method="get">
			                        <div class="form-group"><label class="col-sm-2 control-label">To:</label>
			                            <div class="col-sm-10"><input type="text" class="form-control" name="toEmployee" id="toEmployee" value="${message.getMessageTo() }"></div>
			                        </div>
			                        <div class="form-group"><label class="col-sm-2 control-label">Subject:</label>
			                            <div class="col-sm-10"><input type="text" class="form-control" name="subject" id="subject" value="${message.getMessageSubject() }"></div>
			                        </div>
				                    <div class="">
										<div class="form-group"><label class="col-sm-2 control-label">Message:</label>
											<div class="col-sm-10"><textarea rows="10" cols="50" class="form-control" name="messageTxt" id="messageTxt"></textarea></div>	
				                        </div>	                    
										
										<div class="clearfix"></div>
				                    </div>	                        
			                        </form>
			                </div>
		                    <div class="mail-body text-right tooltip-demo">
		                        <a href="mailbox.html" class="btn btn-md btn-success" data-toggle="tooltip" data-placement="top" title="Send"><i class="fa fa-paper-plane"></i> Send</a>
		                        <a href="mailbox.html" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> Discard</a>
		                    	<div class="clearfix"></div>
		                	</div>						            
						</div> 
				 	</div>  
            	</div>
			</div>		
	</div>
    <div class="footer-contaner">
		<jsp:include page="../footer.jsp"/>
    </div>
</div>



</body>

</html>
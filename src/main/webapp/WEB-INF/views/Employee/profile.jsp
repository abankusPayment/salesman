<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../head.jsp" />
</head>
<body class="layout-fixed  ">
	<div id="wrapper">
		<jsp:include page="../header.jsp" />
		<div class="content-page">	
			<div class="page-header-container">
				<h2>Profile Page</h2>
			</div>
			<div class="content">
				<div class="container-fluid">
			      <div class="portlet portlet-default">
			
			        <div class="portlet-body">
			
			          <div class="row">	
			  <div class="col-md-6 col-sm-6">

              <br class="visible-xs">
			   <c:set value="${employeeInstance.toString()}" var="employee" />
              <h3>${employee}</h3>

              <h6 class="text-muted">${employeeInstance.getJobTitle()}</h6>

              <hr>

              <p>
                <a href="javascript:;" class="btn btn-primary">Follow Soham</a>
                &nbsp;&nbsp;
                <a href="javascript:;" class="btn btn-secondary">Send Message</a>
              </p>

              <hr>

              <ul class="icons-list">
                <li><i class="icon-li fa fa-envelope"></i> ${employeeInstance.getEmailAddress() }</li>
                <li><i class="icon-li fa fa-globe"></i>${employeeInstance.getPhoneNumber() }</li>
                <li><i class="icon-li fa fa-map-marker"></i> ${employeeInstance.getCity() }&nbsp;,${employeeInstance.getRegion() }</li>
              </ul>

              <br>

              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec.</p>

              <hr>


              <br><br>
                <div class="heading-block">
                  <h5>
                    Employment Details
                  </h5>
                </div> <!-- /.heading-block -->
                <br><br>
                <br class="visible-xs">
                <br class="visible-xs">

              </div> <!-- /.col -->


              <div class="col-md-6 col-sm-6">

                <div class="heading-block">
                  <h5>
                    Social Stats
                  </h5>
                </div> <!-- /.heading-block -->

                <div class="list-group">

                  <a href="javascript:;" class="list-group-item">
                      <h3 class="pull-right"><i class="fa fa-eye text-primary"></i></h3>
                      <h4 class="list-group-item-heading">38,847</h4>
                      <p class="list-group-item-text">Profile Views</p>
                    </a>

                  <a href="javascript:;" class="list-group-item">
                    <h3 class="pull-right"><i class="fa fa-facebook-square  text-primary"></i></h3>
                    <h4 class="list-group-item-heading">3,482</h4>
                    <p class="list-group-item-text">Facebook Likes</p>
                  </a>

                  <a href="javascript:;" class="list-group-item">
                    <h3 class="pull-right"><i class="fa fa-twitter-square  text-primary"></i></h3>
                    <h4 class="list-group-item-heading">5,845</h4>
                    <p class="list-group-item-text">Twitter Followers</p>
                  </a>
                </div> <!-- /.list-group -->

                <br>
				<div>
					<div class="panel panel-primary">
					  <div class="panel-heading">
					    <h3 class="panel-title">Create and Send Message</h3>
					  </div>
					  <div class="panel-body">
					    <div class="form-group">
					    	<label>Email Address:</label>
					    	<input type="text" name="receiverEmailAddress" id="receiverEmailAddress" class="form-control" />
					    </div>
					    <div class="form-group">
					    	<label>Subject:</label>
					    	<input type="text" name="messageSubject" id="messageSubject" class="form-control" />
					    </div>			
					    <div class="form-group">
					    	<label>Message:</label>
					    	<textarea name="messageSubject" id="messageSubject" class="form-control" cols="10" rows="10"></textarea>
					    </div>	
					    <hr/>
					    <div>
					    	<button class="btn btn-success" id="btnSendEmployeeMessage">Send Message</button>
					    	<button class="btn btn-primary" id="btnCancelEmployeeMessage">Cancel</button>
					    </div>			    	    
					  </div>
					</div>				
				</div>
              </div> <!-- /.col -->			          
			          </div>
			        </div>
			      </div>				
				</div>
			</div>
		</div>
	</div>
</body>
</html>
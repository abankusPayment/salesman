<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../head.jsp" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datatables.min.css"/>"/>
 
	<script type="text/javascript" src="<c:url value="/resources/js/datatables.min.js"/>"></script>
	<script src="<c:url value="/resources/js/application/employee.js"/>" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../header.jsp" />

		<div class="content-page">	
			<div class="page-header-container">
				<h2>${searchPageTitle }</h2>
			</div>
			<div class="content">
			<c:if test="${not empty messageInfo}">
				<div class="alert alert-success" role="alert">
					<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
					<span>${messageInfo}</span>
				</div>
			</c:if>	
			<c:if test="${not empty messageError}">
				<div class="alert alert-danger" role="alert">
					<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
					<span>${messageInfo}</span>
				</div>
			</c:if>							
				<div class="container-fluid">	
					<div class="row">
						<table class="table table-hover" id="employeeList">
							<thead>
								<tr>
									<th>Employee Name</th>
									<th>Address Information</th>
									<th>Contact Information</th>
									<th>Job Title</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty employeeList }">
										<c:forEach var="employee" items="${employeeList}">
											<tr>
												<td>${employee}</td>
												<td>${employee.getAddress1() }</td>
												<td>${employee.getPhoneNumber() }</td>
												<td>${employee.getPosition().getPositionName() }</td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<tr>
											<td colspan="4">No Employee's Found</td>
										</tr>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
<script type="text/javascript">
$(document).ready(function(){
	$('#employeeList').DataTable();
});
</script>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../head.jsp" />
	<script src="<c:url value="/resources/js/application/employee.js"/>" type="text/javascript"></script>  
	<script src="<c:url value="/resources/js/application/applicationutils.js"/>" type="text/javascript"></script>  
</head>
<body class="layout-fixed  ">
	<div id="wrapper">
		<jsp:include page="../header.jsp" />
	</div>
		<div class="content-page">
			<div class="container-fluid">		
				<div class="row">
					<div class="col-sm-12 col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
						<h2 class="page-header">New Employee</h2>					
						<div id="lead-container"></div>		
						<c:if test="${not empty error_message}">
			          	<div class="alert alert-danger alert-dismissible fade in" role="alert">
							${error_message}
			    	  	</div>
			    	  	</c:if>
			          	<c:if test="${not empty success_message}">
				          <div class="alert alert-success" role="alert">
								${success_message }
				    	  </div>
			    	  	</c:if>    	
			    	  	<c:url value="/employee/saveEmployee" var="startProposal" />  
						<sf:form method="post" modelAttribute="employee" action="${startProposal}"  id="formNewEmployee" name="formEmployee" class="form" >
						<div class="form-group">
								<label for="firstname" > First Name:</label>
								<input type="text" class="form-control width-100" id="firstname" name="firstname" value="${employeeInstance.getFirstname() }">			
						</div>
						<div class="form-group">
								<label for="lastname" > Last Name:</label>
								<input type="text" class="form-control" id="lastname" name="lastname" value="${employeeInstance.getLastname() }">				
						</div>
						<div class="form-group">
								<label for="address1" >Address:</label>
								<input type="text" class="form-control" id="address1" name="address1" value="${employeeInstance.getAddress1() }">	
								<br/>
								<input type="text" class="form-control width-100" id="address2" name="address2" value="${employeeInstance.getAddress2() }" placeholder="Optional">		
						</div>
						<div class="row">
							<div class="col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="gender" >Gender:</label>
									<select name="gender" id="gender" class="form-control" >
										<option value="">Select a Gender</option>
										<option value="male"> Male </option>
										<option value="female"> Female </option>
									</select>
								</div>	
							</div>	
							<div class="col-sm-6 col-md-6 col-lg-6">						
								<div class="form-group">
									<label for="city" >City:</label>
									<input type="text" class="form-control width-100" id="city" name="city" value="${employeeInstance.getCity() }">	
								</div>
							</div>		
							<div class="col-sm-6 col-md-6 col-lg-6">									
								<div class="form-group">
									<label for="state" >Region:</label>
									<script>createRegionList();</script>
								</div>	
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6">	
								<div class="form-group">
									<label for="emailAddress" >Email Address:</label>
									<input type="email" class="form-control width-100" id="emailAddress"  name="emailAddress" value="${employeeInstance.getEmailAddress() }">	
								</div>	
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="phoneNumber" >Phone Number:</label>
									<input type="text" class="form-control width-100" id="phoneNumber"  name="phoneNumber"value="${employeeInstance.getPhoneNumber() }">				
								</div>	
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12 col-lg-6">
							<fieldset>
								<legend>Insurance Agent Information</legend>
								<div class="form-group">
								<label>Is Employee an Insurance Agent?&nbsp;&nbsp;</label>
								  <div class="radio">
								    <label>
								      <input type="radio" name="position" id="positioninsuranceAgent" value="insuranceAgent" onclick="javascript:toggleAgentDetails(this);" >&nbsp;Insurance Agent
								    </label>
								  </div>
								  <div class="radio">
								    <label>
								      <input type="radio" name="position" id="positionsalesRep" value="salesRep"  onclick="javascript:toggleAgentDetails(this);"> &nbsp;Sales Rep/ Underwriter
								    </label>
								  </div>
								  <div class="radio">
								    <label>
								      <input type="radio" name="position" id="positionOther" value="other"  onclick="javascript:toggleAgentDetails(this);"> &nbsp;Office Staff
								    </label>
								  </div>								  								  
								</div>
								<div id="agentDetails" class="hidden">
									<div class="form-group">
										<label>Location</label>
										<input type="text" name="agentName" id="agentName" class="form-control" placeholder="Name of Agency or Agent Full Name" />
										<span id="helpBlock" class="help-block">Please enter the Agency name or the First and Last Name of Insurance Agent</span>
									</div>									
									<div class="form-group">
										<label>Address 1</label>
										<input type="text" class="form-control" id="locAddress1" name="locAddress1" />
										<br/>
										<input type="text" class="form-control" id="locAddress2" name="locAddress2" />
									</div>
									<div class="form-group">
										<label>City/Town or Village</label>
										<input type="text" class="form-control" id="locAddress1" name="location" />
									</div>
									<div class="form-group">
										<label>Region</label>
										<input type="text" class="form-control" id="locAddress1" name="locAddress1" />
									</div>									
									<div class="">
										<label>Type of Insurance Service</label>
										<input type="checkbox" name="insuranceType" id="insuranceTypeLife" class="form-control" value="life" />Life Insurance
										<input type="checkbox" name="insuranceType" id="insuranceTypeAuto" class="form-control" value="auto" />Auto Insurance
										<input type="checkbox" name="insuranceType" id="insuranceTypeHome" class="form-control" value="home" />Home Insurance
									</div>							
								</div>														
							</fieldset>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="employeePermission">Permission/ Role </label>
									<select class="form-control" name="employeePermission" id="employeePermission">
										<option value="staff">Office Staff</option>
										<option value="agent"> Insurnace Agent</option>
										<option value="admin">Administrator</option>
									</select>
								</div>
							</div>
							</div>
						
			          	<hr>	
			          	<div class="form-group">
			          		<input type="hidden" name="employeeRole[]" id="employeeRole[]" value="" />
			          		<input type="submit" name="btnAddEmployeeSubmit" id="btnAddEmployeeSubmit" class="btn btn-success" value="Add Employee" />
			          	</div>	
			          	</sf:form>							
							</div>
 
		          		</div>
					</div>
				</div>
		</div>
		
</body>
<script type="text/javascript">
	$(document).ready(function(){

	});
	function toggleAgentDetails(element){
		var employeeRole = element.id;
		if($('#positioninsuranceAgent').is(':checked')) {
		    $('#agentDetails').removeClass("hidden");
		    document.getElementById("employeeRole[]").value = "4";
		} else {
			$('#agentDetails').addClass("hidden");
			document.getElementById("employeeRole[]").value = "3";
		}
	}
</script>
</html>
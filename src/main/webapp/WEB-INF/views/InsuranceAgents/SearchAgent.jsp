<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../head.jsp" />
	<script src="<c:url value="/resources/js/application/employee.js"/>" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../header.jsp" />

		<div class="content-page">	
			<div class="page-header-container">
				<h2>${searchPageTitle }</h2>
			</div>
			<div class="content">
			<c:if test="${not empty messageInfo}">
				<div class="alert alert-info" role="alert">
					<span>${messageInfo}</span>
				</div>
			</c:if>				
				<div class="container-fluid">	
					<div class="row">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Employee Name</th>
									<th>Address Information</th>
									<th>Contact Information</th>
									<th>Location</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty agentList }">
										<c:forEach var="agent" items="${agentList}" varStatus="count">
											<tr>
												<td>${agent.getFirstname()} </td>
												<td>${agent.getAddress1() }</td>
												<td>${agent.getPhoneNumber() }</td>
												<td>${agent.getLocation() }</td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<tr>
											<td colspan="4">No Employee's Found</td>
										</tr>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>
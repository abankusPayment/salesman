<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Kejetia Sales Pipeline</title>

  <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
  <link href="<c:url value="/resources/font-awesome/css/font-awesome.css"/>" rel="stylesheet">
  <link href="<c:url value="/resources/css/mvpready-admin.css"/>" rel="stylesheet">
  <link href="<c:url value="/resources/css/salesman.css"/>" rel="stylesheet">
  <link href="<c:url value="/resources/css/plugins/toastr/toastr.min.css"/>" rel="stylesheet">
  <!-- Mainly scripts -->
	<script src="<c:url value="/resources/js/jquery.js"/> "></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js"/> "></script>
	<script src="<c:url value="/resources/js/jquery.slimscroll.min.js"/>"></script>
	<script src="<c:url value="/resources/js/plugins/toastr/toastr.min.js" /> "></script>
	
	<script src="<c:url value="/resources/js/mvpready-core.js" />"></script>
	<script src="<c:url value="/resources/js/mvpready-helpers.js" />"></script>
	<script src="<c:url value="/resources/js/mvpready-admin.js" /> "></script>   
	<script src="<c:url value="/resources/js/genesis10.js"/>"></script>
	<script src="<c:url value="/resources/js/validation/bootstrapValidator.js"/> "></script>
	<script src="<c:url value="/resources/js/application/applicationutils.js"/>" type="text/javascript"></script>  

	
	

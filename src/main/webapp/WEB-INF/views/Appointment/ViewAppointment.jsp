<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="../head.jsp" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datatables.min.css"/>"/>
 
	<script type="text/javascript" src="<c:url value="/resources/js/datatables.min.js"/>"></script>
	<script src="<c:url value="/resources/js/application/employee.js"/>" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../header.jsp" />

		<div class="content-page">	
			<div class="page-header-container">
				<h2>Appointment and Schedule</h2>
			</div>
			<div class="row">
			
			</div>
		</div>
	</div>
</body>
</html>
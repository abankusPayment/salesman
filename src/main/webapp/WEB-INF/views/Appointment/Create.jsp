<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../head.jsp" />
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../header.jsp" />
		<div class="content-page">	
			<div class="page-header-container">
				<h2>Create an Appointment</h2>
			</div>

		</div>
	</div>	
</body>
<script type="text/javascript">
$(document).ready(function(){
	$('#employeeList').DataTable();
});
</script>
</html>
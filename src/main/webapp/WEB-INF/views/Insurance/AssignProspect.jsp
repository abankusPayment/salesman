<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../head.jsp" />
</head>
<body class="layout-fixed  ">
	<div id="wrapper">
		<jsp:include page="../header.jsp" />
		<div class="content-page">	
			<div class="page-header-container">
				<h2>Assign Prospect to Insurance Agent</h2>
			</div>
			<div class="content">
				<div class="container-fluid">	
				<div class="row">
					<div>
						<input type="submit" class="btn btn-primary" value="Submit" id="btnAssignAgent" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
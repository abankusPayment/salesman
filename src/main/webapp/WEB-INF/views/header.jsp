<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
  <div class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <i class="fa fa-cog"></i>
        </button>

        <a href="./index.html" class="navbar-brand navbar-brand-img">
          <img src="<c:url value="/resources/images/kejetia.png"/>" alt="MVP Ready" width="50" height="">
        </a>
      </div> <!-- /.navbar-header -->

      <nav class="collapse navbar-collapse hidden" role="navigation">
      <c:url value="/customer/search" var="search"/>
        <sf:form class="navbar-form navbar-search-form navbar-left" action="${search }" method="post">
          <div class="form-group">
            <input type="text" class="form-control navbar-search-field" name="search" placeholder="Type here for search...">
          </div>
        </sf:form>      
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="<c:url value="/messages/mailInbox" />">Messages</a>
          </li>

        </ul>

      </nav>

    </div> <!-- /.container -->

  </div>
<div class="sidebar">

    <div class="sidebar-bg"></div><!-- /.sidebar-bg -->

    <div class="sidebar-trigger sidebar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
      <i class="fa fa-bars"></i>
    </div><!-- /.sidebar-trigger -->

    <div class="sidebar-inner sidebar-collapse collapse">

      <ul class="sidebar-menu">

        <li class="sidebar-header">
            Dashboards & Reports
        </li>

        <li class="">
          <a href="<c:url value="/salesman/index" />">
            <i class="fa fa-home"></i>
            Dashboard
          </a>
        </li>

        <li class="divider"></li>
		<sec:authorize access="hasRole('ROLE_SALESREP')">
        <li class="sidebar-header">
           Sales Management
        </li>

        <li class="dropdown has_sub ">

          <a href="#" class="">
            <i class="fa fa-edit"></i>

            Direct Sales

            <span class="">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>

          <ul class="list-unstyled" style="">
            <li>
              <a href="<c:url value="/sales/AddNewProspect"/>">
                  New Sales Prospect
              </a>
            </li>

            <li>
              <a href="<c:url value="/sales/assignProspect" />">
               Assign Prospect to Agent
              </a>
            </li>

            <li>
              <a href="<c:url value="/sales/viewSales" />">
                View Generated Sales
              </a>
            </li>
          </ul>
        </li>

        <li class="dropdown has_sub ">
          <a href="javascript:;" class="">
            <i class="fa fa-table"></i>
            Sales
            <span class="pull-right">
              <i class="fa fa-angle-right"></i>
            </span>
          </a>

          <ul class="list-unstyled" style="">
            <li>
              <a href="<c:url value="/insurance/insurancePolicy" />">
                Sales Lookup
              </a>
            </li>

            <li>
              <a href="<c:url value="/sales/viewSales" />">
                View Generated Sales
              </a>
            </li>
          </ul>
        </li>

        <li class="">
           <a href="<c:url value="/insurance/proposal" />">
            <i class="fa fa-bars dropdown-icon "></i>
              New Insurance Proposal
           </a>          
        </li>

        <li class="">
          <a href="<c:url value="/customer/search" />">
            <i class="fa fa-search dropdown-icon "></i>
            Search Customer
          </a>
        </li>

        <li class="divider"></li>
		</sec:authorize>
		<sec:authorize access="hasRole('ROLE_INSURANCEAGENT')">
        <li class="sidebar-header">
            Search and Appointment
        </li>
         <li>
           <a href="<c:url value="/customer/search"/>">
           <i class="fa fa-search"></i>
             Search Customer
           </a>
         </li>
         <li>
           <a href="<c:url value="/sales/AddNewProspect"/>">
           <i class="fa fa-list"></i>
             List Customers
           </a>
         </li> 
         <li>
         	<a href="<c:url value="/appointment/viewAppointments"/>">
         		<i class="fa fa-calendar"></i>
         	 Appointments</a>
         </li>   
		
         <li class="divider"></li>
         </sec:authorize>
		  <li class="sidebar-header">Customer</li>
              <li>
                <a href="<c:url value="/sales/AddNewProspect"/>">
                <i class="fa fa-desktop"></i>
                  Make New Sale
                </a>
              </li>

              <li>
                <a href="<c:url value="/insurance/proposal" />">
                <i class="fa fa-desktop"></i>
                  Create Insurance Prosposal
                </a>
              </li>		  
		  <li class="divider"></li>
			<li class="sidebar-header">Messages</li>
				<li>
	                <a href="<c:url value="/messages/mailInbox" />" >
	                  Mail Box
	                </a>
	              </li>
	
	              <li>
	                <a href="<c:url value="/messages/createMessage" />">
	                <i class="fa fa-envelope"></i>
	                	Send Message
	                </a>
	              </li>
	
	              <li>
	                <a href="./account-signup.html">
	                <i class="fa fa-comments text-primary"></i>
	                  Send Notification
	                </a>
	           	  </li> 
	      <li class="divider"></li>  
          <li class="sidebar-header">Settings</li>
			
          <li>
            <a href="<c:url value="/employee/profile" />" class="">
              <i class="fa fa-desktop"></i>
              View your Profile
            </a>
          </li>
          <li class="divider"></li>  
        <sec:authorize access="hasRole('ROLE_ADMINISTRATOR')"> 
		<li class="sidebar-header">
            Administration
        </li>
        <li class="dropdown has_sub">
          <a href="http://jumpstartthemes.com/#" class="">
            <i class="fa fa-file-text-o"></i>

            Manage Select Options

            <span class="pull-right">
              <i class="fa fa-angle-right"></i>
            </span>
          </a>
        </li>
        <li class="dropdown has_sub">
          <a href="http://jumpstartthemes.com/#" class="">
            <i class="fa fa-users" aria-hidden="true"></i>
            Employee Management

            <span class="pull-right">
              <i class="fa fa-angle-right"></i>
            </span>
          </a>

          <ul class="list-unstyled">

            <li>
              <a href="<c:url value="/employee/addEmployee" />">
                New Employee
              </a>
            </li>

            <li>
              <a href="<c:url value="/employee/listEmployee" />">
                List All Employees
              </a>
            </li>
          </ul>
        </li>
          </sec:authorize>
          <li class="divider"></li>
          <li>
            <a href="<c:url value="/security/logout" />" class="">
              <i class="fa fa-rocket"></i>

              Logout
            </a>
          </li>
        </ul>

        <div class="clearfix"></div>

    </div><!-- /.sidebar-inner -->

  </div> <!-- /.side-menu -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../head.jsp" />

</head>
<body>
	<div id="wrapper">
		<jsp:include page="../header.jsp" />

		<div class="content-page">
		<c:choose>
			<c:when test="${not empty prospective}">
					<div class="page-header-container">
						<h2><strong>${prospective.getFullname()}</strong> Profile View</h2>
					</div>
					<div class="content" id="errorContainer">
					<c:if test="${not empty messageInfo}">
						<div class="alert alert-info" role="alert">
							<span>${messageInfo}</span>
						</div>
					</c:if>				
						<div class="container">	
					      <div class="portlet portlet-default">
					
					        <div class="portlet-body">
					
					          <div class="row">
								<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 co">
					
					              <br class="visible-xs">
					
					              <h6 class="text-muted">Date Enquired: ${prospective.getDateEnquired() }</h6>
					
					              <hr>
					
					              <p>
					                <a href="javascript:;" class="btn btn-primary" id="emailDiv-open"><i class="glyphicon glyphicon-envelope" > </i> Send Email</a>
					                &nbsp;&nbsp;
					                <a href="javascript:;" class="btn btn-secondary" id="smsDiv-open" ><i class="glyphicon glyphicon-comment" > </i> Send SMS Message</a>
					                &nbsp;&nbsp;
					                <a href="javascript:;" class="btn btn-secondary"><i class="glyphicon glyphicon-calendar" > </i> Schedule Appointment</a>			                
					              </p>
					              <hr>
					              <div class="col-md-6">
						              <ul class="icons-list">
						                <li><i class="icon-li fa fa-phone"></i> ${prospective.getPhoneNumber() }</li>
						                <li><i class="icon-li fa fa-envelope"></i> ${prospective.getEmailAddress() }</li>
						                <li><i class="icon-li fa fa-map-marker"></i> ${prospective.getAddressLocation()}</li>
						              </ul>
					              </div>
					              <c:if test="${not empty prospective.getAssignedAgent() }">
					              <c:set value="${prospective.getAssignedAgent()}" var="agent"/>
						              <div class="col-md-6">
						              	<h5>Agency Information</h5>
						              	<address>
						              		<strong>${agent.getAgencyName()}</strong>
						              		<div>${agent.getAddress1() }<br/>${agent.getAddress2() }</div>
						              		<div>${agent.getCity() }&nbsp;${agent.getRegion() }</div>
						              		<span><strong>Phone Number:</strong>&nbsp;</span>${agent.getPhoneNumber() }
						              	</address>
						              </div>			              
					              </c:if>			              
					              <div class="col-md-4">
					              	<a class="btn btn-success btn-sm" href="<c:url value="/customer/updateInformation" />">Update Personal Information</a>
					              </div>
					              <div class="clearfix"></div>
					              <br>
					              <hr>
					              <div class="heading-block">
					                <h4>
					                  Add Notes
					                </h4>
					              </div> <!-- /.heading-block -->
					                <div class="share-widget clearfix">
					                <c:url value="/customer/saveEmployeeNoteToCustomer" var="prospectiveCustomerNoteAction" />
					                <sf:form name="prospectiveCustomerNoteForm" id="prospectiveCustomerNoteForm" action="${prospectiveCustomerNoteAction}">
					                  <textarea class="form-control share-widget-textarea" rows="3" name="notes" placeholder="Add notes for future reference..." tabindex="1"></textarea>
					                  <div class="share-widget-actions">
					                    <div class="share-widget-types pull-left hidden">
					                      <a href="javascript:;" class="fa fa-picture-o ui-tooltip" title="Post an Image"><i></i></a>
					                      <a href="javascript:;" class="fa fa-video-camera ui-tooltip" title="Upload a Video"><i></i></a>
					                      <a href="javascript:;" class="fa fa-lightbulb-o ui-tooltip" title="Post an Idea"><i></i></a>
					                      <a href="javascript:;" class="fa fa-question-circle ui-tooltip" title="Ask a Question"><i></i></a>
					                    </div>
										  <input type="hidden" name="prospectiveId" value="${prospective.getProspectiveId() }" />
						                  <div class="pull-right">
						                    <a class="btn btn-success btn-sm" tabindex="2" id="btnAddNote" onclick="javascript:addNoteToCustomerAccount();">Save</a>
						                  </div>
										
					                  </div> <!-- /.share-widget-actions -->
									</sf:form>
					                </div> <!-- /.share-widget -->						          
								</div>
								<div class="col-md-6 hidden">
								<h4>Insurance Agent Notes</h4>
									<c:forEach items="${prospective.getInsuredStatusList() }" var="insuredState">
							           <div class="feed-item feed-item-idea">
						
						                  <div class="feed-icon bg-primary">
						                    <i class="fa fa-lightbulb-o"></i>
						                  </div> <!-- /.feed-icon -->
						
						                  <div class="feed-subject">
						                    <p><a href="javascript:;"> ${insuredState.getEmployeeName() }</a> Notes on Customer</p>
						                  </div> <!-- /.feed-subject -->
						
						                  <div class="feed-content">
						                    <ul class="icons-list">
						                      <li>
						                        <i class="icon-li fa fa-quote-left"></i>
						                        ${insuredState.getMessage() }
						                      </li>
						                    </ul>
						                  </div> <!-- /.feed-content -->
						
						                  <div class="feed-actions">
						                    <a href="javascript:;" class="pull-left"><i class="fa fa-thumbs-up"></i> 123</a>
						                    <a href="javascript:;" class="pull-left"><i class="fa fa-comment-o"></i> 29</a>
						
						                    <a href="javascript:;" class="pull-right"><i class="fa fa-clock-o"></i> ${insuredState.getDateChanged()}</a>
						                  </div> <!-- /.feed-actions -->
						
						                </div> <!-- /.feed-item -->							
									</c:forEach>
						
								</div>
								<div  class="col-md-3 hidden">
					              <div class="list-group">
					
					                <a href="javascript:;" class="list-group-item">
					                  <i class="fa fa-asterisk text-primary"></i> &nbsp;&nbsp;Assign Agent to Prospect
					
					                  <i class="fa fa-chevron-right list-group-chevron"></i>
					                </a>
					
					                <a href="javascript:;" class="list-group-item">
					                  <i class="fa fa-book text-primary"></i> &nbsp;&nbsp;Change Prospect Status
					
					                  <i class="fa fa-chevron-right list-group-chevron"></i>
					                </a>
					
					                <a href="javascript:;" class="list-group-item">
					                  <i class="fa fa-envelope text-primary"></i> &nbsp;&nbsp;Messages
					
					                  <i class="fa fa-chevron-right list-group-chevron"></i>
					                </a>
					
					                <a href="javascript:;" class="list-group-item">
					                  <i class="fa fa-group text-primary"></i> &nbsp;&nbsp;Friends
					
					                  <i class="fa fa-chevron-right list-group-chevron"></i>
					                </a>
					
					                <a href="javascript:;" class="list-group-item">
					                  <i class="fa fa-cog text-primary"></i> &nbsp;&nbsp;Settings
					
					                  <i class="fa fa-chevron-right list-group-chevron"></i>
					                </a>
					              </div>						
								</div>
					        </div>
					      </div>
						</div>
					</div>
				</div>			
			</c:when>
			<c:when test="${not empty error_message}">
				<div class="container">
					<div class="alert alert-info" role="alert">
						<span>${ error_message}</span>
					</div>				
				</div>			
			</c:when>
		</c:choose>	
	</div>
	<%--- Hidden Modals --%>
	<!-- The Modal -->	
	<div class="modal fade" tabindex="-1" id="emailDiv-modal" role="dialog">
	  <div class="modal-dialog" role="document">
	  	<div class="modal-content">	 
		    <div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">Compose Email message</h4>
	      	</div>
      		<div class="modal-body">
		      	<div class="form-group">
		      		<label>Subject:</label>
		      		<input type="text" class="form-control" name="emailSubject" id="emailSubject"/>
		      	</div>
      			<textarea class="form-control" rows="10" cols="60" id="emailMessage" name="emailMessage"></textarea>
      		</div>
		    <div class="modal-footer">
		      <button class="btn btn-success" onclick="javascript:sendCustomerEmail()">Send Message</button>
		    </div>
	    </div>
	  </div>
	</div>	
	<!-- Email Modal -->
	<div class="modal fade" tabindex="-1" id="smsDiv-modal" role="dialog" aria-labelledby="myLargeModalLabel">
	  <div class="modal-dialog" role="document">
	  	 <div class="modal-content">	 
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Send SMS Message to </span>&nbsp;${prospective.getFullname()}</h4>
		      </div>
		      <div class="modal-body">
		      	<textarea class="form-control" rows="10" cols="60" id="smsMessage" name="smsMessage"></textarea>
		      </div>
			    <div class="modal-footer">
			      <button class="btn btn-success" onclick="javascript:sendCustomerSMS()">Send Message</button>
			    </div>
	    </div>
	  </div>
	</div>	
	<script src="<c:url value="/resources/js/application/employee.js"/>" type="text/javascript"></script>  
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../head.jsp"/>
	
    <style>

        .wizard > .content > .body { position: relative; }

.radio input[type="radio"]{
    vertical-align:middle;
    }
    </style>	
</head>
<body  class="top-navigation">

	<div id="wrapper">
		<jsp:include page="../header.jsp" />
		<div class="content-page">	
			<div class="page-header-container">
				<h2>Prospective Client</h2>
			</div>
			<div class="content">
				<div class="container-fluid">
		            <div class="row">
		            	<c:choose>
		            		<c:when test="${prospect.getInsuranceType() eq 'life' }">
		            		<div class="col-md-8 col-md-offset-2 col-lg-9">
						<c:url value="/proposal/submitProposal" var="submitProposal"/>
		            		<sf:form htmlEscape="true" method="put" action="${submitProposal}" id="insuranceProposalForm" class="wizard-big">
		            		<!-- Personal Information tab -->
		            		<fieldset>
		            			<h2>Personal Information</h2>
								<div class="form-group">
		            				<div class="row">
		            					<div class="col-md-6">
				            				<label for="firstname">First name:</label>
				            				<input type="text" name="txtfirstname" id="txtfirstname" class="form-control" value="${prospect.getFirstname() }"/>		            					
		            					</div>
		            					<div class="col-md-6">
				            				<label for="lastname">Last name:</label>
				            				<input type="text" name="txtlastname" id="txtlastname" class="form-control"value="${prospect.getLastname() }"/>		            					
		            					</div>		            					
		            				</div>
		            			</div>
		            			<div class="form-group">
		            				<div class="row">
		            				<div class="col-md-6">
		            					<label for="selGender">Gender</label>
		            					<select id="custGender"  name="custGender"onchange="javascript:customerGender(this.value);" class="form-control">
		            						<option value="">Select a Gender</option>
		            						<option value="male">Male</option>
		            						<option value="female">Female</option>
		            					</select>
		            				</div>
		            				<div class="col-md-6">
		            					<label for="selDOB"> Date Of Birth: </label>
		            					<input type="text" class="form-control" name="dateOfBirth" id="dateOfBirth" onclick="javascript:getDateOfBirth(this);" value="${prospect.getDateOfBirth() }"/>
		            				</div>
		            				</div>		            				
		            			</div> 		            			
								<div class="form-group">
				            		<label for="firstname">Address 1:</label>
				            		<input type="text" name="txtaddress1" id="txtaddress1" class="form-control" value="${prospect.getAddress1() }"/>	
				            		<br>
				            		<label for="firstname">Address 2: <span class="help-text">Optional</span></label>
				            		<input type="text" name="txtaddress2" id="txtaddress2" class="form-control" value="${prospect.getAddress2() }"/>		
		            			</div>	
		            			<div class="form-group">
									<div class="row">
		            					<div class="col-md-6">
				            				<label for="txtfirstname">City:</label>
				            				<input type="text" name="txtcity" id="txtcity" class="form-control" value="${prospect.getCity() }"/>		            					
		            					</div>
		            					<div class="col-md-6">
				            				<label for="txtregion">Region:</label>
				            				<input type="text" name="txtregion" id="txtregion" class="form-control" value="${prospect.getRegion() }"/>		            					
		            					</div>		            					
		            				</div>		            			
		            			</div>
		            			<div class="form-group">
									<div class="row">
		            					<div class="col-md-6">
				            				<label for="txtphoneNumber">Phone Number:</label>
				            				<input type="text" name="txtphoneNumber" id="txtphoneNumber" class="form-control" value="${prospect.getPhoneNumber() }"/>	
				            				 <div class="form-group  ">  				
												<div class="radio">
												    <input type="radio" name="phoneType" id="phoneTypeHome" value="home" ${prospect.getPhoneType() == 'home' ? 'checked' : '' }/>
												    <label>
												    	Home Phone
												  	</label>
												</div>
												<div class="radio">
												    <input type="radio" name="phoneType" id="phoneTypeWork" value="work" ${prospect.getPhoneType() == 'work' ? 'checked' : '' }/>
												    <label>
												    	Work Phone
												  	</label>
												</div>
												<div class="radio">
												    <input type="radio" name="phoneType" checked="checked" id="phoneTypeMobile" value="mobile" ${prospect.getPhoneType() == 'mobile' ? 'checked' : '' }/>
												    <label for="phoneTypeMobile">
												    	Mobile Phone
												  	</label>
												</div>
											</div>               					
		            					</div>
		            					<div class="col-md-6">
		            						<div class="col-md-12">
				            					<label for="txtemailAddress">Email Address:</label>
				            					<input type="email" name="txtEmailAddress" id="txtemailAddress" class="form-control" value="${prospect.getEmailAddress()}"/>	
				            				</div>           					
		            					</div>			            					
		            				</div>	
		            			</div>				            			 		            				
		            			</fieldset>
		            			
		            			<h1>Existing Insurance</h1>
		            			<fieldset>
		            				<h2>Existing Insurance</h2>
										<div class="form-group">
			            				<div class="row">
				            				<div class="col-md-6">
				            					<label for="selHsdIncome">Household Income (Monthly) - Ghana Cedis</label>
				            					<select id="selHsdIncome" name="selHsdIncome" class="form-control">
				            						<option value="">Select your/household monthly Income</option>
				            						<option value="0500"> GHC 0 - 500 </option>
				            						<option value="5001000"> GHC 500 - 1,000</option>
				            						<option value="10003000"> GHC 1,000 - 3,000</option>
				            						<option value="30005000"> GHC 3,000 - 5,000</option>
				            						<option value="500010000"> GHC 5,000 - 10,000</option>
				            						<option value="10000plus"> GHC 10,000+ </option>
				            					</select>
				            				</div>
				            				<div class="col-md-6">
				            					<label for="selHsdNumber"> Household Number: </label>
				            					<select id="selHsdNumber" name="selHsdNumber" class="form-control">
				            						<option value="">Select number in your household</option>
				            						<option value="01"> Single - 01 </option>
				            						<option value="02"> Married with no Children - 02 </option>
				            						<option value="03"> 3 people </option>
				            						<option value="04"> 4 people </option>
				            						<option value="05"> 5 - 10 people </option>
				            						<option value="06plus"> 10+ people</option>
				            					</select>		            				
				            				</div>
			            				</div>		            				
			            			</div> 
			            			<div class="form-group">
			            				<div class="row">
				            				<div class="col-md-6">
				            					<label for="selMaritalStatus">Marital Status</label>
				            					<select id="selMaritalStatus" class="form-control">
				            						<option value="">Select your Marital Status</option>
				            						<option value="single"> Single </option>
				            						<option value="married"> Married </option>
				            						<option value="divorced"> Divorced </option>
				            						<option value="widowed"> Widow/Widower</option>
				            					</select>
				            				</div>
				            				<div class="col-md-6">
				            					<label for="txtOccupation"> Occupation </label>
												<input type="text" name="txtOccupation" id="txtOccupation" class="form-control" />		            				
				            				</div>
			            				</div>		            				
			            			</div>
			            			<div class="form-group">
			            				<div class="row">
				            				<div class="col-md-6">
				            					<label>Hobbies: </label>
				            					<input type="text" name="hobby" id="hobby" class="form-control" />
				            				</div>
				            				<div class="clearfix"></div>
				            			</div>		            				
			            			</div>		
			            			<div class="form-group">
			            				<div class="row">
				            				<div class="col-md-6">
				            					<label>Hometown: </label>
				            					<input type="text" name="hometown" id="hometown" class="form-control" />
				            				</div>
				            				<div class="clearfix"></div>
				            			</div>		            				
			            			</div>			            				            				
		            				<div class="form-group">
										<div class="row">
													<div class="col-md-12">
														<label>Do you or have you ever had any assurance on your life?  </label>
														<br/>
														<div class="radio radio-inline">
														  
														    <input type="radio" name="hasInsuranceWithUs" value="Yes"  onclick="javascript:hideShowContainer('sectionInsuranceCommenceDate',true)"/> <label>Yes
														  </label>
														</div>
														<div class="radio radio-inline">
														    <input type="radio" name="hasInsuranceWithUs" value="No" checked="checked" onclick="javascript:hideShowContainer('sectionInsuranceCommenceDate',false)"/><label> No
														  </label>
														</div>														
														<div id="sectionInsuranceCommenceDate" class="dynamicContainer">
															<label>Commencement Date:</label>
															<input type="text" name="txtInsuranceCommenceDate" id="txtInsuranceCommenceDate" class="form-control" />
														</div>
													
													</div>
													<div class="col-md-12">
														<label> Have you ever made a claim under any existing or previous Policy? </label>
														<br/>
														<div class="radio radio-inline">
														    <input type="radio" name="hasClaimWithExistingInsure" value="Yes" onclick="javascript:hideShowContainer('sectionExistingPolicyNumber',true)"/> <label> Yes
														  </label>
														</div>
														<div class="radio  radio-inline">
														 
														    <input type="radio" name="hasClaimWithExistingInsure" value="No" checked="checked" onclick="javascript:hideShowContainer('sectionExistingPolicyNumber',false)"/> <label> No
														  </label>
														</div>														
														<div id="sectionExistingPolicyNumber" class="dynamicContainer">
															<label>Policy Number:</label>
															<input type="text" name="txtExistingInsurePolicyNo" id="txtExistingInsurePolicyNo" class="form-control" />
														</div>
													
													</div>	
											</div>
									</div>											
								</fieldset>
								
										<h1>Customer Family History</h1>
										<fieldset>
											<h2>Your Family History</h2>
											<div class="form-group" id="genderSpecificContainer">
												<label>Are you Pregnant or may be pregnant?</label>
												<br/>
													<div class="radio radio-inline">
													    <input type="radio" name="isPregnant" id="isPregnantYes" value="Yes" /> <label> Yes
													  </label>
													</div>
													<div class="radio radio-inline">
													 
													    <input type="radio" name="isPregnant" id="isPregnantNo" checked="checked" value="No"/> <label> No
													  </label>
													</div>												
											</div>
											<div class="form-group">
												<label> Have you ever had cancer or disorder of the breast or female organ or birth by caesarean section? </label>
												<br/>
												<div class="radio radio-inline">
													<input type="radio" name="hasBreastCancerCaesarean" id="hasBreastCancerCaesareanYes" value="Yes" /> 
													<label>
													   Yes
													</label>
												</div>
												<div class="radio   radio-inline">
													 
													    <input type="radio" name="hasBreastCancerCaesarean" id="hasBreastCancerCaesareanNo" checked="checked" value="No"/> <label> No
													  </label>
												</div>												
											</div>													
										<h2>Customer Family History</h2>
											<div class="form-group">
											<div class="row">
											
											<label>Has any member of your family ever had: </label>
											<ol>
												<li><label>Heart Ailment?</label><br/>
													<div class="radio radio-inline">
													    <input type="radio" name="hasHeartAilment" value="Yes"  onclick="javascript:hideShowContainer('sectionHeartAilmentDetails',true)"/>  <label>Yes
													  </label>
													</div>
													<div class="radio radio-inline">
													 
													    <input type="radio" name="hasHeartAilment" value="No" checked="checked" onclick="javascript:hideShowContainer('sectionHeartAilmentDetails',false)"/> <label> No
													  </label>
													</div>	
													<div id="sectionHeartAilmentDetails" class="dynamicContainer">
														<label>Describe further details about your illness - Heart Ailment</label>
														<textarea rows="3" cols="3" class="form-control" name="sectionHeartAilmentDetails"></textarea>
													</div>													
												</li>
												<li>
													<label>Nervous disease?</label><br/>
													<div class="radio radio-inline">
													  
													    <input type="radio" name="hasNervousDisease" value="Yes"  onclick="javascript:hideShowContainer('sectionDiabetesDetails',true)"/><label> Yes
													  </label>
													</div>
													<div class="radio radio-inline">
													    <input type="radio" name="hasNervousDisease" value="No" checked="checked" onclick="javascript:hideShowContainer('sectionDiabetesDetails',false)"/><label> No
													  </label>
													</div>
													<div id="sectionNervousDetails" class="dynamicContainer">
														<label>Describe further details about your illness - Nervous disease</label>
														<textarea rows="3" cols="3" class="form-control" name="txtNervousDetails"></textarea>
													</div>																										
												</li>
												<li>
													<label>Diabetes?</label><br/>
													<div class="radio radio-inline">
													 
													    <input type="radio" name="hasDiabetes" value="Yes" onclick="javascript:hideShowContainer('sectionDiabetesDetails',true)" /><label> Yes
													  </label>
													</div>
													<div class="radio  radio-inline">
													    <input type="radio" name="hasDiabetes" value="No" checked="checked" onclick="javascript:hideShowContainer('sectionDiabetesDetails',false)"/> <label> No
													  </label>
													</div>			
													<div id="sectionDiabetesDetails" class="dynamicContainer">
														<label>Describe further details about your illness - Diabetes</label>
														<textarea rows="3" cols="3" class="form-control" name="txtDiabetesDetails"></textarea>
													</div>																						
												</li>
												<li><label>Asthma?</label><br/>
													<div class="radio  radio-inline">
													  
													    <input type="radio" name="hasAsthma" value="Yes"  onclick="javascript:hideShowContainer('sectionAsthmaDetails',true)"/><label> Yes
													  </label>
													</div>
													<div class="radio radio-inline">
													    <input type="radio" name="hasAsthma" value="No" checked="checked" onclick="javascript:hideShowContainer('sectionAsthmaDetails',false)"/><label> No
													  </label>
													</div>	
													<div id="sectionAsthmaDetails" class="dynamicContainer">
														<label>Describe further details about your illness - Tuberculosis</label>
														<textarea rows="3" cols="3" class="form-control" name="txtAsthmaDetails"></textarea>
													</div>																								
												</li>
												<li><label>Tuberculosis?</label><br/>
													<div class="radio  radio-inline">
													  
													    <input type="radio" name="hasTuberculosis" value="Yes" onclick="javascript:hideShowContainer('sectionTuberculosisDetails',true)"/><label> Yes
													  </label>
													</div>
													<div class="radio  radio-inline">
													    <input type="radio" name="hasTuberculosis" value="No" checked="checked" onclick="javascript:hideShowContainer('sectionTuberculosisDetails',false)"/><label> No
													  </label>
													</div>
													<div id="sectionTuberculosisDetails" class="dynamicContainer">
														<label>Describe further details about your illness - Tuberculosis</label>
														<textarea rows="3" cols="3" class="form-control" name="txtTuberculosisDetails"></textarea>
													</div>																										
												</li>
												<li>
													<label>Cancer?</label><br/>
													<div class="radio radio-inline">
													    <input type="radio" name="hasCancer" value="Yes"  onclick="javascript:hideShowContainer('sectionCancerDetails',true)"/><label> Yes
													  </label>
													</div>
													<div class="radio  radio-inline">
													    <input type="radio" name="hasCancer" value="No"  checked="checked" onclick="javascript:hideShowContainer('sectionCancerDetails',false)"/><label> No
													  </label>
													</div>	
													<div id="sectionCancerDetails" class="dynamicContainer">
														<label>Describe further details about your illness - Cancer</label>
														<textarea rows="3" cols="3" class="form-control" placeholder="Provide treatment date, hospital name, result of treatment"></textarea>
													</div>											
												 </li>
											</ol>
											</div>
										</div>
										</fieldset>

								<h1>Health Information</h1>
								<fieldset>
									<h2>Health Information</h2>
									<div class="row">
				            			<div class="form-group">
				            				<div class="col-md-6 col-lg-4">
				            					<label for="customerHeight"> Height </label>
												<input type="text" name="customerHeight" id="customerHeight" class="form-control" />
				            				</div>
				            				<div class="col-md-6 col-lg-4">
				            					<label for="customerWeight"> Weight </label>
												<input type="text" name="customerWeight" id="customerWeight" class="form-control" />		            				
				            				</div>	 
				            				<div class="clearfix"></div>           				
				            			</div>										
										<div class="form-group col-md-12">
											<label>Do you smoke?</label>
												<div class="radio radio-inline">
												  
												    <input type="radio" name="isSmoker" value="Yes" onclick="javascript:hideShowContainer('sectionSmokerDetails',true)" /><label> Yes
												  </label>
												</div>
												<div class="radio radio-inline">
												    <input type="radio" name="isSmoker" value="No" checked="checked" onclick="javascript:hideShowContainer('sectionSmokerDetails',false)"/><label> No
												  </label>
												</div>		
												<div id="sectionSmokerDetails" class="dynamicContainer">
													<label>How many sticks of cigarettes do you smoke in a day?</label>
													<input type="number" name="numberOfCigarettesDaily" class="form-control" />
												</div>										
										</div>
										<div class="form-group col-md-12">
											<label> Do you drink or consume alcohol? </label>
												<div class="radio radio-inline">
												    <input type="radio" name="drinkAlcohol" value="Yes" onclick="javascript:hideShowContainer('sectionAlcoholDetails',true)" /><label> Yes
												  </label>
												</div>
												<div class="radio radio-inline">
												    <input type="radio" name="drinkAlcohol" value="No" checked="checked" onclick="javascript:hideShowContainer('sectionAlcoholDetails',false)"/><label> No
												  </label>
												</div>				
												<div id="sectionAlcoholDetails" class="dynamicContainer">
													<label>What is your average consumption of alcohol?(in Bottles) </label>
													<input type="number" name="numberOfAlcoholDaily" class="form-control" />
												</div>																						
										</div>									
										<div class="col-md-12">
											<label>Are you at present suffering from any physical 
												defect or is there any ailment or disease from
												which you suffer or to which you have a
												tendency?
											</label>
											<br/>
											<div class="radio">
											    <input type="radio" name="hasInsuranceWithUs" value="Yes" /><label> Yes
											  </label>
											</div>
											<div class="radio ">
											    <input type="radio" name="hasInsuranceWithUs" checked="checked" value="No"/><label> No
											  </label>
											</div>		
																					
										</div>	
										<div class="form-group">	
										<div class="col-md-12">
											<label>
												Have you at any time suffered from any illness
												or injury requiring medical or psychiatric/
												herbal attention?
											</label>
											<br/>
											<div class="radio">
												<input type="radio" name="hasMentalIllnes" value="Yes" /><label>  Yes </label>
											</div>
											<div class="radio">
											    <input type="radio" name="hasMentalIllnes" checked="checked" value="No"/><label> No </label>	
											</div>										
										</div>	
										</div>										
										<div class="col-md-12">
											<label>Have you undergone any special investigation
												or laboratory tests or ever had a surgical
												operation?
											</label>
											<br/>
											<div class="radio radio-inline">
											  
											    <input type="radio" name="hasLabSurgicalOps" name="hasLabSurgicalOpsYes" value="Yes" /><label> Yes
											  </label>
											</div>
											<div class="radio radio-inline">
											  
											    <input type="radio" name="hasLabSurgicalOps" name="hasLabSurgicalOpsNo" value="No" checked="checked" /><label> No
											  </label>
											</div>												
										</div>											
										<div class="col-md-12">
											<label>
												Are you currently taking prescribed drugs,
												medicines, tablets or other treatment?
											</label>
											<br/>
											<div class="radio radio-inline">
											 
											    <input type="radio" name="takingPrescribedDrugs" value="Yes" /> <label> Yes
											  </label>
											</div>
											<div class="radio radio-inline">
											    <input type="radio" name="takingPrescribedDrugs" value="No" checked="checked" /> <label> No
											  </label>
											</div>												
										</div>											
										<div class="col-md-12">
											<label>Have you any intentions or prospect of engaging in any
													hazardous sports or other activities? Have you received
													medical advice or treatment in respect of AIDS of HIV related
													condition or any sexually transmitted disease?
											</label>
											<br/>
											<div class="radio radio-inline">
											    <input type="radio" name="intentHazardSptSTD" id="intentHazardSptSTDYes" value="Yes" /> <label> Yes
											  </label>
											</div>
											<div class="radio radio-inline">
											    <input type="radio" name="intentHazardSptSTD" id="intentHazardSptSTDNo" checked="checked" value="No"/> <label> No
											  </label>
											</div>												
										</div>											
										<div class="col-md-12">
											<label>
												Have you received medical advice or treatment in respect of
												Anaemia, Asthma, Tuberculosis, Ulcer, Syphilis, Rheumatism,
												Heart Disease, High Blood Pressure, Piles, Sickle Cell, Jaundice,
												Bilharzia, Leprosy, Chest Pain, Fit, Diabetes or any contagious
												disease?
											</label>
											<div class="radio radio-inline">
											  <input type="radio" name="hasMedicalTreatment" id="hasMedicalTreatmentYes" value="Yes" /> <label> Yes
											  </label>
											</div>
											<div class="radio radio-inline">
											    <input type="radio" name="hasMedicalTreatment" id="hasMedicalTreatmentNo" checked="checked" value="No"/> <label> No
											  </label>
											</div>												
										</div>											
										<div class="col-md-12">
											<label>Are you free from any physical deformity or defect of speech,
													vision and hearing?
											</label>
											<br/>
											<div class="radio radio-inline">
											    <input type="radio" name="hasPhysicalDeformity" id="hasPhysicalDeformityYes" value="Yes"/> <label> Yes
											  </label>
											</div>
											<div class="radio radio-inline">
											    <input type="radio" name="hasPhysicalDeformity" id="hasPhysicalDeformityNo" checked="checked" value="No"/> <label> No
											  </label>
											</div>												
										</div>											
										<div class="col-md-12">
											<label>Are there any other medical problem(s) relevant to the
												assessment of the risk?
											</label><br/>
											<div class="radio">
											    <input type="radio" name="hasOtherMedicalProb" id="hasOtherMedicalProbYes" value="Yes" /> <label> Yes
											  </label>
											</div>
											<div class="radio">
										 <label> 
											    <input type="radio" name="hasOtherMedicalProb" id="hasOtherMedicalProbNo" checked="checked" value="No"/>No
											  </label>
											</div>												
										</div>																												
									</div>
								</fieldset>           			            			            					            			          				            		
		            		</sf:form>    
		            		</div> 		
		            		</c:when>
		            		<c:when test="${prospect.getInsuranceType() eq 'auto' }">

		            		</c:when>
		            		<c:otherwise>
		            		<div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
		            		<!-- Personal Information tab -->
		            		<h1>Personal Information</h1>
		            		<fieldset>
		            			<h2>Personal Information</h2>
								<div class="form-group">
		            				<div class="row">
		            					<div class="col-md-6">
				            				<label for="firstname">First name:</label>
				            				<input type="text" name="txtfirstname" id="txtfirstname" class="form-control" value="${prospect.getFirstname() }"/>		            					
		            					</div>
		            					<div class="col-md-6">
				            				<label for="lastname">Last name:</label>
				            				<input type="text" name="txtlastname" id="txtlastname" class="form-control"value="${prospect.getLastname() }"/>		            					
		            					</div>		            					
		            				</div>
		            			</div>
		            			<div class="form-group">
		            				<div class="row">
		            				<div class="col-md-6">
		            					<label for="selGender">Gender</label>
		            					<select id="custGender"  name="custGender"onchange="javascript:customerGender(this.value);" class="form-control">
		            						<option value="">Select a Gender</option>
		            						<option value="male">Male</option>
		            						<option value="female">Female</option>
		            					</select>
		            				</div>
		            				<div class="col-md-6">
		            					<label for="selDOB"> Date Of Birth: </label>
		            					<input type="text" class="form-control" name="dateOfBirth" id="dateOfBirth" onclick="javascript:getDateOfBirth(this);" value="${prospect.getDateOfBirth() }"/>
		            				</div>
		            				</div>		            				
		            			</div> 		            			
								<div class="form-group">
				            		<label for="firstname">Address 1:</label>
				            		<input type="text" name="txtaddress1" id="txtaddress1" class="form-control" value="${prospect.getAddress1() }"/>	
				            		<br>
				            		<label for="firstname">Address 2: <span class="help-text">Optional</span></label>
				            		<input type="text" name="txtaddress2" id="txtaddress2" class="form-control" value="${prospect.getAddress2() }"/>		
		            			</div>	
		            			<div class="form-group">
									<div class="row">
		            					<div class="col-md-6">
				            				<label for="txtfirstname">City:</label>
				            				<input type="text" name="txtcity" id="txtcity" class="form-control" value="${prospect.getCity() }"/>		            					
		            					</div>
		            					<div class="col-md-6">
				            				<label for="txtregion">Region:</label>
				            				<input type="text" name="txtregion" id="txtregion" class="form-control" value="${prospect.getRegion() }"/>		            					
		            					</div>		            					
		            				</div>		            			
		            			</div>
		            			<div class="form-group">
									<div class="row">
		            					<div class="col-md-6">
				            				<label for="txtphoneNumber">Phone Number:</label>
				            				<input type="text" name="txtphoneNumber" id="txtphoneNumber" class="form-control" value="${prospect.getPhoneNumber() }"/>	
				            				 <div class="form-group  ">  				
												<div class="radio">
												    <input type="radio" name="phoneType" id="phoneTypeHome" value="home" ${prospect.getPhoneType() == 'home' ? 'checked' : '' }/>
												    <label>
												    	Home Phone
												  	</label>
												</div>
												<div class="radio">
												    <input type="radio" name="phoneType" id="phoneTypeWork" value="work" ${prospect.getPhoneType() == 'work' ? 'checked' : '' }/>
												    <label>
												    	Work Phone
												  	</label>
												</div>
												<div class="radio">
												    <input type="radio" name="phoneType" checked="checked" id="phoneTypeMobile" value="mobile" ${prospect.getPhoneType() == 'mobile' ? 'checked' : '' }/>
												    <label for="phoneTypeMobile">
												    	Mobile Phone
												  	</label>
												</div>
											</div>               					
		            					</div>
		            					<div class="col-md-6">
		            						<div class="col-md-12">
				            					<label for="txtemailAddress">Email Address:</label>
				            					<input type="email" name="txtEmailAddress" id="txtemailAddress" class="form-control" value="${prospect.getEmailAddress()}"/>	
				            				</div>           					
		            					</div>			            					
		            				</div>	
		            			</div>				            			 		            				
		            			</fieldset>
		            			</div>
		            		</c:otherwise>		            		
		            	</c:choose>
		            </div>
		        </div>
		    </div>		
	</div>

</div>


</body>
    <div class="footer-contaner">
		<jsp:include page="../footer.jsp"/>
    </div>
    <script src="<c:url value="/resources/js/plugins/staps/jquery.steps.min.js"/>"></script>
    <script src="<c:url value="/resources/js/plugins/validate/jquery.validate.min.js"/>"></script>
    <script src="<c:url value="/resources/js/NewInsurance.js"/>"></script>
    <script src="<c:url value="/resources/js/application/prospectiveTemplate.js"/>" type="text/babel">

	</script>
</html>
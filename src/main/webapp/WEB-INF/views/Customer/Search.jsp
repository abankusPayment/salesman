<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="../head.jsp"/>	
	<script src="<c:url value="/resources/js/application/customerSearch.js"/>" type="text/javascript"></script>
</head>
<body  class="top-navigation">
	<div id="wrapper">
		<jsp:include page="../header.jsp" />
		<div class="content-page">	
			<div class="content">
				<div class="container-fluid">	
					<div class="row">
			          	<div class="portlet portlet-default">
				            <div class="portlet-header">
				              <h4 class="portlet-title">
				                Advanced Search
				              </h4>
				            </div> <!-- /.portlet-header -->
							<div id="search-container" class="col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
					            <div class="portlet-body">
									<fieldset>
						            	<c:url value="/customer/advanceSearch" var="customerSearch"/>
							            <sf:form name="customerSearchForm" action="${customerSearch }" method="post"  class="form" id="customerSearchForm">
							            	<div class="row">
							            		<div class="col-md-4">
									            	<div class="form-group">
									            		<label>First Name</label>
									            		<input type="text" class="form-control" id="firstname" name="firstname" />
									            	</div>			            	
							            		</div>
							            		<div class="col-md-4">
									            	<div class="form-group">
									            		<label>Last Name</label>
									            		<input type="text" class="form-control" id="lastname" name="lastname" />
									            	</div>	            		
							            		</div>	
							            		<div class="col-md-4">
									            	<div class="form-group">
									            		<label>Business Name</label>
									            		<input type="text" class="form-control" name="businessName" />
									            	</div>	            		
							            		</div>							            		
							            	</div>
							            	<div class="row">         		         
							            		<div class="col-md-3">
									            	<div class="form-group">
									            		<label>City or Town or Village</label>
									            		<input type="text" class="form-control" name="city" />
									            	</div>	            		
							            		</div>
							            		<div class="col-md-3">
									            	<div class="form-group">
									            		<label>Region</label>
														<script>createRegionList("searchRegion");</script>
									            	</div>	            		
							            		</div>           		
							            		<div class="col-md-3">
									            	<div class="form-group">
									            		<label>Phone Number:</label>
									            		<input type="text" class="form-control" name="phoneNumber" />
									            	</div>	            		
							            		</div>
							            		<div class="col-md-3">
									            	<div class="form-group">
									            		<label>Email Address</label>
														<input type="text" class="form-control" name="emailAddress" >
									            	</div>	            		
							            		</div>							            			
							            	</div>
							            	<span id="helpBlock" class="help-block">To narrow your results, select at least a Region or enter the first initial or Business Name of the Customer(s) to search</span>
							            	<input type="submit" class="btn btn-success" id="btnCustomerSearch" name="btnCustomerSearch" value="Search"/>
							            </sf:form>           
							             	
						            </fieldset>
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div id="searchResults">
								<c:if test="${not empty message}">
									<div class="alert alert-info">
										${message }
									</div>
								</c:if>
							</div>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
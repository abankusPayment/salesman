<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
	<jsp:include page="../head.jsp"/>
</head>

<body  class="top-navigation">

<div id="wrapper">
	<div id="header">
		<jsp:include page="../header.jsp"/>
       	
 			<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2>Teams Board</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<c:url value="/" />" >Home</a>
                        </li>
                        <li>
                            <strong>Team</strong>
                        </li>
                        <li class="active">
                            <strong>Team Board</strong>
                        </li>                        
                    </ol>
                </div>
            </div>
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="wrapper wrapper-content">
			            <div class="col-lg-12">
			                        <div class="row">
			                            <div class="col-sm-4">
						                    <div class="ibox">
						                        <div class="ibox-title">
						                            <h5>IT-07 - Finance Team</h5>
						                        </div>
						                        <div class="ibox-content">
						                            <h4>Info about Design Team</h4>
						                            <p>
						                                Uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
						                            </p>
						                            <div>
						                                <span>Status of current project:</span>
						                                <div class="stat-percent">73%</div>
						                                <div class="progress progress-mini">
						                                    <div style="width: 73%;" class="progress-bar"></div>
						                                </div>
						                            </div>
						                            <div class="row  m-t-sm">
						                                <div class="col-sm-4">
						                                    <div class="font-bold">PROJECTS</div>
						                                    11
						                                </div>
						                                <div class="col-sm-4">
						                                    <div class="font-bold">RANKING</div>
						                                    6th
						                                </div>
						                                <div class="col-sm-4 text-right">
						                                    <div class="font-bold">BUDGET</div>
						                                    $560,105 <i class="fa fa-level-up text-navy"></i>
						                                </div>
						                            </div>
						
						                        </div>
						                    </div>                            
			                            </div>
			                            <div class="col-sm-4">
						                    <div class="ibox">
						                        <div class="ibox-title">
						                            <h5>IT-07 - Finance Team</h5>
						                        </div>
						                        <div class="ibox-content">
						                            <h4>Info about Design Team</h4>
						                            <p>
						                                Uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
						                            </p>
						                            <div>
						                                <span>Status of current project:</span>
						                                <div class="stat-percent">73%</div>
						                                <div class="progress progress-mini">
						                                    <div style="width: 73%;" class="progress-bar"></div>
						                                </div>
						                            </div>
						                            <div class="row  m-t-sm">
						                                <div class="col-sm-4">
						                                    <div class="font-bold">PROJECTS</div>
						                                    11
						                                </div>
						                                <div class="col-sm-4">
						                                    <div class="font-bold">RANKING</div>
						                                    6th
						                                </div>
						                                <div class="col-sm-4 text-right">
						                                    <div class="font-bold">BUDGET</div>
						                                    $560,105 <i class="fa fa-level-up text-navy"></i>
						                                </div>
						                            </div>
						
						                        </div>
						                    </div>                            
			                            </div>
			                            <div class="col-sm-4">
						                    <div class="ibox">
						                        <div class="ibox-title">
						                            <h5>IT-07 - Finance Team</h5>
						                        </div>
						                        <div class="ibox-content">
						                            <h4>Info about Design Team</h4>
						                            <p>
						                                Uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
						                            </p>
						                            <div>
						                                <span>Status of current project:</span>
						                                <div class="stat-percent">73%</div>
						                                <div class="progress progress-mini">
						                                    <div style="width: 73%;" class="progress-bar"></div>
						                                </div>
						                            </div>
						                            <div class="row  m-t-sm">
						                                <div class="col-sm-4">
						                                    <div class="font-bold">PROJECTS</div>
						                                    11
						                                </div>
						                                <div class="col-sm-4">
						                                    <div class="font-bold">RANKING</div>
						                                    6th
						                                </div>
						                                <div class="col-sm-4 text-right">
						                                    <div class="font-bold">BUDGET</div>
						                                    $560,105 <i class="fa fa-level-up text-navy"></i>
						                                </div>
						                            </div>
						
						                        </div>
						                    </div>                            
			                            </div>                                                        
			                        </div>
			                    </div>
			                </div>
            	</div>
			</div>           
        </div>
        
        <div class="footer-contaner">
			<jsp:include page="../footer.jsp"/>
        </div>

    </div>
</body>

</html>
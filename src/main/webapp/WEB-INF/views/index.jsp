<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

<head>
	<jsp:include page="head.jsp"/>
	<script src="<c:url value="/resources/js/application/index.js"/>" type="text/javascript"></script>

</head>

<body class="layout-fixed ">
<div id="wrapper">
<jsp:include page="header.jsp"/>

<!-- /.container -->
	<div class="content-page">

    <div class="container">

      <div class="row">

        <div class="col-sm-7 col-md-7">
          <div class="portlet portlet-boxed">

            <div class="portlet-header">
              <h4 class="portlet-title">
                Monthly Stats
              </h4>
            </div> <!-- /.portlet-header -->

            <div class="portlet-body">

              <div class="row">

                <div class="col-md-3 col-xs-6 text-center">
                  <div>
                    <h3><small><i class="fa fa-caret-up text-success"></i></small> &nbsp;$1486</h3>
                    <small class="text-muted">Total Earnings</small>
                  </div>
                </div> <!-- /.col -->

                <div class="col-md-3 col-xs-6 text-center">
                  <div>
                    <h3><small><i class="fa fa-caret-down text-danger "></i></small> &nbsp;$863</h3>
                    <small class="text-muted">Total Weekly Revenue</small>
                  </div>
                </div> <!-- /.col -->

                <div class="col-md-3 col-xs-6 text-center">
                  <div>
                    <h3><small><i class="fa fa-minus text-warning"></i></small> &nbsp;$622</h3>
                    <small class="text-muted">New Sources</small>
                  </div>
                </div> <!-- /.col -->

                <div class="col-md-3 col-xs-6 text-center">
                  <div>
                    <h3><small><i class="fa fa-caret-up text-success"></i></small> &nbsp;$458</h3>
                    <small class="text-muted">Gross Margin</small>
                  </div>
                </div> <!-- /.col -->

              </div> <!-- /.row -->

            </div> <!-- /.portlet-body -->

          </div> <!-- /.portlet -->


          <div class="row">
			<div class="col-md-12">
				<div class="portlet portlet-boxed">
	            <div class="portlet-body">
		            <fieldset>
		            	<legend>Find Customer</legend>
		            	<c:url value="/customer/advanceSearch" var="customerSearch"/>
			            <sf:form name="customerSearchForm" action="${customerSearch }" method="post"  class="form" id="customerSearchForm">
			            	<div class="row">
			            		<div class="col-md-6">
					            	<div class="form-group">
					            		<label>First Name</label>
					            		<input type="text" class="form-control" id="firstname" name="firstname" />
					            	</div>			            	
			            		</div>
			            		<div class="col-md-6">
					            	<div class="form-group">
					            		<label>Last Name</label>
					            		<input type="text" class="form-control" id="lastname" name="lastname" />
					            	</div>	            		
			            		</div>	
			            	</div>
			            		<div class="row">
			            		<div class="col-md-12">
					            	<div class="form-group">
					            		<label>Business Name</label>
					            		<input type="text" class="form-control" name="businessName" />
					            	</div>	            		
			            		</div>	  
			            		</div> 
			            		<div class="row">         		         
			            		<div class="col-md-6">
					            	<div class="form-group">
					            		<label>City or Town or Village</label>
					            		<input type="text" class="form-control" name="city" />
					            	</div>	            		
			            		</div>
			            		<div class="col-md-6">
					            	<div class="form-group">
					            		<label>Region</label>
										<script>createRegionList("searchRegion");</script>
					            	</div>	            		
			            		</div>	
			            		</div>
			            		<div class="row">            		
			            		<div class="col-md-6">
					            	<div class="form-group">
					            		<label>Phone Number:</label>
					            		<input type="text" class="form-control" name="phoneNumber" />
					            	</div>	            		
			            		</div>
			            		<div class="col-md-6">
					            	<div class="form-group">
					            		<label>Email Address</label>
										<input type="text" class="form-control" name="emailAddress" >
					            	</div>	            		
			            		</div>
			            		</div>
			            		
			            	<input type="submit" class="btn btn-success" id="btnCustomerSearch" name="btnCustomerSearch" value="Search"/>
			            </sf:form>           
			             	
		            </fieldset>
	            <!-- 
	              <a href="<c:url value="/insurance/proposal"/>" class="btn btn-success btn-jumbo btn-block "><i class="fa fa-line-chart" aria-hidden="true"></i>
	               &nbsp;New Sales Proposal</a>
	              <br>
	              <a href="javascript:;" class="btn btn-secondary btn-lg btn-block ">New Template</a>
	               -->
				  
	            </div> <!-- /.portlet-body -->
	          </div> <!-- /.portlet -->			
			</div>
          </div> <!-- /.row -->
        </div> <!-- /.col -->


		
        <div class="col-sm-5 col-md-5">
          <sec:authorize access="hasRole('ROLE_INSURANCEAGENT')">
		  <div class="row">
		  	<div class="col-sm-12 col-md-12">
				<div class="portlet portlet-boxed">
	            	<div class="portlet-body">
	            	<div id="searchContainer">
	            		<div id="adentAddressDiv"></div>
	            		<div id="adentAddressTxt"></div>
	            	</div>
	            	
					<script type="text/javascript">loadAgentLocation("${employeeSessionInstance.getEmployeeId()}")</script>
	            <!-- 
	              <a href="<c:url value="/insurance/proposal"/>" class="btn btn-success btn-jumbo btn-block "><i class="fa fa-line-chart" aria-hidden="true"></i>
	               &nbsp;New Sales Proposal</a>
	              <br>
	              <a href="javascript:;" class="btn btn-secondary btn-lg btn-block ">New Template</a>
	               -->
				  
	            	</div> <!-- /.portlet-body -->
	          	</div> <!-- /.portlet -->		  	
		  	</div>
		  </div>
		  </sec:authorize>
          <div class="row">
          	<div class="col-md-12">
          <div class="portlet portlet-boxed">

            <div class="portlet-header">
              <h4 class="portlet-title">
                Recent Messages
              </h4>
            </div> <!-- /.portlet-header -->

            <div class="portlet-body">

              <ul class="icons-list text-md">
                <li>
                  <i class="icon-li fa fa-comments-o text-success"></i>
                  <strong class="semibold">New Sale!</strong>
                  <br>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </li>
                <li>
                  <i class="icon-li fa fa-check-square text-secondary"></i>
                  <strong class="semibold">New Action!</strong>
                  <br>
                  Vestibulum iaculis felis eu eros pellentesque placerat.
                </li>
                <li>
                  <i class="icon-li fa fa-truck text-tertiary"></i>
                  <strong class="semibold">New Product!</strong>
                  <br>
                  Curabitur cursus nisl et mauris imperdiet porttitor.
                </li>
                <li>
                  <i class="icon-li fa fa-comments-o text-primary"></i>
                  <strong class="semibold">New Comment!</strong>
                  <br>
                  Vestibulum iaculis felis eu eros pellentesque placerat.
                </li>
                <li>
                  <i class="icon-li fa fa-comments-o text-primary"></i>
                  <strong class="semibold">New Comment!</strong>
                  <br>
                  Curabitur cursus nisl et mauris imperdiet porttitor.
                </li>
              </ul>

            </div> <!-- /.portlet-body -->

          </div> <!-- /.portlet -->               	
          	</div>
          </div>
        </div> <!-- /.col -->

      </div> <!-- /.row -->

    </div> <!-- /.container -->

  </div> 

</div>
    <div class="footer-contaner">
		<jsp:include page="footer.jsp"/>
    </div>
<script type="text/javascript">
displayToastAlert();
</script>


</body>

</html>
	$(document).ready(function(){
		$(".dynamicContainer").addClass('hidden');
		
		$("#insuranceContainer").steps({
            bodyTag: "fieldset",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                var form = $(this);


                // Disable validation on fields that are disabled or hidden.
                form.validate().settings.ignore = ":disabled,:hidden";

                // Start validation; Prevent going forward if false
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex)
            {

            },
            onFinishing: function (event, currentIndex)
            {
                var form = $(this);

                // Disable validation on fields that are disabled.
                // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                form.validate().settings.ignore = ":disabled";

                // Start validation; Prevent form submission if false
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                var form = $(this);

                // Submit form input
                form.submit();
            }
		}).validate({
            errorPlacement: function (error, element)
            {
                element.before(error);
            },
            rules: {
                txtfirstname: {
                    required: true
                },
                txtlastname: {
                	required: true
                },
                txtaddress1 : {
                	required: true
                },
                txtcity : {
                	required: true
                },
                txtregion: {
                	required: true
                },
                custGender : {
                	required :true
                },
                hasOtherMedicalProb: {
                	required :true
                },
                hasPhysicalDeformity : {
                	required : true
                },
                hasMedicalTreatment : {
                	required : true
                },
                intentHazardSptSTD : {
                	required: true
                },
                takingPrescribedDrugs : {
                	required : true
                },
                hasLabSurgicalOps : {
                	required: true
                },
                hasMentalIllnes : {
                	required: true
                },
                hasInsuranceWithUs : {
                	required: true
                },
                drinkAlcohol :{
                	required: true
                },
                customerWeight : {
                	required: true
                },
                numberOfCigarettesDaily : {
                	required: true
                },
                customerHeight : {
                	required: true
                },
                hometown : {
                	required: true
                },
                hobby: {
                	required: true
                },
                txtphoneNumber : {
                	required: true
                },
                txtEmailAddress : {
                	required: true
                },
                phoneType : {
                	required: true
                },
                selHsdIncome : {
                	required: true
                },
                selMaritalStatus : {
                	required : true
                },
                selHsdNumber : {
                	required: true
                },
                isSmoker : {
                	required : true
                }
            }
        });
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
		
	});
	
	function customerGender(value){
		if(!value){
			return false;
		}else{
			if(value === "female"){
				$("#hasBreastCancerCaesareanNo").prop("checked", true);
				$("#isPregnantNo").prop("checked", true);
				$("#hasBreastCancerCaesareanYes").attr("disabled",true);
				$("#isPregnantYes").attr("disabled",true);
			}else{
				$("#hasBreastCancerCaesareanNo").prop("checked", false);
				$("#isPregnantNo").prop("checked", false);		
				$("#hasBreastCancerCaesareanYes").removeAttr("disabled");
				$("#isPregnantYes").removeAttr("disabled");
			}
		}
	}
	/**
	*element  - Element Id to hide/show
	* action - boolean value ; if true then show else if false then hide
	**/
	function hideShowContainer(element,action){
		if(!element && !action){
			return;
		}
		if(action === true){
			$("#"+element).removeClass("hidden");
		}else if(action === false){
			$("#"+element).addClass("hidden");
		}
	}
	function getDateOfBirth(element){
		if(element){
			console.log(element);
            $('#'+element.id).datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });
		}
	}
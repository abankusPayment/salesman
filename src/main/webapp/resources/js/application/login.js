
$(document).ready(function(){
	$('#loginFormBtn').on('click',function(){
		validateForm();
	});
});

function validateForm(){
	$('#loginForm').bootstrapValidator({
//      live: 'disabled',
      message: 'This value is not valid',
      fields: {
    	  username: {
    		  message: 'The username is not valid',
              validators: {
                  notEmpty: {
                      message: 'The username is required and cannot be empty'
                  }
              }
          },
          password : { 
        	  validators: {
                  notEmpty: {
                      message: 'The password is required and cannot be empty'
                  }
              }        	  
          }
      }
	})        .on('success.form.bv', function(e) {
        // Prevent submit form
        e.preventDefault();

        	document.loginForm.submit();
    });
}
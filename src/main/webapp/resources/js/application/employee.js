$(document).ready(function(){
	$('#btnAddEmployeeSubmit').on('click',function(e){

		validateAndSubmitEmployee('formNewEmployee');
	});
	
	$('#btnAddNote').on('click',function(e){
		const form = document.prospectiveCustomerNoteForm;
		addNoteToCustomerAccount(form);
	});
	
	$('#smsDiv-open').on('click', function(){
		$('#smsDiv-modal').modal({
			keyboard: false,
			backdrop: false
		});
	});
	
	$('#emailDiv-open').on('click', function(){
		$('#emailDiv-modal').modal({
			keyboard: false,
			backdrop: false
		});
	});
});

function addNoteToCustomerAccount(form){
	if(form){
		const action = form.action;
		const formData = $('form').serialize();
		$.ajax({
			url: action,
			method: "post",
			data: formData,
			dataType: "json",
			beforeSend: function(data){
				console.log(data);
				$('#btnAddNote').addClass("disabled");
			},
			success: function(data){
			},
			error: function(err){
				console.log(err);
			},
			complete: function(data){
				$('#btnAddNote').removeClass("disabled");
			}				
		});
	}
}
function validateAndSubmitEmployee(formId){
	var form = document.formEmployee;
			$('#'+formId).bootstrapValidator({
				 message: 'This value is not valid',
			      fields: {
			    	  firstname: {
			    		  message: 'The username is not valid',
			              validators: {
			                  notEmpty: {
			                      message: 'The First name is required and cannot be empty'
			                  }
			              }
			          },
			          lastname : { 
			        	  validators: {
			                  notEmpty: {
			                      message: 'The Last name is required and cannot be empty'
			                  }
			              }        	  
			          },
			          middlename : { 
			        	  validators: {
		                      regexp: {
		                          regexp: /^[a-zA-Z-\.]+$/,
		                          message: 'Middle Name can only be an alphabetic character'
		                      }
			              }        	  
			          },	          
		              emailAddress: {
		                  validators: {
		                      notEmpty: {
		                          message: 'The email address is required and can\'t be empty'
		                      },
		                      emailAddress: {
		                          message: 'The input is not a valid email address'
		                      }
		                  }
		              },
		              'secPermissions[]': {
		                  validators: {
		                      notEmpty: {
		                          message: 'The Id Number is required and can\'t be empty'
		                      }
		                  }
		              },
		              'employeeRole[]': {
		                  validators: {
		                      notEmpty: {
		                          message: 'The Id Number is required and can\'t be empty'
		                      }
		                  }
		              },		              
		              address1: {
		            	  validators: {
			                  notEmpty: {
			                      message: 'Address 1 field is required and can\'t be empty'
			                  }    
		            	  }
		              },
		              city: {
		            	  validators: {
			                  notEmpty: {
			                      message: 'City field is required and can\'t be empty'
			                  }         
		            	  }
		              },
		              region : {
		            	  validators: {
			                  notEmpty: {
			                      message: 'Region field is required and can\'t be empty'
			                  }         	
		            	  }
		              },
		              phoneNumber : {
		            	  validators: {
			                  notEmpty: {
			                      message: 'Phone Number is required and can\'t be empty'
			                  },
			                  phone: {
			                      country: 'GH',
			                      message: 'The value is not valid %s phone number'
			                  }
		            	  }
		              }
			      }
				})  
	            .on('error.field.bv', function(e, data) {
	                //console.log('error.field.bv -->', data.element);
	            })
				.on('success.form.bv', function(e) {
			        // Prevent submit form
			        e.preventDefault();
			        submitForm(form);
			        			
				});
}
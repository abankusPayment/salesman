$(document).ready(function(){
	$('#btnCustomerSearch').on('click',function(){
		validateCustomerSearchForm('customerSearchForm');
	});
});
function validateCustomerSearchForm(formId){
	var form = document.formEmployee;
			$('#'+formId).bootstrapValidator({
				 message: 'This value is not valid',
			      fields: {
			    	  firstname: {
			    		  message: 'The First Name you entered is not valid',
			              validators: {
		                      regexp: {
		                          regexp: /^[a-zA-Z-\.]+$/,
		                          message: 'Middle Name can only be an alphabetic character'
		                      }			                  
			              }
			          },
			          lastname : { 
			        	  validators: {
		                      regexp: {
		                          regexp: /^[a-zA-Z-\.]+$/,
		                          message: 'Last Name can only be an alphabetic character'
		                      }			                  
			              }        	  
			          },
			          middlename : { 
			        	  validators: {
		                      regexp: {
		                          regexp: /^[a-zA-Z-\.]+$/,
		                          message: 'Middle Name can only be an alphabetic character'
		                      }
			              }        	  
			          },	          
		              emailAddress: {
		                  validators: {
		                      emailAddress: {
		                          message: 'The input is not a valid email address'
		                      }
		                  }
		              },
		              city: {
		            	  validators: {
		                      regexp: {
		                          regexp: /^[a-zA-Z ]+$/,
		                          message: 'Middle Name can only be an alphabetic character'
		                      }        
		            	  }
		              },
		              region : {
		            	  validators: {
		                      regexp: {
		                          regexp: /^[a-zA-Z ]+$/,
		                          message: 'Middle Name can only be an alphabetic character'
		                      }      	
		            	  }
		              },
		              phoneNumber : {
		            	  validators: {
			                  phone: {
			                      country: 'GH',
			                      message: 'The value is not valid %s phone number'
			                  }		                  
		            	  }
		              },
		              businessName: {
		            	  validators: {
		                      regexp: {
		                          regexp: /^[a-Z0-9-\.]+$/,
		                          message: 'Middle Name can only be an alphabetic character'
		                      }	        
		            	  }
		              },
		              currentState: {
		            	  validators: {
			                  notEmpty: {
			                      message: 'City field is required and can\'t be empty'
			                  }         
		            	  }
		              },		              
			      }
				})  
	            .on('error.field.bv', function(e, data) {
	                //console.log('error.field.bv -->', data.element);
	            })
				.on('success.form.bv', function(e) {
			        // Prevent submit form
			        e.preventDefault();
			        submitSearchForm(form);
			        			
				});
}

function submitSearchForm (form){
	$.ajax({
		url: "searchCustomer",
		method: "get",
		data: form,
		dataType: "json",
		beforeSend: function(data){
			$('#searchResult').html("");
			loader("show");
		},
		success: function(data){
		},
		error: function(err){
			console.log(err);
		},
		complete: function(data){
			loader("hide");
			populateResults(data);
		}				
	});
}

function populateResults(data){
	
}

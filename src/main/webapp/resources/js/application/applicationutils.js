function createRegionList(name){
	if(name && name !== undefined) {
		var region ="<select name='"+name+"' class='form-control'>";
	}else{
		var region ="<select name='region' class='form-control'>";
	}
	region +="<option value=''> Select a Region </option>";
	region +="<option value='AR'> Ashanti Region </option>";
	region +="<option value='BA'> Brong-Ahafo Region </option>";
	region +="<option value='CR'> Central Region </option>";
	region +="<option value='ER'> Eastern Region </option>";
	region +="<option value='GAR'> Greater Accra Region </option>";
	region +="<option value='NR'> Northern Region </option>";
	region +="<option value='UER'> Upper East Region </option>";
	region +="<option value='UWR'> Upper West Region </option>";
	region +="<option value='VR'> Volta Region </option>";
	region +="<option value='WR'> Western Region </option>";
	region +="</select>";	
	document.write(region);
}
function submitForm(form){
	if(form){
		form.submit();
	}
}

function toggleHideShowDivContainer(divContainerId,toggle){
	if(divContainerId && toggle){
		if(toggle === "show"){
			$('#'+divContainerId).show();
		}else if(toggle =="hide"){
			$('#'+divContainerId).hide();
		}
		
	}
}
function toggleDisableInput(divContainerId,toggle){
	if(divContainerId && toggle){
		if(toggle === "show"){
			$('#'+divContainerId).show();
		}else if(toggle =="hide"){
			$('#'+divContainerId).hide();
		}
		
	}
}
function displayToastAlert(){
	Command: toastr["success"]("You have successfully logged into Ketejia", "Welcome")

	toastr.options = {
			  "closeButton": false,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": false,
			  "positionClass": "toast-top-center",
			  "preventDuplicates": true,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
	}
}
function checkStatus(response) {
	  if (response.status >= 200 && response.status < 300) {
	    return response
	  } else {
	    var error = new Error(response.statusText)
	    error.response = response
	    throw error
	  }
}

function parseJSON(response) {
	  return response.json()
}
function post(url,parameters,async){
	fetch(url,{
		method: 'post',
		mode: 'no-cors',
		credentials: 'same-origin',
		headers: {
		    'Content-Type': 'application/json'
		 },
		body: JSON.stringify(parameters)
	}).then(function(response) {
		  if (response.ok) {
			    return response.json()
			  } else {
			    var error = new Error(response.statusText)
			    error.response = response
			    throw error
			  }
		})
		.then(function(data){
			
		}).catch(function(e){
			console.log(e);
		});
}

/**
function populateTable(mapData){
	var count = 0;
	var body;
	if(mapData.length > 0){
		console.log(mapData);
		$.each(mapData,function(key,value){
			var keys = count+1;
			var row = "<tr>";
			row +="<td>"+mapData[key]["fullname"]+"</td>";
			row +="<td>"+getInsuranceType(mapData[key]["insuranceType"])+"</td>";
			row +="<td> Phone Number: "+mapData[key]["phoneNumber"]+" Email Address: "+mapData[key]["emailAddress"]+"</td>";
			row +="<td> Email Address: "+mapData[key]["emailAddress"]+"</td>";
			row +="<td><a href=\"/salesman/sales/assignProspect/"+mapData[key]["prospectiveUniqueId"]+"\" class=\"btn btn-sm btn-success\" >View Details</a></td>";
			row +="</tr>";
			body +=row;
		});		
	}else{
		var row = "<tr>";
			row +="<td colspan='4'> No Prospective Customer was found for you.</td>";
			row +="</tr>";
			body +=row;
	}
	return body;
}

function populateErrorMessage(){
	var row = "<tr>";
	row +="<td colspan='4'>Your Search did not return results.</td>";
	row +="</tr>"
		
	return row;
}
function get(url,parameters){
	var tableData;
	$.ajax({
		url: url,
		method: "get",
		data: {employee: parameters},
		dataType: "json",
		success: function(data){
			if(data.result === true){
				tableData = populateTable(data.prospectiveInsuredList);
			}else{
				tableData = populateErrorMessage();
			}
		},
		error: function(err){
			tableData = populateErrorMessage();
		},
		complete: function(data){
			$('#salesDivContainer').html(tableData);
		}
	});

}
**/
function getInsuranceType(insurance){
	var insuranceType = "";
	switch(insurance){
		case "life":
			insuranceType = "Life Insurance";
			break;
		case "auto":
			insuranceType = "Auto Insurance";
			break;
	}
	return insuranceType;
}
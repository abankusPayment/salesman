function retrieveAgentsByLocation(location,url,prospect){
	if(location){
		$.ajax({
			url: "/salesman/sales/findAgentByLocation",
			method: "get",
			data: {customerLocation: location},
			dataType: "json",
			beforeSend: function(data){
				$('#searchResult').html("");
				loader("show");
			},
			success: function(data){
			},
			error: function(err){
				console.log(err);
			},
			complete: function(data){
				loader("hide");
				populateResults(data,url,prospect);
			}			
		});
	}else{
		return null;
	}
}

function populateResults(data,url,prospect){
	const response = data.responseJSON;
	if(response.agentDTO.length < 1){
		const message = displayErrorMessage("Your search returned 0 Insurance Agent(s).","")
		$('#searchResult').html(message);		
	}else if(response.result){
		const agentList = createBuildAssignmentCard(response,url,prospect);
		console.log(data);
		$('#searchResult').html(agentList);
	}else{
		const message = displayErrorMessage("Your search did not return any results....","")
		$('#searchResult').html(message);
		
	}
}

function displayErrorMessage(message,className){
	 var ui = "<div class='search-container page-header'>";
	 	ui +="<h2>Search Results</h2><hr /></div>";
	 	ui += "<div class='alert'"+className+"'>";
	  	ui +=""+message+"</div>";
	return ui;
}

function createBuildAssignmentCard(data,url,prospect){
	const agent = data.agentDTO;
	const len = agent.length;
	var ui= "<div class='search-container page-header'>";
		ui +="<h2>Search Results</h2><hr /></div>";
	for(i=0;i < len;i++){
		if(agent[i]){
			 ui +="<div class='col-md-3 gap-below-20'><div class='card'>";
			 ui +="<div class='container'>";
			 ui +="<h4><b>"+agent[i].fullName+"</b></h4>";
			 ui +="<p><b>Insurance: </b>"+agent[i].insuranceType+"</p>";
			 ui +="<address><strong>"+agent[i].fullName+"</strong><br>"+agent[i].address1+"<br>"+agent[i].city+","+agent[i].region+"<br></address>";
			 ui +="<p><b>Email Address: </b>"+agent[i].emailAddress+"</p>";
			 ui +="<p><b>Phone Number: </b>"+agent[i].phoneNumber+"</p>";
			 ui +="<input type='hidden' id='prospectiveIdHidden' value='"+prospect+"' /><input type='hidden' id='agentIdHidden' value='"+agent[i].agentId+"' />";
			 ui +="<div><hr/><a href='javascript:void(0);' class='btn btn-success' onclick='javascript:saveSelectedAgent();'>Select</a></div>";
			 ui +="</div>";	
			 
			 ui +="</div></div>";
		}
	}
	ui+= "</div>";
	return ui;
}
function test(){
	console.log("test");
}

function loader(state){
	const container = $('#search-container');
	
	if(state == "hide"){
		container.removeClass("loader");
	}else{
		container.addClass("loader");
	}
}

function saveSelectedAgent(agent,prospective){
		var agentId = $('#agentIdHidden').val();
		var prospective = $('#prospectiveIdHidden').val();
		$('#agentId').val(agentId);
		var frm = $('#agentProspectiveForm');
		frm.submit();

}
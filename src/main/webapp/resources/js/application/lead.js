function validateProspectiveForm(action){
	var formAction = action;
	$('#newProposalForm').bootstrapValidator({
		 message: 'This value is not valid',
	      fields: {
	    	  txtfirstname: {
	    		  message: 'The username is not valid',
	              validators: {
	                  notEmpty: {
	                      message: 'The First name is required and cannot be empty'
	                  }
	              }
	          },
	          txtlastname : {
	        	  validators: {
	                  notEmpty: {
	                      message: 'The Last name is required and cannot be empty'
	                  }
	              }
	          },
	          txtmiddlename : {
	        	  validators: {
                      regexp: {
                          regexp: /^[a-zA-Z-\.]+$/,
                          message: 'Middle Name can only be an alphabetic character'
                      }
	              }
	          },
	          txtEmailAddress: {
                  validators: {
                      notEmpty: {
                          message: 'The email address is required and can\'t be empty'
                      },
                      emailAddress: {
                          message: 'The input is not a valid email address'
                      }
                  }
              },
              typeOfInsurance: {
                  validators: {
                      notEmpty: {
                          message: 'Type of Insurance is required and can\'t be empty'
                      }
                  }
              },
              leadGender: {
                  validators: {
                      notEmpty: {
                          message: 'The Gender or Sex is required and can\'t be empty'
                      }
                  }
              },
              phoneType : {
            	  validators: {
                      notEmpty: {
                          message: 'Select the type of Phone used by Customer.'
                      }
            	  }
              },
              txtaddress1: {
            	  validators: {
	                  notEmpty: {
	                      message: 'Address 1 field is required and can\'t be empty'
	                  }
            	  }
              },
              txtcity: {
            	  validators: {
	                  notEmpty: {
	                      message: 'City field is required and can\'t be empty'
	                  }
            	  }
              },
              txtregion : {
            	  validators: {
	                  notEmpty: {
	                      message: 'Region field is required and can\'t be empty'
	                  }
            	  }
              },
              txtphoneNumber : {
            	  validators: {
	                  notEmpty: {
	                      message: 'Phone Number is required and can\'t be empty'
	                  },
	                  phone: {
	                      country: 'GH',
	                      message: 'The value is not valid %s phone number'
	                  }
            	  }
              }
	      }
		})        .on('success.form.bv', function(e) {
	        // Prevent submit form
			//e.preventDefault();
			var form = document.newProposalForm;
			form.navigator.value = formAction;
	        submitForm(form);

	});
}
$(document).ready(function(){
	$('#btnSubmitClose').on('click',function(e){
		validateProspectiveForm("close");
	});
	
	$('#btnSubmitContinue').on('click',function(e){
		validateProspectiveForm("continue");
	});
});

function getProspectBySalesRep(empId,state){
	if(empId){
		var data = {
				employeeId: empId,
				currentState: state
		};
		const resp = get("getSalesList",data);
	}
}
function populateTable(mapData){
	var count = 0;
	var body;
	if(mapData.length > 0){
		console.log(mapData);
		$.each(mapData,function(key,value){
			var keys = count+1;
			var row = "<tr>";
			row +="<td>"+mapData[key]["fullname"]+"</td>";
			row +="<td>"+getInsuranceType(mapData[key]["insuranceType"])+"</td>";
			row +="<td> Phone Number: "+mapData[key]["phoneNumber"]+" Email Address: "+mapData[key]["emailAddress"]+"</td>";
			row +="<td> Email Address: "+mapData[key]["emailAddress"]+"</td>";
			row +="<td> "+mapData[key]["dateEnquired"]+"</td>";
			row +="<td><a href=\"/salesman/customer/viewProspectiveProfile/"+mapData[key]["prospectiveUniqueId"]+"\" class=\"btn btn-sm btn-success\" >View Details</a></td>";
			row +="</tr>";
			body +=row;
		});		
	}else{
		var row = "<tr>";
			row +="<td colspan='5'> No Prospective Customer was found for you.</td>";
			row +="</tr>";
			body +=row;
	}
	return body;
}
function getInsuranceType(insurance){
	var insuranceType = "";
	switch(insurance){
		case "life":
			insuranceType = "Life Insurance";
			break;
		case "auto":
			insuranceType = "Auto Insurance";
			break;
	}
	return insuranceType;
}
function populateErrorMessage(message){
	var row = "<tr>";
	row +="<td colspan='5'>"+message+"</td>";
	row +="</tr>"
		
	return row;
}
function get(url,parameters){
	var tableData;
	$.ajax({
		url: url,
		method: "get",
		data: parameters,
		dataType: "json",
		success: function(data){
			if(data.result === true){
				tableData = populateTable(data.prospectiveInsuredList);
			}else{
				tableData = populateErrorMessage("Your Search did not return results.");
			}
		},
		error: function(err){
			tableData = populateErrorMessage("Your Search did not return results.");
		},
		complete: function(data){
			$('#salesDivContainer').html(tableData);
		}
	});

}

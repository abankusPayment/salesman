$(document).ready(function(){
	$('#btnCustomerSearch').on('click',function(e){

		validateAndSubmitCustomerSearch('customerSearchForm');
	});
});

function loadAgentLocation(employeeId){
	if(employeeId){
		const request = {
				employeeId: employeeId
		}
		let address = "";
		$.ajax({
			url: 'findAgentLocation',
			method: 'get',
			data: request,
			dataType: 'json',
			beforeSend: function(){
				$('#adentAddressDiv').addClass("sk-spinner sk-spinner-pulse");
				$('#adentAddressTxt').text("Search for Address and Contact Information");
			},
			success: function(response){
				if(response.result){
					$('#adentAddressTxt').text("Building Address and Contact Information");
					address = buildAgentLocationUI(response.agent);
					
				}
				console.log(response);
				
			},
			error: function(err){
				console.log(err);
				address = "Could not retrieve your Agency Address..";
			},
			complete: function(data){
				$('#adentAddressDiv').removeClass("sk-spinner sk-spinner-pulse");
				$('#adentAddressTxt').text('');
				$('#adentAddressDiv').html(address);
			}
		});
	}
}

function buildAgentLocationUI(data){
	if(data){
		let ui ="";
		ui +="<address>";
		ui +="<strong>"+data.address1+"</strong><br/>";
		ui +=""+data.address2+"<br/>";
		ui +=""+data.city+" "+data.region+"<br/>";
		ui +="</address>";
		return ui;
	}
}

validateAndSubmitCustomerSearch = (formId) => {
	var form = document.customerSearchForm;
	$('#'+formId).bootstrapValidator({
		 message: 'This value is not valid',
	      fields: {
	    	  firstname: {
	              validators: {
                      regexp: {
                          regexp: /^[a-zA-Z-']+$/,
                          message: 'Middle Name can only be an alphabetic character'
                      }
	              }
	          },
	          lastname : { 
	        	  validators: {
                      regexp: {
                          regexp: /^[a-zA-Z- \.]+$/,
                          message: 'Middle Name can only be an alphabetic character'
                      }
	              }        	  
	          },	
	          businessName : { 
	        	  validators: {
                      regexp: {
                          regexp: /^[a-zA-Z0-9-\. ]+$/,
                          message: 'Middle Name can only be an alphabetic character'
                      }
	              }        	  
	          },	          
              emailAddress: {
                  validators: {
                      emailAddress: {
                          message: 'The input is not a valid email address'
                      }
                  }
              },
              city: {
            	  validators: {
                      regexp: {
                          regexp: /^[a-zA-Z0-9- ]+$/,
                          message: 'Middle Name can only be an alphabetic character'
                      }        
            	  }
              },
              searchRegion : {
            	  validators: {
                      regexp: {
                          regexp: /^[a-zA-Z ]+$/,
                          message: 'Middle Name can only be an alphabetic character'
                      }       	
            	  }
              },
              phoneNumber : {
            	  validators: {
	                  phone: {
	                      country: 'GH',
	                      message: 'The value is not valid %s phone number'
	                  }
            	  }
              }
	      }
		})  
        .on('error.field.bv', function(e, data) {
        })
		.on('success.form.bv', function(e) {
			$('#btnCustomerSearch').addClass('hidden');	
	        form.submit();
	        		
		});
}
$(document).ready(function(){
	$('#btnSubmitLead').on('click',function(e){
		e.preventDefault();
			$('#customerForm').bootstrapValidator({
				 message: 'This value is not valid',
			      fields: {
			    	  firstname: {
			    		  message: 'The username is not valid',
			              validators: {
			                  notEmpty: {
			                      message: 'The First name is required and cannot be empty'
			                  }
			              }
			          },
			          lastname : { 
			        	  validators: {
			                  notEmpty: {
			                      message: 'The Last name is required and cannot be empty'
			                  }
			              }        	  
			          },
			          middlename : { 
			        	  validators: {
		                      regexp: {
		                          regexp: /^[a-zA-Z-\.]+$/,
		                          message: 'Middle Name can only be an alphabetic character'
		                      }
			              }        	  
			          },	          
		              emailAddress: {
		                  validators: {
		                      notEmpty: {
		                          message: 'The email address is required and can\'t be empty'
		                      },
		                      emailAddress: {
		                          message: 'The input is not a valid email address'
		                      }
		                  }
		              },
		              IdNumber: {
		                  validators: {
		                      notEmpty: {
		                          message: 'The Id Number is required and can\'t be empty'
		                      }
		                  }
		              },         
		              IdType: {
		                  validators: {
		                      notEmpty: {
		                          message: 'The Id Number is required and can\'t be empty'
		                      }
		                  }
		              },  
		              company_name : {
		            	  validators: {
		                      regexp: {
		                          regexp: /^[a-zA-Z0-9-\.]+$/,
		                          message: 'Company Name can only contain alphanumeric, underscore and a dot characters'
		                      }            		  
		            	  }
		              },
		              address1: {
		            	  validators: {
			                  notEmpty: {
			                      message: 'Address 1 field is required and can\'t be empty'
			                  }    
		            	  }
		              },
		              city: {
		            	  validators: {
			                  notEmpty: {
			                      message: 'City field is required and can\'t be empty'
			                  }         
		            	  }
		              },
		              region : {
		            	  validators: {
			                  notEmpty: {
			                      message: 'Region field is required and can\'t be empty'
			                  }         	
		            	  }
		              },
		              phoneNumber : {
		            	  validators: {
			                  notEmpty: {
			                      message: 'Phone Number is required and can\'t be empty'
			                  },
			                  phone: {
			                      country: 'Ghana',
			                      message: 'The value is not valid %s phone number'
			                  }
		            	  }
		              },
		              customerIndustry : {
		            	  validators: {
		            		  notEmpty : {
		            			  message: 'Customer Industry must be selected.'
		            		  }
		            	  }
		              },
		              accountStatus : {
		            	  validators: {
			                  notEmpty: {
			                      message: 'Account Status is required and can\'t be empty'
			                  }           		  
		            	  }
		              }
			      }
				})        .on('success.form.bv', function(e) {
			        // Prevent submit form
			        e.preventDefault();
			        submitForm(document.newProposalForm);
			        			
			});
	});
});
package com.protecksoftware.salesman.domain;

public class AgentGeoLocation {
	private final String longitude;
	
	private final String latitude;

	
	public AgentGeoLocation(String longitude, String latitude) {
		super();
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getLatitude() {
		return latitude;
	}
	
	
}

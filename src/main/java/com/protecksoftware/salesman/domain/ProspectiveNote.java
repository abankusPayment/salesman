package com.protecksoftware.salesman.domain;

import java.sql.Timestamp;

import javax.persistence.*;

/**
 * Bean for Employee to store information about a Prospective Customer
 * 
 * This object hold information about an employee encounter with
 * a Prospective Customer or some information pertinent to the Customer
 * 
 * @author abankwah
 *
 */
public class ProspectiveNote {

	private Employee employee;
	
	private ProspectiveInsured prospectiveInsured;
	
	@Lob
	private String notes;
	
	private Timestamp dateOfNote;
	
	private int prospectiveNoteId;

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public ProspectiveInsured getProspectiveInsured() {
		return prospectiveInsured;
	}

	public void setProspectiveInsured(ProspectiveInsured prospectiveInsured) {
		this.prospectiveInsured = prospectiveInsured;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Timestamp getDateOfNote() {
		return dateOfNote;
	}

	public void setDateOfNote(Timestamp dateOfNote) {
		this.dateOfNote = dateOfNote;
	}

	public int getProspectiveNoteId() {
		return prospectiveNoteId;
	}

	public void setProspectiveNoteId(int prospeciveNote) {
		this.prospectiveNoteId = prospeciveNote;
	}
	
	
}

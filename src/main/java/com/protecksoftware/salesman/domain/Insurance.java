package com.protecksoftware.salesman.domain;

public interface Insurance {

     public String getInsuranceNumber();
     
	 public String getInsuranceType();
	 
	 public String getEffectiveDate();
	 
	 public void setInsuranceNumber(String insuranceNumber);
}

package com.protecksoftware.salesman.domain;

import java.io.Serializable;

public class Vehicle  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int vehicleId;
	
	private String vehicleMake;
	
	private String model;
	
	private String modelYear;
	
	private String registrationNumber;

	public int getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getModelYear() {
		return modelYear;
	}

	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	
	
}

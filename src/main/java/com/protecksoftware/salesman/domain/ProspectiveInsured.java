package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import javax.persistence.*;

import com.protecksoftware.salesman.application.factory.Genesis10;

import java.math.BigInteger;
import java.util.List;


/**
 * The persistent class for the prospectiveInsured database table.
 * 
 */
@Entity
public class ProspectiveInsured implements Serializable, Genesis10 {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int prospectiveId;

	@Lob
	private String comments;

	private String dateEnquired;

	private String firstname;

	private String insuranceType;

	private String lastname;

	private String middlename;

	private String prospectiveUniqueId;
	
	private String currentState;
	
	private Address addresses;

	private Email email;

	private Phone phone;

	/**
	 * Employee is the Sales Rep or Staff 
	 * that saved Prospective Customer 
	 */
	private Employee employee;
	
	public ProspectiveInsured() {
	}

	public int getProspectiveId() {
		return this.prospectiveId;
	}

	public void setProspectiveId(int prospectiveId) {
		this.prospectiveId = prospectiveId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDateEnquired() {
		return this.dateEnquired;
	}

	public void setDateEnquired(String dateEnquired) {
		this.dateEnquired = dateEnquired;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getInsuranceType() {
		return this.insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return this.middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getProspectiveUniqueId() {
		return this.prospectiveUniqueId;
	}

	public void setProspectiveUniqueId(String prospectiveUniqueId) {
		this.prospectiveUniqueId = prospectiveUniqueId;
	}


	public Address getAddresses() {
		return this.addresses;
	}

	public void setAddresses(Address addresses) {
		this.addresses = addresses;
	}

	public Address addAddress(Address address) {
		setAddresses(address);
		return address;
	}

	
	public Email getEmail() {
		return this.email;
	}

	public void setEmail(Email emails) {
		this.email = emails;
	}

	public Email addEmail(Email email) {
		setEmail(email);

		return email;
	}

	public Phone getPhone() {
		return this.phone;
	}

	public void setPhones(Phone phones) {
		this.phone = phones;
	}

	public Phone addPhone(Phone phone) {
		setPhones(phone);
		return phone;
	}



	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getFirstname());
		if(getMiddlename() !=null){
			builder.append(" ");
			builder.append(getMiddlename());
		}
		builder.append(" ");
		builder.append(getLastname());		
		return builder.toString();
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public String getCurrentState() {
		return currentState;
	}

	public void String(String currentState) {
		this.currentState = currentState;
	}

	
	
}
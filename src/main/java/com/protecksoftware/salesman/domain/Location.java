package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.search.annotations.Latitude;
import org.hibernate.search.spatial.Coordinates;


/**
 * The persistent class for the location database table.
 * 
 */
public class Location implements Serializable,Coordinates {
	private static final long serialVersionUID = 1L;

	private int locationId;

	private String geospatial;
	
	@Latitude(of="location")
	private Double latitude;
	
	@Latitude(of="location")
	private Double longitude;	
	
	/**
	//bi-directional one-to-one association to InsuranceAgent
	@OneToOne
	@JoinColumn(name="locationId", referencedColumnName="locationId")
	private InsuranceAgent insuranceAgent;

	private Address agentAddress;
		**/
	public Location() {
	}

	public int getLocationId() {
		return this.locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getGeospatial() {
		return this.geospatial;
	}

	public void setGeospatial(String geospatial) {
		this.geospatial = geospatial;
	}

	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	/**
	public InsuranceAgent getInsuranceAgent() {
		return this.insuranceAgent;
	}

	public void setInsuranceAgent(InsuranceAgent insuranceAgent) {
		this.insuranceAgent = insuranceAgent;
	}

	public Address getAgentAddress() {
		return agentAddress;
	}

	public void setAgentAddress(Address agentAddress) {
		this.agentAddress = agentAddress;
	}
	**/
}
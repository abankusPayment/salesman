package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
public class Position implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int positionId;

	private String details;

	private String positionName;


	public Position() {
	}

	public int getPositionId() {
		return this.positionId;
	}

	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getPositionName() {
		return this.positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

}
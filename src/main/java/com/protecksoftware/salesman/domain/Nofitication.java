package com.protecksoftware.salesman.domain;

import java.time.LocalDateTime;

/**
 * Same as Email message.
 * @author abankwah
 *
 */
public class Nofitication {

	private int notificationId;
	
	private String sentBy;
	
	private String sentTo;
	
	private String message;
	
	private boolean messageRead;

	/**
	 * Removes the notification from the
	 * view of the Employee
	 */
	private boolean deleted;
	
	private LocalDateTime dateCreated;
	
	public int getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}

	public String getSentBy() {
		return sentBy;
	}

	public void setSentBy(String sentBy) {
		this.sentBy = sentBy;
	}

	public String getSentTo() {
		return sentTo;
	}

	public void setSentTo(String sentTo) {
		this.sentTo = sentTo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isMessageRead() {
		return messageRead;
	}

	public void setMessageRead(boolean messageRead) {
		this.messageRead = messageRead;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
}

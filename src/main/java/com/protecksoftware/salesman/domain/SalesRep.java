package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
public class SalesRep  extends Employee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String salesRepNumber;
	
	private Timestamp dateCreated;

	
	public String getSalesRepNumber() {
		return salesRepNumber;
	}

	public void setSalesRepNumber(String salesRepNumber) {
		this.salesRepNumber = salesRepNumber;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public SalesRep() {
	}

}

package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import javax.persistence.*;



/**
 * The persistent class for the phone database table.
 * 
 */
@Entity
public class Phone implements Serializable, com.protecksoftware.salesman.application.factory.Genesis10 {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int phoneId;

	private String countrycode;

	private String phoneNumber;

	private String phoneType;

	private boolean primaryPhone;

	public Phone() {
	}

	public int getPhoneId() {
		return this.phoneId;
	}

	public void setPhoneId(int phoneId) {
		this.phoneId = phoneId;
	}

	public String getCountrycode() {
		return this.countrycode;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneType() {
		return this.phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public boolean getPrimaryPhone() {
		return this.primaryPhone;
	}

	public void setPrimaryPhone(boolean primaryPhone) {
		this.primaryPhone = primaryPhone;
	}


}
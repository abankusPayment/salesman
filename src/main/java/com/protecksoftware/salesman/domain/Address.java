package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import javax.persistence.*;

import com.protecksoftware.salesman.domain.ProspectiveInsured;


/**
 * The persistent class for the address database table.
 * 
 */
@Table
@Entity
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int addressId;

	private String address1;

	private String address2;

	private String addressType;

	private String city;

	private String country;

	private String region;
	
	public Address() {
	}

	public int getAddressId() {
		return this.addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddressType() {
		return this.addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	
}
package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the employee database table.
 * 
 */
@Entity
public class Employee implements Serializable,Comparable<Employee> {
	private static final long serialVersionUID = 1L;
	
	@Id
	private int employeeId;

	private String address1;

	private String address2;

	private String city;

	private int companyId;

	private String emailAddress;

	private String employeeNumber;

	private String firstname;

	private String gender;

	private String lastname;

	private String middlename;

	private String phoneNumber;

	private String region;

	private Position position;	

	private InsuranceAgent insuranceAgent;

	private List<InsuredState> insuredStates;


	private List<ProspectiveInsured> prospectiveInsureds;

	private List<AgentSalesMessage> agentSalesMessages;

	@Transient
	private String employeeType;
	
	public Employee() {
	}

	public int getEmployeeId() {
		return this.employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmployeeNumber() {
		return this.employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return this.middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Position getPosition() {
		return this.position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getFirstname());
		if(getMiddlename() !=null){
			builder.append(" ");
			builder.append(getMiddlename());
		}
		builder.append(" ");
		builder.append(getLastname());		
		return builder.toString();
	}

	@Override
	public int compareTo(Employee o) {
		return this.compareTo(o);
	}

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}
	
	public InsuranceAgent getInsuranceAgent() {
		return this.insuranceAgent;
	}

	public void setInsuranceAgent(InsuranceAgent insuranceAgent) {
		this.insuranceAgent = insuranceAgent;
	}

	public List<InsuredState> getInsuredStates() {
		return this.insuredStates;
	}

	public void setInsuredStates(List<InsuredState> insuredStates) {
		this.insuredStates = insuredStates;
	}

	public InsuredState addInsuredState(InsuredState insuredState) {
		getInsuredStates().add(insuredState);
		insuredState.setEmployeeId(this);

		return insuredState;
	}

	public InsuredState removeInsuredState(InsuredState insuredState) {
		getInsuredStates().remove(insuredState);
		insuredState.setEmployeeId(null);

		return insuredState;
	}

	public List<ProspectiveInsured> getProspectiveInsureds() {
		return this.prospectiveInsureds;
	}

	public void setProspectiveInsureds(List<ProspectiveInsured> prospectiveInsureds) {
		this.prospectiveInsureds = prospectiveInsureds;
	}

	public ProspectiveInsured addProspectiveInsured(ProspectiveInsured prospectiveInsured) {
		getProspectiveInsureds().add(prospectiveInsured);
		prospectiveInsured.setEmployee(this);

		return prospectiveInsured;
	}

	public ProspectiveInsured removeProspectiveInsured(ProspectiveInsured prospectiveInsured) {
		getProspectiveInsureds().remove(prospectiveInsured);
		prospectiveInsured.setEmployee(null);

		return prospectiveInsured;
	}

	public List<AgentSalesMessage> getAgentSalesMessages() {
		return this.agentSalesMessages;
	}

	public void setAgentSalesMessages(List<AgentSalesMessage> agentSalesMessages) {
		this.agentSalesMessages = agentSalesMessages;
	}

	public AgentSalesMessage addAgentSalesMessage(AgentSalesMessage agentSalesMessage) {
		getAgentSalesMessages().add(agentSalesMessage);
		agentSalesMessage.setEmployee(this);

		return agentSalesMessage;
	}

	public AgentSalesMessage removeAgentSalesMessage(AgentSalesMessage agentSalesMessage) {
		getAgentSalesMessages().remove(agentSalesMessage);
		agentSalesMessage.setEmployee(null);

		return agentSalesMessage;
	}
}
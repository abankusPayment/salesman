package com.protecksoftware.salesman.domain;

public enum InsuranceType {
	AUTO_INSURANCE("auto"),
	
	LIFE_INSURANCE("life");
	
	InsuranceType(String insurance){
		this.insurance = insurance;
	}
	String insurance;
}

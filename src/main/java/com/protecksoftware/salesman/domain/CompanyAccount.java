package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the companyAccount database table.
 * 

@Entity
@Table(name="companyAccount")
@DynamicUpdate(value=true)
@NamedQuery(name="CompanyAccount.findAll", query="SELECT c FROM CompanyAccount c")
 */
public class CompanyAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int companyAccountId;

	//bi-directional one-to-one association to Company
	@OneToOne(mappedBy="companyAccount")
	private Company companyBean;

	public CompanyAccount() {
	}

	public int getCompanyAccountId() {
		return this.companyAccountId;
	}

	public void setCompanyAccountId(int companyAccountId) {
		this.companyAccountId = companyAccountId;
	}

	public Company getCompanyBean() {
		return this.companyBean;
	}

	public void setCompanyBean(Company companyBean) {
		this.companyBean = companyBean;
	}

}
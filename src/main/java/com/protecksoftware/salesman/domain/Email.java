package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the email database table.
 * 
 */
@Entity
public class Email implements Serializable, com.protecksoftware.salesman.application.factory.Genesis10 {
	private static final long serialVersionUID = 1L;

	@Id
	private int emailId;

	private String emailAddress;

	private String emailType;
	
	public Email() {
	}

	public int getEmailId() {
		return this.emailId;
	}

	public void setEmailId(int emailId) {
		this.emailId = emailId;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailType() {
		return this.emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
	
	
	

}
package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the messages database table.
 * 
 */
public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	private int messageId;

	private String dateSent;

	@Lob
	private String message;

	private String messageUUID;

	private byte read;

	private String subject;

	private int toEmployeeId;

	private AgentSalesMessage agentSalesMessage;

	public Message() {
	}

	public int getMessageId() {
		return this.messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public String getDateSent() {
		return this.dateSent;
	}

	public void setDateSent(String dateSent) {
		this.dateSent = dateSent;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageUUID() {
		return this.messageUUID;
	}

	public void setMessageUUID(String messageUUID) {
		this.messageUUID = messageUUID;
	}

	public byte getRead() {
		return this.read;
	}

	public void setRead(byte read) {
		this.read = read;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getToEmployeeId() {
		return this.toEmployeeId;
	}

	public void setToEmployeeId(int toEmployeeId) {
		this.toEmployeeId = toEmployeeId;
	}

	public AgentSalesMessage getAgentSalesMessage() {
		return this.agentSalesMessage;
	}

	public void setAgentSalesMessage(AgentSalesMessage agentSalesMessage) {
		this.agentSalesMessage = agentSalesMessage;
	}

}
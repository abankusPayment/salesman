package com.protecksoftware.salesman.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the agent_prospect database table.
 * 
 */
public class AgentProspectiveInsured implements Serializable {
	private static final long serialVersionUID = 1L;

	private InsuranceAgent agentId;

	private Timestamp dateCreated;

	private int agentProspectId;
	
	private ProspectiveInsured prospectiveInsured;

	public AgentProspectiveInsured() {
	}

	public InsuranceAgent getInsuranceAgent() {
		return this.agentId;
	}

	public void setInsuranceAgent(InsuranceAgent agentId) {
		this.agentId = agentId;
	}

	public Timestamp getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getAgentProspectId() {
		return this.agentProspectId;
	}

	public void setAgentProspectId(int employeeProspectId) {
		this.agentProspectId = employeeProspectId;
	}

	public ProspectiveInsured getProspectiveInsured() {
		return this.prospectiveInsured;
	}

	public void setProspectiveInsured(ProspectiveInsured prospectiveInsured) {
		this.prospectiveInsured = prospectiveInsured;
	}

}
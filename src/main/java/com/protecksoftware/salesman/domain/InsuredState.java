package com.protecksoftware.salesman.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

import com.protecksoftware.salesman.utils.PlatformUtils;

/**
 * Bean that has the status of the the Prospective Insured
 * from the time the lead was initiated to becoming a customer
 * of the Insurance Company or not.
 * 
 * @author abankwah
 *
 */
public class InsuredState implements Serializable, com.protecksoftware.salesman.application.factory.Genesis10 {
	private static final long serialVersionUID = 1L;

	private int insuredStateId;

	private String currentState;

	private String dateChanged;

	private Employee employeeId;

	@Lob
	private String message;

	private ProspectiveInsured prospectiveInsured;
	
	public InsuredState() {
	}

	public int getInsuredStateId() {
		return this.insuredStateId;
	}

	public void setInsuredStateId(int insuredStateId) {
		this.insuredStateId = insuredStateId;
	}

	public String getCurrentState() {
		return this.currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public String getDateChanged() {
		return this.dateChanged;
	}

	public void setDateChanged(String dateChanged) {
		this.dateChanged = dateChanged;
	}

	public Employee getEmployeeId() {
		return this.employeeId;
	}

	public void setEmployeeId(Employee employeeId) {
		this.employeeId = employeeId;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public ProspectiveInsured getProspectiveInsured() {
		return this.prospectiveInsured;
	}
	
	public void setProspectiveInsured(ProspectiveInsured prospectiveInsured) {
		this.prospectiveInsured = prospectiveInsured;
	}
	
	public String formatDateString(){
		if(this.dateChanged != null){
			return PlatformUtils.formatDateToString(new DateTime(Long.parseLong(this.dateChanged)), "MMMM dd, YYYY HH:MM:SS").toString();
		}else{
			return null;
		}
	}

}
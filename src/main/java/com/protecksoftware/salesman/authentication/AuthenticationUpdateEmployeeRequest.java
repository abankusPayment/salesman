package com.protecksoftware.salesman.authentication;

import com.protecksoftware.salesman.domain.Employee;

public final class AuthenticationUpdateEmployeeRequest {

	private  String firstname;
	
	private String middlename;
	
	private  String lastname;
	
	private  String region;
	
	private String city;
	
	private  String zip;
	
	private  String phoneNumber;
	
	private  String emailAddress;
	
	private  String address1;
	
	private  String address2;

	private  int companyId;
	
	private  long employeeId;
	
	private  String gender;
	
	public AuthenticationUpdateEmployeeRequest(Employee employee) {
		/**
		this.companyId = employee.getCompanyId();
		this.firstname = employee.getFirstname();
		this.lastname = employee.getLastname();
		this.middlename = employee.getMiddlename();
		this.address1 = employee.getAddress1();
		this.address2 = employee.getAddress2();
		this.region = employee.getState();
		this.city = employee.getCity();
		this.phoneNumber = employee.getPhoneNumber();
		this.emailAddress = employee.getEmailAddress();
		this.employeeId = employee.getEmployeeId();
		this.gender = employee.getGender();
		***/
	}

	public AuthenticationUpdateEmployeeRequest(){
		
	}
	public String getFirstname() {
		return firstname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public String getLastname() {
		return lastname;
	}

	public String getRegion() {
		return region;
	}

	public String getCity() {
		return city;
	}

	public String getZip() {
		return zip;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}

	public int getCompanyId() {
		return companyId;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public String getGender() {
		return gender;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	
}

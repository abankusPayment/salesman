package com.protecksoftware.salesman.authentication;

import java.io.Serializable;

/**
 * Use this class for Forgot password or Username
 * 
 * @author abankwah
 *
 */
public class AuthenticationUsernamePasswordRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String username;
	
	private int companyId;
	
	private long employeeId;
	
	private String emailAddress;
	
	private String lastname;
	
	private String requestType;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public AuthenticationUsernamePasswordRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	
	
}

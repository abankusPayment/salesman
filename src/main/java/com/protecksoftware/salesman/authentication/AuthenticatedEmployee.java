package com.protecksoftware.salesman.authentication;

import java.io.Serializable;
import java.security.Principal;

import com.protecksoftware.salesman.domain.Employee;


/**
 * Is the object that stores information about the Employee that is logged in.
 * 
 * This class is Thread-safe
 * @author abankwah
 *
 */
public class AuthenticatedEmployee implements Principal,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1069546427988585078L;

	private final String username;
	
	private final Employee employee;
	
	private final String permission;
	
	private int numberOfAttemps;

	public AuthenticatedEmployee(String username, Employee employee,String permission) {
		super();
		this.username = username;
		this.employee = employee;
		this.permission = permission;
	}
	
	public AuthenticatedEmployee(AuthenticationResponse response, String username){
		this.username = username;
		this.employee = response.getEmployee();		
		this.permission = response.getPermissions()[0];
	}

	public String getUsername() {
		return username;
	}

	public Employee getEmployee() {
		return employee;
	}

	public String getPermission() {
		return permission;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return getUsername();
	}

	public int getNumberOfAttemps() {
		return numberOfAttemps;
	}

	public void setNumberOfAttemps(int numberOfAttemps) {
		this.numberOfAttemps = numberOfAttemps;
	}
	
	
}

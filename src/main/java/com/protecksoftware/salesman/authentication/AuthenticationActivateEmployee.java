package com.protecksoftware.salesman.authentication;

public class AuthenticationActivateEmployee {

	private String username;
	
	private String tempPassword;
	
	private String hashPassword;
	
	private boolean accountLocked;
	
	private boolean accountEnbaled;
	
	private boolean accountExpired;
	
	private String requestType;

	private String token;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public String getHashPassword() {
		return hashPassword;
	}

	public void setHashPassword(String hashPassword) {
		this.hashPassword = hashPassword;
	}

	public boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public boolean isAccountEnbaled() {
		return accountEnbaled;
	}

	public void setAccountEnbaled(boolean accountEnbaled) {
		this.accountEnbaled = accountEnbaled;
	}

	public boolean isAccountExpired() {
		return accountExpired;
	}

	public void setAccountExpired(boolean accountExpired) {
		this.accountExpired = accountExpired;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
	
}

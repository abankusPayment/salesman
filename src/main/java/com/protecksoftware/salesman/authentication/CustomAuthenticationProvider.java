package com.protecksoftware.salesman.authentication;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.protecksoftware.salesman.domain.Permission;
import com.protecksoftware.salesman.exception.CustomAuthenticationException;
import com.protecksoftware.salesman.utils.PlatformUtils;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	
	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class.getName());
	
	private String authenticateUser = "/authentication/authentication/authenticateUser";
	
	private String getUserPermissions = "/authentication/authentication/getUserPermissions";

	private String authentication_hostname = System.getenv("hostUrl");

	
	@Override
	public Authentication authenticate(Authentication authenticate) throws AuthenticationException {
		String username = authenticate.getName().trim();
		String password = authenticate.getCredentials().toString().trim();
		AuthenticationRequest request = new AuthenticationRequest();
		request.setUsername(username);
		request.setPassword(password);
		AuthenticationResponse authenticationResponse = null;
		String status = null;
		Client client = PlatformUtils.getRESTClientInstance();
		String authUrl = authentication_hostname.concat(authenticateUser);
		Response response = null;
		try {
			logger.info(authentication_hostname);
			String json = PlatformUtils.convertToJSON(request);
			logger.info(json);
			byte[] requestString = Base64.encodeBase64(json.getBytes());
					response = client.target(authUrl)
							.request(MediaType.APPLICATION_JSON)
							.post(Entity.entity(new String(requestString), MediaType.APPLICATION_JSON),Response.class);

			if(response == null || response.getStatus() != 200){
				CustomAuthenticationException ace = new CustomAuthenticationException("Authentication Web Service is currently down or offlines.");
				throw ace;
			}			
			status = response.readEntity(String.class);
			authenticationResponse = PlatformUtils.convertFromJSON( AuthenticationResponse.class,status);
			
			logger.info("Retreiving User Detail information for authentication and authorization for Username: "+authenticationResponse.getUserId());
			
			if(!authenticationResponse.isResult()){
				logger.warn("User:"+username+" authentication failed. Message = "+authenticationResponse.getMessage());
				 throw new BadCredentialsException(authenticationResponse.getMessage());
			}
			
			Collection<? extends GrantedAuthority> authorities = getAllUserPermission(authenticationResponse.getPermissions());

			if(authorities == null){
				logger.warn("User:"+username+" authentication failed. Message = User Authority/Role is null or empty.");
				 throw new BadCredentialsException("Error loading Employee information.");
			}else{
				AuthenticatedEmployee authenticatedEmployee = new AuthenticatedEmployee(authenticationResponse,username);
				
				logger.info("User "+username+" authentication is completed and valid.");
				return new UsernamePasswordAuthenticationToken(authenticatedEmployee,password,authorities);
			}	
		} catch (Exception e) {
			logger.warn(e.getMessage(),e);
			CustomAuthenticationException ace = new CustomAuthenticationException("Authentication Web Service is currently down or offline.");
			throw ace;
		}

	}


	@Override
	public boolean supports(Class<?> authentication) {
		// TODO Auto-generated method stub
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	private List<Permission> getAllUserPermission(String[] permissionList){

		List<Permission> permissions = Arrays.asList(permissionList)
						.stream().map(permission -> {
							Permission p = new Permission();
							p.setPermission("ROLE_"+permission);
							return p;
						}).collect(Collectors.toList());

		return permissions;
	}
	public List<String> getUserPermissions(int userId){
		logger.info("Retreiving user's permissions.....");
		Client client = ClientBuilder.newClient();
		String results = null;
		List<String> permissionList = null;
		Response response = client.target(authentication_hostname.concat(getUserPermissions))
								.queryParam("userId", userId)
								.request(MediaType.APPLICATION_JSON)
								.get();
		results = response.readEntity(String.class);
		if(results != null){
			try {
				permissionList = PlatformUtils.convertFromJson(new TypeReference<List<String>>() {}, results);
			} catch (JsonParseException e) {
				logger.warn(e.getMessage(), e);
			} catch (JsonMappingException e) {
				logger.warn( e.getMessage(), e);
			} catch (IOException e) {
				logger.warn(e.getMessage(), e);
			}
		}
		logger.info("Retreiving user's permission completed.....");
		return permissionList;
	}
}

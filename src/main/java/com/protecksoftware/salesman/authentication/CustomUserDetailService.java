/**
 * hkboateng
 */
package com.protecksoftware.salesman.authentication;

import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.protecksoftware.salesman.exception.CustomAuthenticationException;
import com.protecksoftware.salesman.exception.PlatformException;

/**
 * @author hkboateng
 *
 */

@Component
public class CustomUserDetailService implements UserDetailsService{

	private static final Logger logger = Logger.getLogger(CustomUserDetailService.class.getName());
	
	private @Value("${auth.authenticateUser}") String authenticateUser;
	
	private @Value("${auth.getUserPermission}") String getUserPermissions;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("Authenticating username: "+username);
		return null;
	}

	public void valid(){
		
	}

	public AuthenticationResponse authenticateUserDetails( AuthenticationRequest request) {
		AuthenticationResponse authenticationResponse = null;
		String status = null;
		ObjectMapper mapper = new ObjectMapper();
		
		Client client = ClientBuilder.newClient();
		
		Response response = null;
		try {
			response = client.target(authenticateUser)		
							.request(MediaType.APPLICATION_JSON)
							.post(Entity.entity(mapper.writeValueAsString(request), MediaType.APPLICATION_JSON),Response.class);
		} catch (JsonProcessingException e) {
			PlatformException ace = new PlatformException(e);
		}
		
		if(response.getStatus() != 200){
			CustomAuthenticationException ace = new CustomAuthenticationException("Authentication Web Service is currently down or offline.");
			
			throw ace;
		}
		
		try {
			status = response.readEntity(String.class);
			authenticationResponse = mapper.readValue(status, AuthenticationResponse.class);
		} catch (Exception e) {
			PlatformException ace = new PlatformException(e);
		}
		return authenticationResponse;
	}
}

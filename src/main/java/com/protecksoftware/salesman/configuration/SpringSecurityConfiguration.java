package com.protecksoftware.salesman.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.protecksoftware.salesman.authentication.CustomAuthenticationProvider;


public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource dataSource;
	
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.jdbcAuthentication()
				.dataSource(dataSource)
				.and()
				.authenticationProvider(customAuthenticationProvider());
	}
	
	public void configure(WebSecurity web) throws Exception {
		web   
		.ignoring()
        .antMatchers("/resources/**"); 
	}
	
	protected void configure(HttpSecurity http) throws Exception {
		http
		.headers().xssProtection().and().cacheControl().and().and()
			.authenticationProvider(customAuthenticationProvider())
			.authorizeRequests()
				.antMatchers("/customer/**").fullyAuthenticated()
				.antMatchers("/employee/**").fullyAuthenticated()
				.antMatchers("/team/**").fullyAuthenticated()
				.antMatchers("/campaign/**").fullyAuthenticated()
				.and()
			
			.formLogin()
				.loginPage("/security/login") 
				.loginProcessingUrl("/security/authenticate")
				.permitAll()
			.and()
				.logout()
				.logoutUrl("/security/logout")
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID")
			.and()
				.exceptionHandling()
				.accessDeniedPage("/security/accessdenied")
			.and().csrf()
			.and().sessionManagement()
				.maximumSessions(1)
				.maxSessionsPreventsLogin(true);
	}
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public CustomAuthenticationProvider customAuthenticationProvider() {
		return new CustomAuthenticationProvider();
	}
	
	
}

/**
 * hkboateng
 */
package com.protecksoftware.salesman.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author hkboateng
 *
 */
public class PlatformException extends Exception {

	private static final Logger logger = LoggerFactory.getLogger(PlatformException.class.getName());
	
	private String message;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8408549120452171340L;

	/**
	 * 
	 */
	public PlatformException() {
		// TODO Auto-generated constructor stub
	}

	public void logger(String message, Throwable t){
		logger.warn( message, t);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public PlatformException(String message, Throwable cause,boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public PlatformException(String message, Throwable cause) {
		super(message, cause);
		this.message = message;
	}

	/**
	 * @param message
	 */
	public PlatformException(String message) {
		super(message);
		this.message = message;
	}

	/**
	 * @param cause
	 */
	public PlatformException(Throwable cause) {
		super(cause);
		logger.warn( cause.getMessage(),cause);

	}


	
}

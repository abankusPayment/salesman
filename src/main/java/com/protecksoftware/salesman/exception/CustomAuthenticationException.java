/**
 * hkboateng
 */
package com.protecksoftware.salesman.exception;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;

/**
 * @author hkboateng
 *
 */
public class CustomAuthenticationException extends AuthenticationException {

	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationException.class.getName());
	/**
	 * 
	 */
	private static final long serialVersionUID = -2529490377891872455L;

	/**
	 * @param msg
	 */
	public CustomAuthenticationException(String msg) {
		super(msg);
		logger.warn(msg);
	}

	public CustomAuthenticationException(String msg,Throwable e) {
		super(msg);
		logger.warn(msg,e);
	}
}

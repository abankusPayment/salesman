package com.protecksoftware.salesman.sales.processor;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.protecksoftware.salesman.application.response.InsuranceAgentResponse;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.InsuranceAgentDTO;
import com.protecksoftware.salesman.insurance.businessobjects.dto.LocationDTO;
import com.protecksoftware.salesman.insurance.services.InsuranceAgentService;
import com.protecksoftware.salesman.utils.PlatformUtils;
import com.protecksoftware.salesman.utils.ProcessorAbstract;

@Component
public class SalesmanProcessor  extends ProcessorAbstract{
	private final Logger logger = LoggerFactory.getLogger(SalesmanProcessor.class.getName());
	
	private @Value("${googleMapKey}") String googleGeoCodingKey;
	
	@Autowired
	private InsuranceAgentService insuranceAgentServiceImpl;
	
	public InsuranceAgentResponse findInsuranceAgentByLocation(HttpServletRequest request,String location){
		List<InsuranceAgentDTO> agentList = null;
		InsuranceAgentResponse response = null;
		logActivity(" is doing a search for Insurance Agent is "+location,request,logger);
		try {
			agentList = insuranceAgentServiceImpl.findAgentByLocation(location);
			if(agentList != null){
				response = new InsuranceAgentResponse();
				response.setAgentDTO(agentList);
				response.setResult(true);
			}else{
				response = new InsuranceAgentResponse();
				response.setResult(false);
			}
		} catch (PlatformException e) {
			logger.error(e.getMessage(),e);
		}
		return response;
	}
}

package com.protecksoftware.salesman.sales.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.protecksoftware.salesman.application.factory.GenesisFactory;
import com.protecksoftware.salesman.application.request.AdvanceSearchRequest;
import com.protecksoftware.salesman.application.request.AgentProspectiveRequest;
import com.protecksoftware.salesman.application.response.AgentProspectResponse;
import com.protecksoftware.salesman.application.response.InsuranceAgentResponse;
import com.protecksoftware.salesman.application.response.ProspectiveInsuredResponse;
import com.protecksoftware.salesman.domain.Address;
import com.protecksoftware.salesman.domain.AgentProspectiveInsured;
import com.protecksoftware.salesman.domain.Email;
import com.protecksoftware.salesman.domain.Employee;
import com.protecksoftware.salesman.domain.InsuredState;
import com.protecksoftware.salesman.domain.Phone;
import com.protecksoftware.salesman.domain.ProspectiveInsured;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.ProspectiveInsuredDTO;
import com.protecksoftware.salesman.insurance.services.InsuranceAgentService;
import com.protecksoftware.salesman.insurance.services.ProspectiveInsuranceSvc;
import com.protecksoftware.salesman.insurance.services.SalesmanResponse;
import com.protecksoftware.salesman.utils.PlatformUtils;
import com.protecksoftware.salesman.utils.ProcessorAbstract;
import com.protecksoftware.salesman.utils.SalesmanUtils;
import com.protecksoftware.salesman.utils.ValidationUtils;

public class SalesProcessor extends ProcessorAbstract{
	private final Logger logger = LoggerFactory.getLogger(SalesProcessor.class.getName());
	
	@Autowired
	private GenesisFactory genesisfactory;
	
	@Autowired
	private ProspectiveInsuranceSvc prospectiveInsuredSvcImpl;

	@Autowired
	private InsuranceAgentService insuranceAgentServiceImpl;
	
	@Autowired
	private SalesmanUtils salesmanUtils;
	
	public SalesmanResponse process(String action, HttpServletRequest request){
		SalesmanResponse response = null;
		switch(action){
			case "continue": 
				response = processNewSalesProspect(request);
				break;
			case "close" :
				response = processNewSalesProspect(request);
				break;				
			case "saveLead" :
				response = processNewSalesProspect(request);
				break;
			case "getAll" :
				response = processNewSalesProspect(request);
				break;	
			case "assignAgent" :
				response = assignAgent(request);
				break;
			case "getEmployeeSales":
				response = getEmployeeSales(request);
				break;
			default:
				break;
		}
		return response;
	}

	private ProspectiveInsuredResponse getEmployeeSales(HttpServletRequest request) {
		ProspectiveInsuredResponse response = null;
		String employee = ValidationUtils.encode(request.getParameter("employeeId"));
		String currentState = ValidationUtils.encode(request.getParameter("currentState"));
		logger.info("Looking up Direct Sales with status Active for Employee Id: "+employee+" with current status: "+currentState);
		Integer employeeId = null;
		if(ValidationUtils.isNumeric(employee)){
			employeeId = Integer.parseInt(employee);
		}else{
			logger.warn("Employee Id: "+employee+" is not in number format.");
			employeeId = 0;
		}
			
		try{
			if(!ValidationUtils.isNullOrBlank(currentState)){
				response = prospectiveInsuredSvcImpl.getProspectiveInsuredByEmployeeId(employeeId,currentState);
			}else{			
				response = prospectiveInsuredSvcImpl.getProspectiveInsuredByEmployeeId(employeeId);
			}
		}catch(PlatformException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			logger.info("Employee Id: "+employee+" has completed retriving Direct Sales customers");
		}
		
		return response;
	}

	/**
	 * Use this method to valid any sales request fields.
	 * 
	 * @param request
	 */
	@SuppressWarnings("rawtypes")
	public List<String> validateFields(HttpServletRequest request) {
		/*
		String firstname = request.getParameter("firstname");
		String middlename = request.getParameter("middlename");
		String lastname = request.getParameter("lastname");
		String address1 = request.getParameter("address1");
		String address2 = request.getParameter("address2");
		String city = request.getParameter("city");
		String region = request.getParameter("region");
		String phoneNumber = request.getParameter("phoneNumber");
		String emailAddress = request.getParameter("emailAddress");
		*/
		List<String> validationErrors = new ArrayList<String>();
		Map map = request.getParameterMap();
		for (Object entry : map.entrySet()) {
		    String key = entry.toString();
		    
		    if(key.equalsIgnoreCase("firstname")){
		    	String value = (String) map.get(key);
		    	boolean status = ValidationUtils.isValidStringWithNoSpaces(value);
		    	if(!status){
		    		validationErrors.add("First name cannot be null or empty.");
		    	}
		    }
		    if(key.equalsIgnoreCase("lastname")){
		    	String value = (String) map.get(key);
		    	boolean status = ValidationUtils.isValidStringWithNoSpaces(value);
		    	if(!status){
		    		validationErrors.add("Last name cannot be null or empty.");
		    	}	    	
		    }
		    if(key.equalsIgnoreCase("middlename")){
		    	String value = (String) map.get(key);
		    	boolean status = ValidationUtils.isValidStringWithNoSpaces(value);
		    	if(!status){
		    		validationErrors.add("Middle name cannot be null or empty.");
		    	}
		    }
		    if(key.equalsIgnoreCase("address2")){
		    	String value = (String) map.get(key);
		    	boolean status = ValidationUtils.isValidStringWithNoSpaces(value);
		    	if(!status){
		    		validationErrors.add("Address 1 cannot be null or empty.");
		    	}
		    }
		    if(key.equalsIgnoreCase("emailAddress")){
		    	String value = (String) map.get(key);
		    	boolean status = ValidationUtils.isEmailValid(value);
		    	if(!status){
		    		validationErrors.add("First name cannot be null or empty.");
		    	}
		    }
		    if(key.equalsIgnoreCase("phoneNumber")){
		    	String value = (String) map.get(key);
		    	boolean status = ValidationUtils.isValidStringWithNoSpaces(value);
		    	if(!status){
		    		validationErrors.add("Phone Number cannot be null or is not formatted properly");
		    	}
		    }
		    if(key.equalsIgnoreCase("city")){
		    	String value = (String) map.get(key);
		    	boolean status = ValidationUtils.isValidStringWithNoSpaces(value);
		    	if(!status){
		    		validationErrors.add("City cannot be null or empty.");
		    	}
		    }
		    if(key.equalsIgnoreCase("region")){
		    	String value = (String) map.get(key);
		    	boolean status = ValidationUtils.isValidStringWithNoSpaces(value);
		    	if(!status){
		    		validationErrors.add("Region cannot be null or empty.");
		    	}
		    }		   
		    if(key.equalsIgnoreCase("agentNumber")){
		    	String value = (String) map.get(key);
		    	boolean status = ValidationUtils.isValidStringWithNoSpaces(value);
		    	if(!status){
		    		validationErrors.add("Agent Id cannot be null or empty.");
		    	}
		    }
		    if(key.equalsIgnoreCase("prospectiveId")){
		    	String value = (String) map.get(key);
		    	boolean status = ValidationUtils.isValidStringWithNoSpaces(value);
		    	if(!status){
		    		validationErrors.add("Prospective Insured Id cannot be null or empty.");
		    	}
		    	if(!StringUtils.isNumeric(value)){
		    		validationErrors.add("Prospective Insured Id should be a Numberic");
		    	}
		    }		    
		}
		return validationErrors;

	}
	public List<ProspectiveInsuredDTO> findLead(String search) {
		
		List<ProspectiveInsuredDTO> propectiveInsuredList = null;
		propectiveInsuredList = prospectiveInsuredSvcImpl.getProspectiveInsuredByCity(search);
		int resultsSize = (propectiveInsuredList != null && !propectiveInsuredList.isEmpty() )? propectiveInsuredList.size() : 0;
		logger.info("Search for {} returned {} number of results",search,resultsSize);
		return propectiveInsuredList;
	}

	private ProspectiveInsuredResponse processNewSalesProspect(HttpServletRequest request) {
		ProspectiveInsuredResponse response = null;
		ProspectiveInsured prospect = (ProspectiveInsured) genesisfactory.createInstance("propectiveInsured", request);
		if(prospect == null){
			return response;
		}
		try {
			Employee employee = getEmployeeInSession(request);
			boolean assignAgentToCustomer = false;
			if(employee.getEmployeeType().equalsIgnoreCase("InsuranceAgent")){
				assignAgentToCustomer = true;
			}
			prospect.setEmployee(employee);
			InsuredState insuredState = setProspectiveInsuredState("initiated",employee);
			Address address = buildMailingAddress(request);

			Email emailAddress = buildEmailAddress(request);
			Phone phoneNumber = buildPhone(request);
			response = prospectiveInsuredSvcImpl.saveProspectiveCustomerDetails(prospect,insuredState,address,emailAddress,phoneNumber,assignAgentToCustomer);

		} catch (PlatformException e) {
			logger.error(e.getMessage(), e);
		}
		
		return response;
	}
	
	private Address buildMailingAddress(HttpServletRequest request){
		String address1 = request.getParameter("txtaddress1");
		String address2 = request.getParameter("txtaddress2");
		String city = request.getParameter("txtcity");
		String region = request.getParameter("region");
		String addressType = request.getParameter("addressType");

		
		Address address = new Address();
		address.setAddress1(ValidationUtils.encode(address1));
		address.setAddress2(ValidationUtils.encode(address2));
		address.setCity(ValidationUtils.encode(city));
		address.setRegion(region);
		address.setAddressType(addressType);
		address.setCountry("Ghana");
		address.setAddressType("mailing");
		return address;
	}
	
	private Phone buildPhone(HttpServletRequest request){
		String phoneType = request.getParameter("phoneType");
		String phoneNumber = request.getParameter("txtphoneNumber");
		
		Phone phone = new Phone();
		phone.setPhoneNumber(ValidationUtils.encode(phoneNumber));
		phone.setPhoneType(ValidationUtils.encode(phoneType));	
		phone.setPrimaryPhone(true);
		phone.setCountrycode("+233");
		return phone;
	}
	
	private Email buildEmailAddress(HttpServletRequest request){
		String emailAddress = request.getParameter("txtEmailAddress");
		Email email = new Email();
		email.setEmailAddress(ValidationUtils.encode(emailAddress));
		email.setEmailType("primary");
		return email;
	}
	private InsuredState setProspectiveInsuredState(String state,Employee employee){
		InsuredState insuredState = new InsuredState();
		insuredState.setCurrentState(state);
		insuredState.setEmployeeId(employee);
		insuredState.setDateChanged(String.valueOf(PlatformUtils.getUnixDateTimeNow()));
		insuredState.setMessage(state);
		return insuredState;
	}
	private AgentProspectResponse assignAgent(HttpServletRequest request){
		AgentProspectResponse response = null;
		AgentProspectiveInsured prospect= null;
		try{
			AgentProspectiveRequest agentProspectiveRequest = (AgentProspectiveRequest) genesisfactory.createInstance("assignProspect", request);
			if(agentProspectiveRequest == null){
				response = new AgentProspectResponse();
				response.setMessage("Sales Prospect could not be saved. Try again later.");
				response.setResult(false);			
			}else{
				response = prospectiveInsuredSvcImpl.assignProspectToAgent(agentProspectiveRequest);
			
			}
		}catch(PlatformException e){
			logger.error(e.getMessage(), e);
		}
		return response;
	}

	public ProspectiveInsuredResponse findProspective(String prospective, HttpServletRequest request,boolean loadInsuredState){
		logActivity(" is searching for Prospective Customer with Id: "+prospective,request,logger);
		String prospectiveId = ValidationUtils.encode(prospective);
		ProspectiveInsuredResponse response = null;
		response = prospectiveInsuredSvcImpl.getProspectiveCustomer(prospectiveId,loadInsuredState);
		if(response.isResult() && response.getProspectiveInsuredDTO() != null){
			String insuranceType = response.getProspectiveInsuredDTO().getInsuranceType();
			response.getProspectiveInsuredDTO().setInsuranceType(salesmanUtils.getInsuranceTypeFullName(insuranceType));
			logActivity(" has completed search for Prospective Customer and returned a result.",request,logger);
		}else{
			logActivity(" has completed search for Prospective Customer and didnot return any result.",request,logger);
		}
		return response;
		
	}
	public ProspectiveInsuredResponse findProspective(String prospective, HttpServletRequest request) {
		return findProspective(prospective,request,false);
	}
	
	public InsuranceAgentResponse findAgentLocation(HttpServletRequest request){
		InsuranceAgentResponse response = null;
		String employee = ValidationUtils.encode(request.getParameter("employeeId"));
		if(ValidationUtils.isValid(employee) && ValidationUtils.isNumeric(employee)){
			Integer employeeId = Integer.parseInt(employee);
			response = insuranceAgentServiceImpl.findInsuranceAgentByEmployeeId(employeeId);
		}else{
			response = new InsuranceAgentResponse();
			response.setMessage("The Parameter is invalid");
		}
		
		return response;
	}

	public ProspectiveInsuredResponse doAdvanceSearch(HttpServletRequest request) {
		ProspectiveInsuredResponse response = null;
		String firstname = ValidationUtils.encode(request.getParameter("firstname"));
		String lastname = ValidationUtils.encode(request.getParameter("lastname"));
		String middlename = ValidationUtils.encode(request.getParameter("middlename"));
		String city = ValidationUtils.encode(request.getParameter("city"));
		String region = ValidationUtils.encode(request.getParameter("region"));
		String phoneNumber = ValidationUtils.encode(request.getParameter("phoneNumber"));
		String emailAddress = ValidationUtils.encode(request.getParameter("emailAddress"));
		String businessName = ValidationUtils.encode(request.getParameter("businessName"));		
		try{
			Employee employee = getEmployeeInSession(request);
			if(employee != null){
				AdvanceSearchRequest advanceSearch = new AdvanceSearchRequest(employee,firstname,lastname,middlename,city,businessName,region,emailAddress,phoneNumber);
				response = prospectiveInsuredSvcImpl.performAdvanceSearch(advanceSearch);
			}
			if(response != null){
				if(!response.isResult()){
					response.setMessage("No Prospective Customer where found from your search.");
				}
				
			}else{
				response = new ProspectiveInsuredResponse();
				response.setResult(false);
				response.setMessage("Your search did not return any results.");
			}
			logger.info(response.getMessage());
		}catch(PlatformException e){
			logger.error(e.getMessage(), e);
		}
		return response;
	}
}

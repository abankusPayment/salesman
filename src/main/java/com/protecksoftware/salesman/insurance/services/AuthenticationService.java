package com.protecksoftware.salesman.insurance.services;

public interface AuthenticationService {

	boolean processActivateEmployeeRequest(String username, String passwd, String tempPasswd, String requestType,
			String token);

	boolean validateEmployeeToken(String token);

	boolean phoneNumberUnique(String phoneNumber);

	boolean processForgotPasswordRequest(String confirmationCode, String requestType, String newPasswd,
			String passwordToken);

	boolean validatePasswordToken(String token);

}

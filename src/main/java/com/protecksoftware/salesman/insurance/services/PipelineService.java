package com.protecksoftware.salesman.insurance.services;

import java.util.List;

import com.protecksoftware.salesman.application.response.EmployeeResponse;

/**
 * Default Service Interface for all Transactional interfaces
 * @author abankwah
 *
 */
public interface PipelineService<T,S,C> {
	C save(S s);
	
	C findBy(int Id);
	
	List<T> findAllBy(String number);
	
	List<T> getAll();
	
	List<T> search(String... str);
	C update(S s);
}

package com.protecksoftware.salesman.insurance.services;

import java.util.List;

import com.protecksoftware.salesman.domain.ProspectiveInsured;
import com.protecksoftware.salesman.insurance.businessobjects.dto.ProspectiveInsuredDTO;

public interface SalesService {
	ProspectiveInsured saveProspectiveCustomer(ProspectiveInsured prospec);
	
	ProspectiveInsured getProspectiveInsured(int prospectiveId);
	
	ProspectiveInsured getProspectiveInsured(String prospectiveUniqueId);
	
	List<ProspectiveInsuredDTO> getProspectiveInsuredByCity(String city);
}

package com.protecksoftware.salesman.insurance.services;

import javax.ws.rs.core.Response.Status;

public interface SalesmanResponse {
	public Status getResponseStatus();
	
	public int getResponseCode();
	
	public void setResponseStatus(Status responseStatus);
	
	public void setResponseCode(int responseCode);
	
	public String getMessage();
	
	public boolean isResult();
	
	void setResult(boolean result);
	
	void setMessage(String message);
}

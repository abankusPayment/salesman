package com.protecksoftware.salesman.insurance.services;

public interface MapService {
	String getGeoCoding(String location);
}

package com.protecksoftware.salesman.insurance.services.impl;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.protecksoftware.salesman.application.response.InsuranceAgentResponse;
import com.protecksoftware.salesman.domain.AgentProspectiveInsured;
import com.protecksoftware.salesman.domain.InsuranceAgent;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.InsuranceAgentDTO;
import com.protecksoftware.salesman.insurance.services.InsuranceAgentService;
import com.protecksoftware.salesman.utils.PlatformUtils;

@Transactional
public class InsuranceAgentServiceImpl implements InsuranceAgentService {
	private static final Logger logger = LoggerFactory.getLogger(InsuranceAgentServiceImpl.class.getSimpleName());
	
	private @Value("${waitTimeNumber}") String waitTimeNumber;
	
	private @Value("${findAgentByLocationUrl}") String findAgentByLocation;
	
	private @Value("${findInsuranceAgentByAgentIdUrl}") String findInsuranceAgentByAgentId;
	
	private @Value("${findInsuranceAgentByEmployeeIdUrl}") String findInsuranceAgentByEmployeeId;

	
	@Override
	public List<InsuranceAgentDTO> findAgentByLocation(String location) throws PlatformException {
		List<InsuranceAgentDTO> agentList = null;
		
		try{
			String responseString = null;
			Client client = PlatformUtils.getRESTClientInstance();
			Response response = client.target(findAgentByLocation)
					.queryParam("location", location)
					.request(MediaType.APPLICATION_JSON)
					.get();
			if(response.getStatus() == 200){
				responseString = response.readEntity(String.class);
				agentList = PlatformUtils.convertFromJson(new TypeReference<List<InsuranceAgentDTO>> (){}, responseString);
				
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}
		return agentList;
	}
	
	/**
	 * Find an Insurance Agent by Id
	 * @param agentId
	 * @return
	 */
	@Override
	public InsuranceAgentResponse findInsuranceAgentById(int agentId){
		InsuranceAgentResponse agent = null;
		Client client = PlatformUtils.getRESTClientInstance();
		String responseString = null;
		try{
			Response response = client.target(findInsuranceAgentByAgentId)
			.queryParam("agentId", agentId)
			.request()
			.get(Response.class);
			logger.info("Search for Insurance Agent by Employee Id: "+agentId+" return Response status: "+response.getStatus());
			if(response.getStatus() == 200){
				responseString = response.readEntity(String.class);
				agent = PlatformUtils.convertFromJSON(InsuranceAgentResponse.class, responseString);
			}else{
				agent = null;
			}

		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			responseString = null;
		}
		
		return agent;
	}

	@Override
	public InsuranceAgentResponse findInsuranceAgentByEmployeeId(Integer employeeId) {

		InsuranceAgentResponse agent = null;
		String responseString = null;
		Client client = PlatformUtils.getRESTClientInstance();
		try{
			Response response = client.target(findInsuranceAgentByEmployeeId)
					.queryParam("employeeId", employeeId)
					.request()
					.get(Response.class);
			logger.info("Search for Insurance Agent by Employee Id: "+employeeId+" return Response status: "+response.getStatus());
			if(response.getStatus() == 200){
				responseString = response.readEntity(String.class);
				agent = PlatformUtils.convertFromJSON(InsuranceAgentResponse.class, responseString);
			}else{
				agent = null;
			}

		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			responseString = null;
		}
		return agent;
	}
	private @Value("${findAgentAssignedToProspectiveCustomerUrl}") String findAgentAssignedToProspectiveCustomer;

	public InsuranceAgentResponse findAgentAssignedToProspectiveCustomer(Integer prospectiveId) throws PlatformException{
		InsuranceAgentResponse agent = null;
		String responseString = null;
		Client client = PlatformUtils.getRESTClientInstance();
		try{
			Response response = client.target(findAgentAssignedToProspectiveCustomer)
								.queryParam("prospectiveId", prospectiveId)
								.request(MediaType.APPLICATION_JSON_TYPE)
								.get();
			if(response.getStatus() == 200){
				responseString = response.readEntity(String.class);
				agent = PlatformUtils.convertFromJSON(InsuranceAgentResponse.class, responseString);
			}else{
				agent = new InsuranceAgentResponse();
				agent.setResult(false);
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}	
		return agent;
	}

}

package com.protecksoftware.salesman.insurance.services;

import com.protecksoftware.salesman.application.request.EmailMessageRequest;
import com.protecksoftware.salesman.application.request.EmployeeNoteRequest;
import com.protecksoftware.salesman.application.request.EmployeeRequest;
import com.protecksoftware.salesman.application.response.EmployeeResponse;
import com.protecksoftware.salesman.application.response.ProspectiveInsuredResponse;
import com.protecksoftware.salesman.authentication.AuthenticationEmployeeRequest;
import com.protecksoftware.salesman.domain.Employee;
import com.protecksoftware.salesman.insurance.businessobjects.dto.EmployeeDTO;

public interface EmployeeService extends PipelineService<Employee,EmployeeRequest,EmployeeResponse>{
	/**
	 * Eg. Find all Insurance Agents, Direct Sales reps
	 * @param uniqueId -
	 * @param searchType - employeeId, employeeNumber, position
	 * @return EmployeeResponse
	 */
	EmployeeResponse findAllEmployeeByPosition(String searchType, String uniqueId);
	
	EmployeeDTO findByEmployeeId(int Id);

	EmployeeDTO saveEmployeeDetails(Employee employee);
	
	EmployeeDTO saveEmployeeDetails(Employee employee,String employeeType);

	EmployeeResponse findAllEmployeeByPosition(String position);

	EmployeeResponse save(AuthenticationEmployeeRequest employeeRequest);

	void sendEmailRequest(EmailMessageRequest emailRequest);

	ProspectiveInsuredResponse addNotesToProspectiveCustomer(EmployeeNoteRequest noteRequest);
}

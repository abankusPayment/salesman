package com.protecksoftware.salesman.insurance.services.impl;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.protecksoftware.salesman.application.request.AdvanceSearchRequest;
import com.protecksoftware.salesman.application.request.AgentProspectiveRequest;
import com.protecksoftware.salesman.application.request.ProspectiveInsuredRequest;
import com.protecksoftware.salesman.application.response.AgentProspectResponse;
import com.protecksoftware.salesman.application.response.ProspectiveInsuredResponse;
import com.protecksoftware.salesman.domain.Address;
import com.protecksoftware.salesman.domain.Email;
import com.protecksoftware.salesman.domain.InsuredState;
import com.protecksoftware.salesman.domain.Phone;
import com.protecksoftware.salesman.domain.ProspectiveInsured;
import com.protecksoftware.salesman.domain.SalesRep;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.ProspectiveInsuredDTO;
import com.protecksoftware.salesman.insurance.services.ProspectiveInsuranceSvc;
import com.protecksoftware.salesman.utils.PlatformUtils;
import com.protecksoftware.salesman.utils.SalesmanUtils;


public class ProspectiveInsuredSvcImpl implements ProspectiveInsuranceSvc{
	private final Logger logger = LoggerFactory.getLogger(ProspectiveInsuredSvcImpl.class.getName());
	
	private @Value("${saveProspectiveCustomerUrl}") String saveProspectiveCustomer;
	private @Value("${saveProspectiveCustomerDetailUrl}") String saveProspectiveCustomerDetail;
	private @Value("${findAgentAssignedToProspectiveCustomerUrl}") String findAgentAssignedToProspectiveCustomer;	
	private @Value("${findProspectiveInsuredByEmployeeIdUrl}") String findProspectiveInsuredByEmployeeIdUrl;
	private @Value("${findProspectiveCustomerForAgentUrl}") String findProspectiveCustomerForAgentUrl;
	private @Value("${assignProspectToAgentUrl}") String assignProspectToAgentUrl;
	private @Value("${updateProspectiveInsuredStatusUrl}") String updateProspectiveInsuredStatus;
	private @Value("${findProspectiveCustomerUrl}") String findProspectiveCustomerUrl;
	private @Value("${findProspectiveCustomerandCurrentStatusUrl}") String findProspectiveCustomerandCurrentStatus;
	private @Value("${findProspectiveInsuredByCityUrl}") String findProspectiveInsuredByCityUrl;
	private @Value("${findProspectiveInsuredBySearchUrl}") String findProspectiveInsuredBySearchUrl;
	private @Value("${findProspectiveCustomerStatusListDetailsUrl}") String findProspectiveCustomerStatusListDetails;
	
	private @Value("${performCustomerAdvancedSearchUrl}") String performCustomerAdvancedSearch;
	
	@Override
	public ProspectiveInsuredResponse saveProspectiveCustomer(ProspectiveInsured prospect) {
		ProspectiveInsuredResponse response = null;
		try{
			ProspectiveInsuredRequest request = new ProspectiveInsuredRequest(prospect);
			String requestString = PlatformUtils.convertToJSON(request);
			response = SalesmanUtils.submitPost(saveProspectiveCustomer, requestString, ProspectiveInsuredResponse.class);

		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}
		return response;
	}

	
	@Override
	public AgentProspectResponse assignProspectToAgent(AgentProspectiveRequest prospect) throws PlatformException{
		
		AgentProspectResponse response = null;
		try{
			String requestString = PlatformUtils.convertToJSON(prospect);
			response = SalesmanUtils.submitPost(assignProspectToAgentUrl, requestString, AgentProspectResponse.class);
			if(response.isResult()){
				logger.info("Prospective Insured [with Id: "+ prospect.getProspectiveId()+"] status has being change from Initiated to Assigned.");
				logger.info("Prospective Insured [with Id: "+ prospect.getProspectiveId()+"] has being assigned to agent with Id: ["+ prospect.getAgentId()+"].");
			}else{
				logger.error("Rolling back assigning Prospective Insured [with Id: "+ prospect.getProspectiveId()+"] to Insurance Agent [with Id "+ prospect.getAgentId()+"] becuase there was an error updating Prospective Insured state.");
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}
		return response;
	}


	@Override
	public ProspectiveInsuredResponse getProspectiveCustomer(int prospectiveId) {
		ProspectiveInsuredResponse prospectiveInsuredResponse = null;
		try{
			prospectiveInsuredResponse = SalesmanUtils.submitGet(findProspectiveCustomerUrl, ProspectiveInsuredResponse.class, "prospectiveId",
					String.valueOf(prospectiveId));
			if(prospectiveInsuredResponse.isResult()){
				logger.info("Search for Propective Insured with Id: "+prospectiveId+" was successful");
			}else{
				logger.info("Search for Propective Insured with Id: "+prospectiveId+" was not successful");
			}
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}

		return prospectiveInsuredResponse;
	}

	/**
	 * Load prospective Insured
	 * @param prospectiveUniqueId - uniqueId
	 * @param addInsuredStateNote - if true load prospect Notes and Insured State, else donot load 
	 * @return
	 */
	@Override
	public ProspectiveInsuredResponse getProspectiveCustomer(String prospectiveUniqueId,boolean addInsuredStateNote) {
		ProspectiveInsuredResponse prospectiveInsuredResponse = null;
		ProspectiveInsuredRequest prospectiveInsuredRequest = new ProspectiveInsuredRequest();
		try{
			prospectiveInsuredRequest.setAddInsuredState(addInsuredStateNote);
			prospectiveInsuredRequest.setProspectiveUniqueId(prospectiveUniqueId);
			Client client = PlatformUtils.getRESTClientInstance();
			Response response = client.target(findProspectiveCustomerStatusListDetails)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.post(Entity.entity(PlatformUtils.convertToJSON(prospectiveInsuredRequest),MediaType.APPLICATION_JSON_TYPE));
			if(response.getStatus() == 200){
				String result = response.readEntity(String.class);
				prospectiveInsuredResponse = PlatformUtils.convertFromJSON(ProspectiveInsuredResponse.class, result);
			}else{
				prospectiveInsuredResponse = new ProspectiveInsuredResponse();
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}finally{
			if(prospectiveInsuredResponse == null){
				prospectiveInsuredResponse = new ProspectiveInsuredResponse();
			}
			prospectiveInsuredRequest = null;
		}
		return prospectiveInsuredResponse;	
		
	}

	@Override
	public List<ProspectiveInsuredDTO> getProspectiveInsuredByCity(String search) {
		return null;
	}

	public ProspectiveInsuredResponse getProspectInsuredStatus(Integer prospectiveId){
		ProspectiveInsuredResponse prospectiveInsuredResponse = null;
		ProspectiveInsuredRequest prospectiveInsuredRequest = new ProspectiveInsuredRequest();
		try{
			
			prospectiveInsuredRequest.setProspectiveId(prospectiveId);
			Client client = PlatformUtils.getRESTClientInstance();
			Response response = client.target(findProspectiveCustomerStatusListDetails)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.post(Entity.entity(PlatformUtils.convertToJSON(prospectiveInsuredRequest),MediaType.APPLICATION_JSON_TYPE));
			if(response.getStatus() == 200){
				String result = response.readEntity(String.class);
				prospectiveInsuredResponse = PlatformUtils.convertFromJSON(ProspectiveInsuredResponse.class, result);
			}else{
				prospectiveInsuredResponse = new ProspectiveInsuredResponse();
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}finally{
			prospectiveInsuredRequest = null;
		}
		return prospectiveInsuredResponse;	
	}
	/**
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	public ProspectiveInsured saveProspectiveCustomer(ProspectiveInsured prospect) {//insert into login_log(username,status,dateTime,companyId) values (?,?,?,?) "
		try{
			Connection conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement("insert into prospectiveInsured (firstname,middlename,lastname,phoneType,phoneNumber,emailAddress,address1,address2,city,region,prospectiveUniqueId,dateEnquired,employeeId,insuranceType) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			ps.setString(1,prospect.getFirstname());
			ps.setString(2, prospect.getMiddlename());
			ps.setString(3, prospect.getLastname());
			ps.setString(4, prospect.getPhoneType());
			ps.setString(5,prospect.getPhoneNumber());
			ps.setString(6, prospect.getEmailAddress());
			ps.setString(7, prospect.getAddress1());
			ps.setString(8, prospect.getAddress2());
			ps.setString(9,prospect.getCity());
			ps.setString(10, prospect.getRegion());
			ps.setString(11, prospect.getProspectiveUniqueId());
			ps.setLong(12, prospect.getDateEnquired());			
			ps.setInt(13,prospect.getEmployeeId());
			ps.setString(14, prospect.getInsuranceType());
			ps.executeUpdate();
			prospect = getProspectiveInsured(prospect.getProspectiveUniqueId());			
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}

		return prospect;
	}

	@Transactional
	@Override
	public ProspectiveInsured getProspectiveInsured(int prospectiveId) {
		ProspectiveInsured prospective = null;
		try{
			Session session = getSessionFactory().openSession();
			prospective = session.get(ProspectiveInsured.class, prospectiveId);
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}		
		return prospective;
	}

	@Transactional
	@Override
	public ProspectiveInsured getProspectiveInsured(String prospectiveUniqueId) {
		ProspectiveInsured prospective = null;
			try{
				Session session = getSessionFactory().openSession();
				prospective = (ProspectiveInsured) session.createQuery("From ProspectiveInsured p where p.prospectiveUniqueId =:prospectiveUniqueId")
				.setParameter("prospectiveUniqueId", prospectiveUniqueId)
				.getSingleResult();
			}catch(HibernateException e){
				logger.error(e.getMessage(),e);
			}catch(Exception e){
				logger.error(e.getMessage(),e);
			}		
		return prospective;
	}

	@Transactional
	@Override
	public List<ProspectiveInsured> getProspectiveInsuredByCity(String city) {
		// TODO Auto-generated method stub
		return null;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
***/


	@Override
	public ProspectiveInsuredResponse getProspectiveInsuredByEmployeeId(Integer employeeId,String currentState) throws PlatformException {
		ProspectiveInsuredResponse prospectiveInsuredResponse = null;
		try{
			ProspectiveInsuredRequest prospectiveInsuredRequest = new ProspectiveInsuredRequest();
			prospectiveInsuredRequest.setEmployeeId(employeeId);
			prospectiveInsuredRequest.setCurrentState(currentState);
			Client client = PlatformUtils.getRESTClientInstance();
			Response response = client.target(findProspectiveCustomerandCurrentStatus)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.post(Entity.entity(PlatformUtils.convertToJSON(prospectiveInsuredRequest),MediaType.APPLICATION_JSON_TYPE));
			if(response.getStatus() == 200){
				String result = response.readEntity(String.class);
				prospectiveInsuredResponse = PlatformUtils.convertFromJSON(ProspectiveInsuredResponse.class, result);
			}else{
				prospectiveInsuredResponse = new ProspectiveInsuredResponse();
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}
		return prospectiveInsuredResponse;
	}

	/**
	 * 
	 */
	@Override
	public ProspectiveInsuredResponse saveProspectiveCustomerDetails(ProspectiveInsured prospect, InsuredState insuredState,
			Address address, Email emailAddress, Phone phoneNumber, boolean assignAgentToCustomer) throws PlatformException {
		ProspectiveInsuredResponse prospectiveInsuredResponse = null;
		try{
			ProspectiveInsuredRequest prospectiveInsuredRequest = new ProspectiveInsuredRequest(prospect,insuredState,address,emailAddress,phoneNumber);
			prospectiveInsuredRequest.setAssignCustomerToEmployee(assignAgentToCustomer);
			Client client = PlatformUtils.getRESTClientInstance();
			Response response = client.target(saveProspectiveCustomerDetail)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.post(Entity.entity(PlatformUtils.convertToJSON(prospectiveInsuredRequest),MediaType.APPLICATION_JSON_TYPE));
			if(response.getStatus() == 200){
				String result = response.readEntity(String.class);
				prospectiveInsuredResponse = PlatformUtils.convertFromJSON(ProspectiveInsuredResponse.class, result);
			}else{
				prospectiveInsuredResponse = new ProspectiveInsuredResponse();
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}
		return prospectiveInsuredResponse;
	}

	
	@Override
	public SalesRep findSalesRepByEmployeeId(Integer employeeId){
		SalesRep rep = null;
		//TODO
		return rep;
	}
	
	/**
	 * Rest API POST
	 * @param prospectiveId - Integer
	 */
	@Override
	public ProspectiveInsuredResponse findAgentAssignedToProspectiveCustomer(Integer prospectiveId) throws PlatformException{
		ProspectiveInsuredResponse prospectiveInsuredResponse = null;
		try{
			ProspectiveInsuredRequest prospectiveInsuredRequest = new ProspectiveInsuredRequest();
			prospectiveInsuredRequest.setProspectiveId(prospectiveId);
			Client client = PlatformUtils.getRESTClientInstance();
			Response response = client.target(saveProspectiveCustomerDetail)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.post(Entity.entity(PlatformUtils.convertToJSON(prospectiveInsuredRequest),MediaType.APPLICATION_JSON_TYPE));
			if(response.getStatus() == 200){
				String result = response.readEntity(String.class);
				prospectiveInsuredResponse = PlatformUtils.convertFromJSON(ProspectiveInsuredResponse.class, result);
			}else{
				prospectiveInsuredResponse = new ProspectiveInsuredResponse();
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}
		return prospectiveInsuredResponse;
	}

	@Override
	public ProspectiveInsuredResponse getProspectiveInsuredByEmployeeId(Integer employeeId) throws PlatformException {
		ProspectiveInsuredResponse prospectiveResponse = getProspectiveInsuredByEmployeeId(employeeId,null);
		return prospectiveResponse;
	}
	
	@Override
	public ProspectiveInsuredResponse findProspectiveCustomerForAgent(Integer employeeId,boolean onlyNew) {
		ProspectiveInsuredResponse prospectiveInsuredResponse = null;
		try{
			Client client = PlatformUtils.getRESTClientInstance();
			Response response = client.target(findProspectiveCustomerForAgentUrl)
					.queryParam("employeeId", employeeId)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.get();
			if(response.getStatus() == 200){
				String result = response.readEntity(String.class);
				prospectiveInsuredResponse = PlatformUtils.convertFromJSON(ProspectiveInsuredResponse.class, result);
			}else{
				prospectiveInsuredResponse = new ProspectiveInsuredResponse();
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}
		return prospectiveInsuredResponse;
	}


	@Override
	public ProspectiveInsuredResponse performAdvanceSearch(AdvanceSearchRequest advanceSearch) {
		// performCustomerAdvancedSearch
		ProspectiveInsuredResponse prospectiveInsuredResponse = null;
		try{
			Client client = PlatformUtils.getRESTClientInstance();
			Response response = client.target(performCustomerAdvancedSearch)
					.request(MediaType.APPLICATION_JSON)
					.post(Entity.entity(PlatformUtils.convertToJSON(advanceSearch), MediaType.APPLICATION_JSON_TYPE));
			if(response.getStatus() == 200){
				String result = response.readEntity(String.class);
				prospectiveInsuredResponse = PlatformUtils.convertFromJSON(ProspectiveInsuredResponse.class, result);
			}else{
				prospectiveInsuredResponse = new ProspectiveInsuredResponse();
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}
		return prospectiveInsuredResponse;
	}
}

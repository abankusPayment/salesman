package com.protecksoftware.salesman.insurance.services;

import java.util.List;

import com.protecksoftware.salesman.application.request.AdvanceSearchRequest;
import com.protecksoftware.salesman.application.response.InsuranceAgentResponse;
import com.protecksoftware.salesman.application.response.ProspectiveInsuredResponse;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.InsuranceAgentDTO;

public interface InsuranceAgentService {

	List<InsuranceAgentDTO> findAgentByLocation(String location) throws PlatformException;

	InsuranceAgentResponse findInsuranceAgentById(int agentId);

	InsuranceAgentResponse findInsuranceAgentByEmployeeId(Integer employeeId);
}

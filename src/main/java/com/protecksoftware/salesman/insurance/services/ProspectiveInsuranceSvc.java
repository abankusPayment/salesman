package com.protecksoftware.salesman.insurance.services;

import java.util.List;

import org.hibernate.Session;

import com.protecksoftware.salesman.application.request.AdvanceSearchRequest;
import com.protecksoftware.salesman.application.request.AgentProspectiveRequest;
import com.protecksoftware.salesman.application.response.AgentProspectResponse;
import com.protecksoftware.salesman.application.response.ProspectiveInsuredResponse;
import com.protecksoftware.salesman.domain.Address;
import com.protecksoftware.salesman.domain.AgentProspectiveInsured;
import com.protecksoftware.salesman.domain.Email;
import com.protecksoftware.salesman.domain.InsuranceAgent;
import com.protecksoftware.salesman.domain.InsuredState;
import com.protecksoftware.salesman.domain.Phone;
import com.protecksoftware.salesman.domain.ProspectiveInsured;
import com.protecksoftware.salesman.domain.SalesRep;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.ProspectiveInsuredDTO;

public interface ProspectiveInsuranceSvc {
	
	ProspectiveInsuredResponse saveProspectiveCustomer(ProspectiveInsured prospec);
	
	ProspectiveInsuredResponse getProspectiveCustomer(int prospectiveId);
	
	ProspectiveInsuredResponse getProspectiveCustomer(String prospectiveUniqueId,boolean addInsuredStateNote);
	
	List<ProspectiveInsuredDTO> getProspectiveInsuredByCity(String city);

	AgentProspectResponse assignProspectToAgent(AgentProspectiveRequest agentProspectiveRequest) throws PlatformException;

	ProspectiveInsuredResponse getProspectiveInsuredByEmployeeId(Integer employeeId) throws PlatformException;

	ProspectiveInsuredResponse saveProspectiveCustomerDetails(ProspectiveInsured prospect, InsuredState insuredState,
			Address address, Email emailAddress, Phone phoneNumber, boolean assignAgentToCustomer) throws PlatformException ;


	SalesRep findSalesRepByEmployeeId(Integer employeeId);

	ProspectiveInsuredResponse findAgentAssignedToProspectiveCustomer(Integer prospectiveId) throws PlatformException;

	ProspectiveInsuredResponse getProspectiveInsuredByEmployeeId(Integer employeeId, String currentState)
			throws PlatformException;

	ProspectiveInsuredResponse findProspectiveCustomerForAgent(Integer employeeId, boolean onlyNew);

	ProspectiveInsuredResponse performAdvanceSearch(AdvanceSearchRequest advanceSearch);


}

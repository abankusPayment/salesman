package com.protecksoftware.salesman.insurance.services;

import javax.servlet.http.HttpServletRequest;

import com.protecksoftware.salesman.application.response.EmployeeResponse;
import com.protecksoftware.salesman.exception.PlatformException;

public interface Processor {
	EmployeeResponse process(String action,HttpServletRequest request) throws PlatformException;
}

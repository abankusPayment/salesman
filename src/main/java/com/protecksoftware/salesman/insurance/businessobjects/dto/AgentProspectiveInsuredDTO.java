package com.protecksoftware.salesman.insurance.businessobjects.dto;

import java.time.LocalDateTime;

import com.protecksoftware.salesman.domain.AgentProspectiveInsured;


public class AgentProspectiveInsuredDTO {

	private int agentId;
	
	private int prospectiveInsuredId;
	
	private String prospectiveUniqueId;
	
	private String agentNumber;
	
	private String agencyName;
	
	private String prospectiveCustomerName;
	
	private LocalDateTime dateInquired;
	
	private String currentState;

	
	public AgentProspectiveInsuredDTO(AgentProspectiveInsured agentProspectiveInsured) {
		this.agentId = agentProspectiveInsured.getInsuranceAgent().getAgentId();
		this.currentState = agentProspectiveInsured.getProspectiveInsured().getCurrentState();
		this.prospectiveInsuredId = agentProspectiveInsured.getProspectiveInsured().getProspectiveId();
		this.agentNumber = agentProspectiveInsured.getInsuranceAgent().getAgentNumber();
		this.agencyName = agentProspectiveInsured.getInsuranceAgent().getAgentName();
		this.prospectiveUniqueId = agentProspectiveInsured.getProspectiveInsured().getProspectiveUniqueId();
		this.dateInquired = agentProspectiveInsured.getDateCreated().toLocalDateTime();
	}

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	public int getProspectiveInsuredId() {
		return prospectiveInsuredId;
	}

	public void setProspectiveInsuredId(int prospectiveInsuredId) {
		this.prospectiveInsuredId = prospectiveInsuredId;
	}

	public String getProspectiveUniqueId() {
		return prospectiveUniqueId;
	}

	public void setProspectiveUniqueId(String prospectiveUniqueId) {
		this.prospectiveUniqueId = prospectiveUniqueId;
	}

	public String getAgentNumber() {
		return agentNumber;
	}

	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getProspectiveCustomerName() {
		return prospectiveCustomerName;
	}

	public void setProspectiveCustomerName(String prospectiveCustomerName) {
		this.prospectiveCustomerName = prospectiveCustomerName;
	}

	public LocalDateTime getDateInquired() {
		return dateInquired;
	}

	public void setDateInquired(LocalDateTime dateInquired) {
		this.dateInquired = dateInquired;
	}

	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}
	
	
}

package com.protecksoftware.salesman.insurance.businessobjects.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.protecksoftware.salesman.domain.InsuranceAgent;

public class InsuranceAgentDTO {

	private int agentId;

	private String agentName;
	
	@JsonIgnore
	private String firstname;
	
	@JsonIgnore
	private String lastname;
	
	private String fullName;
	
	private String address1;
	
	private String address2;
	
	private String[] insuranceType;
	
	private String phoneNumber;
	
	private String emailAddress;
	
	private String city;
	
	private String region;
	
	private String agentNumber;

	public InsuranceAgentDTO(InsuranceAgent agent) {
		this.fullName = agent.getEmployee().toString();
		this.firstname = agent.getEmployee().getFirstname();
		this.lastname = agent.getEmployee().getLastname();
		this.emailAddress = agent.getEmployee().getEmailAddress();
		this.phoneNumber = agent.getEmployee().getPhoneNumber();
		this.address1 = agent.getEmployee().getAddress1();
		this.address2 = agent.getEmployee().getAddress2();
		this.agentNumber = agent.getAgentNumber();
		this.city = agent.getEmployee().getCity();
		this.region = agent.getEmployee().getRegion();
		this.agentName = agent.getAgentName();
	}
	
	

	public InsuranceAgentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}



	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String[] getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String[] insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getAgentNumber() {
		return agentNumber;
	}

	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}



	public String getAgencyName() {
		return agentName;
	}



	public void setAgencyName(String agencyName) {
		this.agentName = agencyName;
	}
	
	
}

package com.protecksoftware.salesman.insurance.businessobjects.dto;

import com.protecksoftware.salesman.domain.Employee;

public class EmployeeDTO {
	private int employeeId;

	private String address1;

	private String address2;

	private String city;

	private String emailAddress;

	private String employeeNumber;

	private String firstname;

	private String gender;

	private String lastname;

	private String middlename;

	private String phoneNumber;

	private String region;

	private String jobTitle;
	
	private int positionId;
	
	private String employeeType;
	
	public EmployeeDTO(Employee employee) {
		this.employeeId = employee.getEmployeeId();
		this.firstname = employee.getFirstname();
		this.middlename = employee.getMiddlename();
		this.lastname = employee.getLastname();
		this.address1 = employee.getAddress1();
		this.address2 = employee.getAddress2();
		this.emailAddress = employee.getEmailAddress();
		this.phoneNumber = employee.getPhoneNumber();
		this.city = employee.getCity();
		this.region = employee.getRegion();
		//this.jobTitle = employee.getPosition().getPositionName();
	}

	public int getEmployeeId() {
		return this.employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmployeeNumber() {
		return this.employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return this.middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getFirstname());
		if(getMiddlename() !=null){
			builder.append(" ");
			builder.append(getMiddlename());
		}
		builder.append(" ");
		builder.append(getLastname());		
		return builder.toString();
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public int getPositionId() {
		return positionId;
	}

	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}
}

package com.protecksoftware.salesman.insurance.businessobjects.dto;

public class InsuredStateDTO {
	
	private String currentState;

	private String dateChanged;
	
	private int insuredStateId;
	
	private String prospectiveUniqueId;
	
	private String employeeNumber;
	
	private String employeeName;
	
	private String message;

	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public String getDateChanged() {
		return dateChanged;
	}

	public void setDateChanged(String dateChanged) {
		this.dateChanged = dateChanged;
	}

	public int getInsuredStateId() {
		return insuredStateId;
	}

	public void setInsuredStateId(int insuredStateId) {
		this.insuredStateId = insuredStateId;
	}

	public String getProspectiveUniqueId() {
		return prospectiveUniqueId;
	}

	public void setProspectiveUniqueId(String prospectiveUniqueId) {
		this.prospectiveUniqueId = prospectiveUniqueId;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
	
}

package com.protecksoftware.salesman.utils;

public final class Region {

	private final String abbv;
	
	private final String name;

	private final String region;
	
	private String postcode;
	
	private String phoneCode;
	
	public String getAbbv() {
		return abbv;
	}


	public String getName() {
		return name;
	}

	public Region(String abbv, String name,String region) {
		super();
		this.abbv = abbv;
		this.name = name;
		this.region = region;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public String getRegion() {
		return region;
	}

	
	
}

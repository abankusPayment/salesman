package com.protecksoftware.salesman.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

import com.protecksoftware.salesman.authentication.AuthenticatedEmployee;
import com.protecksoftware.salesman.domain.Employee;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.EmployeeDTO;
import com.protecksoftware.salesman.insurance.services.EmployeeService;

@Component
public class PlatformAbstractUtils {
	public static final String EMPLOYEE_SESSION_INSTANCE = "employeeSessionInstance";
	private static final Logger logger = LoggerFactory.getLogger(PlatformAbstractUtils.class.getName());
	
	
	@Autowired
	private EmployeeService employeeServiceImpl;
	
	public EmployeeDTO getEmploymentDetail(HttpServletRequest request){
		Employee employee = null;
		EmployeeDTO emp =  null;
		try{
			employee = getEmployeeInSession(request);
			emp = new EmployeeDTO(employee);			
		}catch(PlatformException e){
			logger.error(e.getMessage(),e);
		}

		
		return emp;
	}
	public void loadEmploymentDetailsIntoSession( HttpServletRequest request) throws PlatformException{
		
	}
	public void loadEmployeeIntoSession( HttpServletRequest request) throws PlatformException{
 		HttpSession session = request.getSession(false);
		UsernamePasswordAuthenticationToken authenticated = null;
		AuthenticatedEmployee authenticatedEmployee = null;
		Employee employee = null;
		try{
			if(session.getAttribute(PlatformAbstractUtils.EMPLOYEE_SESSION_INSTANCE) == null){
			authenticated = (UsernamePasswordAuthenticationToken) request.getUserPrincipal();
				if(authenticated.isAuthenticated()){
					authenticatedEmployee = (AuthenticatedEmployee) authenticated.getPrincipal();
					employee = authenticatedEmployee.getEmployee();
					if(employee != null){
						if(session.getAttribute(PlatformAbstractUtils.EMPLOYEE_SESSION_INSTANCE) == null){
								session.setAttribute(PlatformAbstractUtils.EMPLOYEE_SESSION_INSTANCE,employee);	
								logger.info(employee.toString()+" has been loaded in Current Session. {}",employee);	
						}			
					}
				}else{
					PlatformException pe = new PlatformException("Could not authenticated using your credentials.");
					throw pe;				
				}
			}
		}catch(Exception e){
			PlatformException pe = new PlatformException(e);
			throw pe;
		}finally{
			authenticated = null;
			authenticatedEmployee = null;
			employee = null;
		}
	}
	
	public String getCurrentUser(HttpServletRequest request){
		UsernamePasswordAuthenticationToken authenticated = null;
		AuthenticatedEmployee authenticatedEmployee = null;
		String username = "";
		authenticated = (UsernamePasswordAuthenticationToken) request.getUserPrincipal();
		if(authenticated.isAuthenticated()){
			authenticatedEmployee = (AuthenticatedEmployee) authenticated.getPrincipal();
			username = authenticatedEmployee.getUsername();
		}
		return username;
	}
	
	public void logMessage(HttpServletRequest request, Logger logger, String message){
		try {
			Employee employee = getEmployeeInSession(request);
			if(employee != null){
				if(logger != null && message != null){
					logger.info(employee+message);
				}			
			}
		} catch (PlatformException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public String logActivity(String activity,HttpServletRequest request){
		Employee employee;
		try {
			employee = getEmployeeInSession(request);
			if(employee != null){
				StringBuilder sbr = new StringBuilder();
				sbr.append("Employee: ").append(employee.toString());
				sbr.append(" [with Employee Number: "+employee.getEmployeeNumber()+" and Company Id: "+employee.getCompanyId());
				sbr.append("] "+activity);

				return sbr.toString();
			}else{
				return "redirect:/platform/logout";
			}			
		} catch (PlatformException e) {
			return "redirect:/platform/logout";
		}

	}
	
	public void logActivity(String activity,HttpServletRequest request,Logger logger){
		Employee employee = null;
		try {
			employee = getEmployeeInSession(request);
			if(employee != null){
				StringBuilder sbr = new StringBuilder();
				sbr.append("Employee: ").append(employee.toString());
				sbr.append(" [with Employee Number: "+employee.getEmployeeNumber()+" and Company Id: "+employee.getCompanyId());
				sbr.append("] "+activity);
				if(logger != null && sbr != null){
					logger.info(sbr.toString());
				}
			}			
		} catch (PlatformException e) {
			logger.error(e.getMessage(), e);
		}

	}
	
	public Employee getEmployeeInSession(HttpServletRequest request)throws PlatformException{
		HttpSession session = request.getSession(false);
		Employee employee = null;
		if(session != null && (Employee) session.getAttribute(PlatformAbstractUtils.EMPLOYEE_SESSION_INSTANCE) != null){
			employee = (Employee) session.getAttribute(PlatformAbstractUtils.EMPLOYEE_SESSION_INSTANCE);
		}
		return employee;
	}
	
	public void getEmployeeInSessionData(HttpServletRequest request){
		
	}
	
	public void addObjectToSession(String sessionName,Object object,HttpServletRequest request) throws PlatformException{
		HttpSession session = request.getSession(false);
		if(session != null){
			if( object != null &&(sessionName != null && !sessionName.isEmpty())){
				if(session.getAttribute(sessionName) == null){
					session.setAttribute(sessionName, object);
				}else{
					session.removeAttribute(sessionName);
					session.setAttribute(sessionName, object);
				}
			}else{
				throw new PlatformException(" Object to be added to Session is null.");
			}
			
		}else{
			throw new PlatformException(" Session is null or is invalid.");
		}
	}
	
	public Object getObjectFromSession(String sessionName,HttpServletRequest request) throws PlatformException{
		HttpSession session = request.getSession(false);
		Object response = null;
		if(sessionName != null && !sessionName.isEmpty()){
			if(session.getAttribute(sessionName) != null){
				response = session.getAttribute(sessionName);
			}
		}else{
			throw new PlatformException(" Session is null or is invalid.");
		}
		return response;
	}
	
	public void removeObjectFromSession(String sessionName,HttpServletRequest request) throws PlatformException{
		HttpSession session = request.getSession(false);
		if(sessionName != null && sessionName.isEmpty()){
			if( session != null){
				session.removeAttribute(sessionName);
			}else{
				throw new PlatformException(" Session is null or is invalid.");
			}
		}else{
			throw new PlatformException(" Attribute name is null or empty.");
		}
	}
	
	/**
	 * This method load the companyAccount instance into session. The
	 * companyAccount Instance has a School attribute in which the
	 * School object can be obtain from.
	 * 
	 * @param request
	 * @param companyId
	 * @throws PlatformException

	public void loadCompanyIntoSession(HttpServletRequest request,int companyId) throws PlatformException{
		HttpSession session = request.getSession(false);
		CompanyAccount company = null;
		if(session != null && session.getAttribute(PlatformFields.COMPANY_SESSION) == null){
			company = companyServiceImpl.findCompanyAccountByCompanyId(companyId);
			if(company != null){
				session.setAttribute(PlatformFields.COMPANY_SESSION,company);
			}else{
				throw new PlatformException("School Information is null or cannot be retrieved.");
			}
		}
	}	 */
}

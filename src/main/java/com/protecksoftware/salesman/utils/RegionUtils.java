package com.protecksoftware.salesman.utils;

import java.util.HashMap;
import java.util.Map;

public class RegionUtils {

	private static Map<String, Region> regionList = new HashMap<String, Region>();
	

	static {
		regionList.put("AR",new Region("AR","Ashanti Region","Ashanti"));
		regionList.put("BA",new Region("BA","Brong Ahafo Region","Brong Ahafo"));
		regionList.put("ER",new Region("ER","Eastern","Eastern Region"));
		regionList.put("CR", new Region("CR","Central", "Central Region"));
		regionList.put("GAR", new Region("GAR","Greater Accra","Greater Accra Region"));
		regionList.put("NR", new Region("NR","Northern","Northern Region"));
		regionList.put("UER", new Region("UER","Upper East", "Upper East Region"));
		regionList.put("UWR",new Region("UWR","Upper West","Upper West Region"));
		regionList.put("VR", new Region("VR","Volta", "Volta Region"));
		regionList.put("WR", new Region("WR", "Western","Western Region"));
	}
	
	/**
	 * Region
	 * @param stateAbbv
	 * @return
	 */
	public static Region getState(String region){
		Region stateName = null;
		if(regionList.containsKey(region)){
			stateName = regionList.get(region);
		}
		return stateName;
	}
	
	/**
	 * Region
	 * @param stateAbbv
	 * @return
	 */
	public static String getRegionName(String region){
		Region stateName = null;
		if(regionList.containsKey(region)){
			stateName = regionList.get(region);
		}
		return stateName.getName();
	}

}

package com.protecksoftware.salesman.utils;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.protecksoftware.salesman.insurance.services.SalesmanResponse;


public class SalesmanUtils {
	private static final Logger logger = LoggerFactory.getLogger(SalesmanUtils.class.getName());

	private @Value("${insuranceTypeList}") String insuranceTypeList;
	
	private static  Map<String,String> insuranceTypeMap = new HashMap<String, String>();
	
	private static Map<String,String> insuranceStatusMap = new HashMap<String, String>();
	
	@Autowired
	private PlatformUtils platformUtils;
	
	static {
		insuranceTypeMap.putIfAbsent("life","Life Insurance");
		insuranceTypeMap.putIfAbsent("auto","Auto Insurance");
		insuranceTypeMap.putIfAbsent("home","Life Insurance");
		
		insuranceStatusMap.put("initiated", "Prospective Customer Information Saved");
		insuranceStatusMap.put("assigned", "Prospective Customer has being assgined to an Insurance Agent");
	}
	public SalesmanUtils(){
	}
	
	public String getInsuranceTypeFullName(String abbeviate){
		return getInsuranceTypeMap().getOrDefault(abbeviate, null);
	}

	public static Map<String,String> getInsuranceTypeMap() {
		return insuranceTypeMap;
	}

	public static Map<String, String> getInsuranceStatusMap() {
		return insuranceStatusMap;
	}

	public static String getInsuranceStatus(String status) {
		return SalesmanUtils.getInsuranceStatusMap().get(status);
	}

	public static void setInsuranceTypeMap(Map<String,String> insuranceTypeMap) {
		logger.info("Insurance Type List has ben loaded into the application.");
		SalesmanUtils.insuranceTypeMap = insuranceTypeMap;
	}
	
	public static <T> T submitPost(String url, String entity,Class<T> cls){
		T salesmanResponse = null;
		if(cls != null && cls instanceof Object){
			try{
				Client client = PlatformUtils.getRESTClientInstance();
				Response response = client.target(url)
						.request(MediaType.APPLICATION_JSON_TYPE)
						.post(Entity.entity(entity, MediaType.APPLICATION_JSON));
				if(response.getStatus() == 200){
					String result = response.readEntity(String.class);
					salesmanResponse = PlatformUtils.convertFromJSON(cls, result);
				}else{
					salesmanResponse = cls.newInstance();;
				}
			}catch(Exception e){
				logger.error(e.getMessage(), e);
			}			
		}

		return salesmanResponse;
	}
	
	/**
	 * Method used to 
	 * @param <T>
	 * @param url - API Url
	 * @param T - SalesmanResponse instance
	 * @param entity
	 * @return
	 */
	public static <T> T submitGet(String url,Class<T> cls,String paramName, String params){
		T salesmanResponse = null;
		try{
			Client client = PlatformUtils.getRESTClientInstance();
			Response response = client.target(url)
					.queryParam(paramName, params)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.get();
			if(response.getStatus() == 200){
				String result = response.readEntity(String.class);
				salesmanResponse = PlatformUtils.convertFromJSON(cls, result);
			}else{
				salesmanResponse = cls.newInstance();
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}
		return salesmanResponse;
	}
}

package com.protecksoftware.salesman.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides Country code for every country
 * @author Kyeremateng Boateng
 *
 */
public class PhoneCode {

	private static Map<String, String> map = new HashMap<String, String>();
	
	private static final PhoneCode countryCode = new PhoneCode();
	
	private PhoneCode(){
		//Nothing here
	}
	
	public static PhoneCode getInstance(){
		return countryCode;
	}
	public  String getPhoneCode(String country){
		String countrycode = null;
		if(country == null || country.isEmpty()){
			throw new IllegalArgumentException("Country cannot be null or empty.");
		}
		countrycode = map.get(country);
		return countrycode;
				
	}
	static {
		map.put("GH", "233");	
	}
}

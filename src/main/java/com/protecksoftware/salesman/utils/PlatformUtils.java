/**
 * hkboateng
 */
package com.protecksoftware.salesman.utils;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.hibernate.search.spatial.impl.Point;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TupleType;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.protecksoftware.salesman.domain.AgentGeoLocation;
import com.protecksoftware.salesman.domain.Employee;
import com.protecksoftware.salesman.domain.Location;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.LocationDTO;

/**
 * @author hkboateng
 * 
 * Utilities that are common through out the application
 *
 */
public class PlatformUtils {

	private PlatformUtils(){}
	
	private static final String DELIMITER = ";";
	
	private static final String SUB_DELIMITER = ":";
	
	public static Location getAgentLocation(String regionStr,String agentLocation,String googleGeoCodingKey) throws PlatformException{
		Location location = new Location();
		GeoApiContext context = new GeoApiContext().setApiKey(googleGeoCodingKey);
		context.setConnectTimeout(30, TimeUnit.SECONDS);
		GeocodingResult[] results =  null;
		try {
			String locationStr = formatEmployeeLocation(regionStr,agentLocation);
			results =  GeocodingApi.geocode(context,locationStr).await();
			location.setLatitude(results[0].geometry.location.lat);
			location.setLongitude(results[0].geometry.location.lng);
			location.setGeospatial(formattedGeoSpatial(results[0].geometry.location.lng,results[0].geometry.location.lat));
		} catch (IOException e) {
			throw new PlatformException(e);
		}catch (Exception e) {
			throw new PlatformException(e);
		}
		return location;
	}
	public static LocationDTO getAgentLocation(String agentLocation,String googleGeoCodingKey) throws PlatformException{
		LocationDTO location = null;
		GeoApiContext context = new GeoApiContext().setApiKey(googleGeoCodingKey);
		context.setConnectTimeout(30, TimeUnit.SECONDS);
		GeocodingResult[] results =  null;
		try {
			results =  GeocodingApi.geocode(context,agentLocation).await();
			if(results.length > 0){
				location = new LocationDTO();
				location.setLatitude(results[0].geometry.location.lat);
				location.setLongitude(results[0].geometry.location.lng);
				location.setGeospatial(Point.fromDegrees(location.getLatitude(), location.getLongitude()));
			}

		} catch (IOException e) {
			throw new PlatformException(e);
		}catch (Exception e) {
			throw new PlatformException(e);
		}
		return location;
	}	
	/**
	 * 
	 * @param employee
	 * @param agentLocation - Location of Agent Office and not where the agent operates
	 * @return
	 */
	private static String formatEmployeeLocation(String regionStr,String agentLocation){
		StringBuilder sbr = new StringBuilder();
		if(regionStr != null){
			String region = RegionUtils.getRegionName(regionStr);
			sbr.append(agentLocation).append(", ").append(region).append(" Ghana");
		}
		return sbr.toString();
	}
	
	private static String formattedGeoSpatial(Double lng, Double lat) throws IOException{
		String longitude = Double.toString(lng);
		String latitude = Double.toString(lat);
		AgentGeoLocation agentGeoLocation  = new AgentGeoLocation(longitude,latitude);
		String geoString = null;
		try {
			geoString = PlatformUtils.convertToJSON(agentGeoLocation);

		} catch (IOException e) {
			throw e;
		}
		return geoString;
	}
	public static String getProspectiveUniqueId(){

		StringBuilder sbr = new StringBuilder();
		String numbers = RandomStringUtils.randomNumeric(8);
		sbr.append("AY")
			.append("0")
			.append(numbers);
			
		return sbr.toString();	
	}
	

	public static String generateEmployeeNumber(){
		StringBuilder sbr = new StringBuilder();
		DateTime dt = DateTime.now();
		
		String characters = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
			sbr.append("HT");
			sbr.append(dt.getHourOfDay());
			sbr.append(characters);
		return sbr.toString();
	}

	public static String generateSalesRepNumber(){
		StringBuilder sbr = new StringBuilder();
		DateTime dt = DateTime.now();
		
		String characters = RandomStringUtils.randomAlphanumeric(6).toUpperCase();
			sbr.append("AR");
			sbr.append(dt.getSecondOfMinute());
			sbr.append(characters);
		return sbr.toString();
	}
	
	public static String generateConfirmationNo(){
		StringBuilder sbr = new StringBuilder();
		DateTime dt = DateTime.now();
		String characters = RandomStringUtils.randomAlphanumeric(6).toUpperCase();
		sbr.append(characters);
		sbr.append("-").append(dt.getHourOfDay())
		   .append(dt.getMinuteOfHour())
		   .append(dt.getSecondOfDay())
		   .append(dt.getMillisOfSecond());
		return sbr.toString();
	}

	public static DateTime convertUnixTime(String date){
		return DateTime.parse(date).toDateTime();
	}

	public static String convertToJSON(Object o) throws IOException{
	ObjectMapper mapper = getObjectMapperInstance();
		String map = null;
		if(o != null && o instanceof Object){
			map = mapper.writeValueAsString(o);
		}
		return map;
	}
	
	public static <T>  T convertFromJSON(Class<T> cls, String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = getObjectMapperInstance();
		T type = null;
		if(json != null && cls instanceof Class){
			type = mapper.readValue(json,cls);
		}
		
		return type;
	}
	
	public static <T> T convertFromJson(TypeReference<T> r, String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = getObjectMapperInstance();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		T t = null;
		if(json != null){
			t = mapper.readValue(json, r);
		}
		return t;
		
	}

	public static <T> T convertFromJsonTree(TypeReference<T> r, String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = getObjectMapperInstance();
		T t = null;
		if(json != null){
			t = mapper.readValue(json, r);
		}
		return t;
		
	}
	
	public static String formatDateToString(Date date,String dateFormat){
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		String formattedDate = format.format(date);
		return formattedDate;
	}
	public static String formatDateToString(DateTime date,String dateFormat){
		DateTimeFormatter format = DateTimeFormat.forPattern(dateFormat);
		String formattedDate = format.print(date);
		return formattedDate;
	}
	
	public static DateTime convertStringToDate(String dateString,String dateFormat){

			DateTimeFormatter format = DateTimeFormat.forPattern(dateFormat);
			DateTime dateTime = format.parseDateTime(dateString);
			
			return dateTime;
	}
	public static String formatDateToString(String dateString){
		SimpleDateFormat format = new SimpleDateFormat("MMMM, dd YYYY hh:mm:ss a");
		//LocalDateTime dateTime = LocalDateTime.parse(dateString);
		DateTime date = DateTime.parse(dateString);
		String formattedDate = format.format(date.toDate());
		return formattedDate;
	}
	
	/**MMMM, dd YYYY hh:mm:ss a
	 * Get New Instance of ObjectMapper
	 * @return
	 */
	public static ObjectMapper getObjectMapperInstance() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

		return mapper;
	}
	
	public static Client getRESTClientInstance(){
		return RestClientProvider.getInstance().client();
	}

	public static HttpClient getRESTInstance(){
		return RestClientProvider.getInstance().getClient();
	}
	public static String padStringRight(String value,int numOfPad,String paddingChar){

		return StringUtils.rightPad(value, numOfPad,paddingChar);
	}
	public static String padStringLeft(String value,int numOfPad,String paddingChar){
		String amount = value.replaceAll(",", "");
		return StringUtils.leftPad(amount, numOfPad,paddingChar);
	}
	
	
	public static byte[] encryptPayment(String payment){
		byte[] encrypt = null;
	 
	    try{
	        MessageDigest crypt = MessageDigest.getInstance("SHA1");

	        crypt.update(payment.getBytes());
	        encrypt= crypt.digest();
	    }catch(NoSuchAlgorithmException e){
	        e.printStackTrace();
	    }
	    return encrypt;
	}

	
	public static String retrieveLastCardNumber(String cardNumber){
		String lastNumber = null;
		if((cardNumber != null && !cardNumber.isEmpty()) && cardNumber.length() > 4){
			lastNumber = cardNumber.substring(cardNumber.length()-4, cardNumber.length());
		}
		return lastNumber;
	}

	public static String convertCheckValue(boolean eInvoice) {
		String isChecked = "";
		if(eInvoice){
			isChecked = "checked";
		}
		return isChecked;
	}

	public static boolean convertYesNoToBoolean(String option) {
		boolean result = false;
		if(StringUtils.isNotEmpty(option)){
			result = option.equalsIgnoreCase("yes") ? true : false;
		}
		return result;
	}

	public static Client getExtClientInstance() {
		// TODO Auto-generated method stub
		return RestClientProvider.getInstance().extenernalClient();
	}

	public static DateTime parseDateTime(String datePaid, String date) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern(date);
		DateTime dateTime = fmt.parseDateTime(datePaid); 
		return dateTime;
	}
	
	public static TupleType getTupleTypeIntText(Session session){
		return TupleType.of(session.getCluster().getConfiguration().getProtocolOptions().getProtocolVersion(),session.getCluster().getConfiguration().getCodecRegistry(),DataType.cint(),DataType.text());
	}

	public static String formatDateToString(LocalDateTime date, String dateformat) {
		DateTimeFormatter format = DateTimeFormat.forPattern(dateformat);
		String formattedDate = format.print(date.toEpochSecond(ZoneOffset.UTC));
		return formattedDate;
	}
	
	public Map<String, String> getApplicationProps(String property){
		
		Map<String, String> applicationProps =null;
		if(property != null && !property.isEmpty()){
			applicationProps  = new HashMap<String, String>();
			StringTokenizer token = new StringTokenizer(property,PlatformUtils.DELIMITER);
			while(token.hasMoreTokens()){
				String[] strToken = token.nextToken().split(PlatformUtils.SUB_DELIMITER);
				applicationProps.put(strToken[0], strToken[1]);
			}
		}
		
		return applicationProps;
	}
	
	public static Long getUnixDateTimeNow(){
		return LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
	}
}

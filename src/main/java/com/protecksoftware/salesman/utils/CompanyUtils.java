/**
 * hkboateng
 */
package com.protecksoftware.salesman.utils;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * @author hkboateng
 *
 */
public class CompanyUtils {
	private static final SecureRandom random = new SecureRandom();
	/**
	 * 
	 */
	private CompanyUtils() {
	}
    
    /**
     * Company secret token
     * @return
     */
    public static String generateCompanyAccountID() {
        return new BigInteger(130, random).toString(32);
      }
	
	/**
	 * Use for password change and forgot password verification
	 * @return password verification code
	 */
    public static String generateAccountKey(){
    	int length = 32;
    	String alphabet = 
    	        new String("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]$!"); 
    	int n = alphabet.length();

    	String result = new String(); 
    	for (int i=0; i<length; i++) {
    	    result = result + alphabet.charAt(random.nextInt(n));
    	}
    	return result;
    }
    
 

	public static String generateCompanyAlias(String companyName) {
		int len = companyName.length();
		String alias = null;
		if(len > 9){
			alias = companyName.trim().substring(0, 9).replaceAll(" ", "-");
		}else{
			alias = companyName.trim().substring(0, len).replaceAll(" ", "-");
		}
		return alias;
	}
}

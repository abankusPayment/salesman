package com.protecksoftware.salesman.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TeamController {
	private static final Logger logger = LoggerFactory.getLogger(TeamController.class);
	
	@RequestMapping(value = "/team/listteams", method = RequestMethod.GET)
	public String team(HttpServletRequest request, Model model) {
		logger.info("Navigating to Index page {}",request.getRemoteAddr());
		return "Team/ListTeams";
	}
}

package com.protecksoftware.salesman.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.protecksoftware.salesman.application.processor.EmployeeProcessor;
import com.protecksoftware.salesman.application.response.EmployeeResponse;
import com.protecksoftware.salesman.application.response.ProspectiveInsuredResponse;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.EmployeeDTO;
import com.protecksoftware.salesman.utils.PlatformAbstractUtils;
import com.protecksoftware.salesman.utils.PlatformUtils;
import com.protecksoftware.salesman.utils.ValidationUtils;

@Controller
public class EmployeeController   extends PlatformAbstractUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
	
	private @Value("${employmentStatus}") String employmentStatusFile;
	
	private @Value("${permissionAuthorization}") String permissionAuthorizationFile;
	
	private @Value("${roleAuthorization}") String roleAuthorizationFile;

	
	@Autowired
	private PlatformUtils platformUtils;
	
	@Autowired
	private EmployeeProcessor employeeProcessor;
	
	@Secured({"ROLE_MANAGER","ROLE_ADMINISTRATOR"})
	@RequestMapping(value = "/employee/addEmployee", method = RequestMethod.GET)
	public String addEmployee(HttpServletRequest request,Model model) throws PlatformException {
		
		logActivity("is accessing the Add New Employee page.",request,logger);
		Map<String, String> employmentStatus = platformUtils.getApplicationProps(employmentStatusFile);
		Map<String, String> permissionAuthorization = platformUtils.getApplicationProps(permissionAuthorizationFile);
		Map<String, String> roleAuthorization = platformUtils.getApplicationProps(roleAuthorizationFile);
		
		if(permissionAuthorization != null && roleAuthorization.size() > 0){
			model.addAttribute("employmentStatus",employmentStatus);
			model.addAttribute("permissionAuthorization",permissionAuthorization);
			model.addAttribute("roleAuthorization",roleAuthorization);		
		}else{

		}
	
		return "Employee/AddNewEmployee";
	}
	
	@Secured({"ROLE_MANAGER","ROLE_ADMINISTRATOR"})
	@RequestMapping(value = "/employee/search", method = RequestMethod.GET)
	public String employeeSearch(HttpServletRequest request,Model model) throws PlatformException {
		
		logActivity("is accessing the Search Employee page.",request,logger);
		return "Employee/SearchEmployee";
	}
	
	@Secured({"ROLE_MANAGER","ROLE_ADMINISTRATOR"})
	@RequestMapping(value = "/employee/editEmployee", method = RequestMethod.GET)
	public String employeeEdit(HttpServletRequest request,Model model) throws PlatformException {
		
		logActivity("is accessing the Add New Employee page.",request,logger);
		Map<String, String> employmentStatus = platformUtils.getApplicationProps(employmentStatusFile);
		Map<String, String> permissionAuthorization = platformUtils.getApplicationProps(permissionAuthorizationFile);
		Map<String, String> roleAuthorization = platformUtils.getApplicationProps(roleAuthorizationFile);
		
		if(permissionAuthorization != null && roleAuthorization.size() > 0){
			model.addAttribute("employmentStatus",employmentStatus);
			model.addAttribute("permissionAuthorization",permissionAuthorization);
			model.addAttribute("roleAuthorization",roleAuthorization);		
		}else{

		}
		return "Employee/SearchEmployee";
	}
	
	@Secured({"ROLE_MANAGER","ROLE_ADMINISTRATOR"})
	@RequestMapping(value = "/employee/saveEmployee", method = RequestMethod.POST)
	public String saveEmployee(HttpServletRequest request,RedirectAttributes redirectAttributess) {
		try{
			EmployeeResponse response = employeeProcessor.process("save", request);
			StringBuilder sbr = new StringBuilder();
			if(response != null && response.isResult()){
				String message = "Employee: "+response.getEmployee().toString()+"has being saved successfull.";
				redirectAttributess.addFlashAttribute("success_message", message);
				sbr.append("redirect:/employee/addEmployee");
			}else{
				redirectAttributess.addFlashAttribute("error_message",response.getMessage());
				sbr.append("redirect:/employee/addEmployee");
			}
			return sbr.toString();
		}catch(PlatformException e){
			logger.error(e.getMessage(),e);
			return "";
		}
	}
	
	@Secured({"ROLE_EMPLOYEE","ROLE_MANAGER","ROLE_ADMINISTRATOR"})
	@RequestMapping(value = "/employee/updateEmployee", method = RequestMethod.POST)
	public String employeeUpdate(HttpServletRequest request,Model model) throws PlatformException {
		StringBuilder sbr = new StringBuilder();
		sbr.append("redirect:/employee/search");

		return sbr.toString();
	}
	
	@RequestMapping(value = "/employee/findAllEmployee", method = RequestMethod.GET)
	public String findAllEmployee(HttpServletRequest request,Model model){
		StringBuilder sbr = new StringBuilder();
					
		try{
			EmployeeResponse response = employeeProcessor.process("getAllEmployee", request);
			if(response.isResult()){
				model.addAttribute("employeeList", response.getEmployeeList());
				sbr.append("Employee/SearchEmployee");
			}else{
				model.addAttribute("messageInfo","No information was found");
				sbr.append("Employee/SearchEmployee");
			}
			
		}catch(PlatformException e){
			logger.error(e.getMessage(),e);
		}


		return sbr.toString();
	}
	
	@Secured({"ROLE_EMPLOYEE","ROLE_MANAGER","ROLE_ADMINISTRATOR"})
	@RequestMapping(value = "/employee/listEmployee", method = RequestMethod.GET)
	public String listEmployee(HttpServletRequest request,Model model){
		StringBuilder sbr = new StringBuilder();
		try{
			EmployeeResponse response = employeeProcessor.process("getAllEmployee", request);
			sbr.append("Employee/SearchEmployee");
			model.addAttribute("searchPageTitle", "List Employee\'s");
			if(response.isResult()){
				model.addAttribute("employeeList", response.getEmployeeList());
			}else{
				model.addAttribute("messageInfo","No information was found");
			}
		}catch(PlatformException e){
			logger.info(e.getMessage(),e);
			model.addAttribute("messageInfo","No information was found");
		}
		return sbr.toString();
	}
	
	@Secured({"ROLE_EMPLOYEE"})
	@RequestMapping(value = "/employee/searchEmployee", method = RequestMethod.GET)
	public String listEmployeeByPosition(HttpServletRequest request,Model model,RedirectAttributes redirectAttributess){
		StringBuilder sbr = new StringBuilder();
		String position = ValidationUtils.encode(request.getParameter("position"));
		sbr.append("InsuranceAgents/SearchAgent");
		if(position == null){
			model.addAttribute("messageError", "Enter a Job Title that you would want to search for...");
		}else{
			try{
				EmployeeResponse response = employeeProcessor.process("findAllEmployeeByPosition", request);
				model.addAttribute("searchPageTitle", "List Employee\'s By Position :- "+position.toUpperCase());
				logger.info("Total result returned: "+response.getEmployee());
				if(response.isResult()){
					model.addAttribute("agentList", response.getInsuranceAgent());
				}else{
					model.addAttribute("messageInfo","No information was found");
				}
			}catch(PlatformException e){
				logger.info(e.getMessage(),e);
				sbr.append("redirect:/salesman/index");
			}
		}
		return sbr.toString();
	}
	
	
	@Secured({"ROLE_EMPLOYEE"})
	@RequestMapping(value = "/employee/profile", method = RequestMethod.GET)
	public String profile(HttpServletRequest request,Model model){
		StringBuilder sbr = new StringBuilder();
		try{
			EmployeeDTO employee = getEmploymentDetail(request);
			
			
			if(employee != null){
				model.addAttribute("employeeInstance",employee);
				sbr.append("Employee/profile");
			}else{
				sbr.append("redirect:/security/login");
			}			
		}catch(Exception e){
			
		}
		return sbr.toString();
	}
	
	@RequestMapping(value = "/employee/sendMessage", method = RequestMethod.POST)
	public String sendMessage(HttpServletRequest request,Model model){
		StringBuilder sbr = new StringBuilder();
		try{
			employeeProcessor.processMessageFromEmployee(request);
		}catch(Exception e){
			
		}
		return sbr.toString();
	}

}

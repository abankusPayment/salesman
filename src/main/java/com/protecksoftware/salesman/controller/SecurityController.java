package com.protecksoftware.salesman.controller;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.protecksoftware.salesman.insurance.services.AuthenticationService;
import com.protecksoftware.salesman.utils.PlatformUtils;
import com.protecksoftware.salesman.utils.ValidationUtils;

@Controller
public class SecurityController {
	private static final Logger logger = LoggerFactory.getLogger( SecurityController.class.getName());
	
	private @Value("${dateTimeFormat}") String dateformat;
	
	private @Value("${accessdeniedMessage}") String accessdeniedMessage;
	
	@Autowired
	private AuthenticationService authenticationServiceImpl;

	@RequestMapping(value="/security/logout", method=RequestMethod.GET)
	public String logout(HttpServletRequest request,Model model){
		HttpSession session = request.getSession(false);
		String username = request.getUserPrincipal().getName();
		logger.info("User: "+username+" is logging out.");
		if(session != null){
			session.invalidate();
			session = null;
		}
		model.addAttribute("logout", "You have being logout successfully;");
		return "Security/logout";
	}
	
	@RequestMapping(value="/security/login", method=RequestMethod.GET)
	public String login(HttpServletRequest request,Model model){
		String date = PlatformUtils.formatDateToString(LocalDateTime.now(), dateformat);
		logger.info("Navigation to Login page - {}",date);
		return "Security/login";
	}

	
	@RequestMapping(value="/security/login_failure", method=RequestMethod.GET)
	public String accessdenied(HttpServletRequest request,Model model){
		try{
		model.addAttribute("accessdenied", accessdeniedMessage);
		}catch(BadCredentialsException e){
			logger.error(e.getMessage());
		}
		return "Security/login";
	}
	
	@RequestMapping(value="/security/authenticate", method=RequestMethod.POST)
	public String authenticate(HttpServletRequest request,Model model){
		logger.info("Redirecting user to Index page.");
		return "redirect:/salesman/index";
	}

	@RequestMapping(value="/security/activateAccount", method=RequestMethod.GET)
	public String activateEmployee(@RequestParam("token") String token,Model model,HttpServletRequest request){
		boolean isTokenExpired = false;
		if(!ValidationUtils.isNullOrBlank(token) && ValidationUtils.isValid(token)){
			isTokenExpired = authenticationServiceImpl.validateEmployeeToken(token);
		}
		if(isTokenExpired){
			HttpSession session = request.getSession(false);
			if(session != null){
				session = request.getSession();
			}else{
				session = request.getSession(true);
			}
			session.setAttribute("activateToken",token);
			session.setAttribute("requestType","activateEmployee");
		}else{
			model.addAttribute("errorMessage", "The time to change your change has exceed has expired or is not valid. Request to change your password again.");
		}
		return "Security/ActivateEmployee";		
	}
	
	@RequestMapping(value="/company/activateEmployee", method=RequestMethod.POST)
	public String activateEmployee(HttpServletRequest request, RedirectAttributes redirectAttributess){
		String username = request.getParameter("username");
		String passwd = request.getParameter("newpasswd");
		String tempPasswd = request.getParameter("authCode");
		StringBuilder sbr = new StringBuilder();
		boolean isSuccessful = false;
		try{
			String token = (String) request.getSession(false).getAttribute("activateToken");
			if(ValidationUtils.isValid(username) && ValidationUtils.isValid(passwd) && ValidationUtils.encode(token) != null){
				isSuccessful = authenticationServiceImpl.processActivateEmployeeRequest(username,passwd,tempPasswd,"activateEmployee",token);
				sbr.append("redirect:/salesman/index");
			}else{
				redirectAttributess.addFlashAttribute("success_message", "The information you provided is invalid or contain invalid characters.");
				sbr.append("/security/activateAccount?token=").append(token);
			}
			if(isSuccessful){
				redirectAttributess.addFlashAttribute("success_message", "You have successfully change your password. Login using your new password.");
			}else{
				redirectAttributess.addFlashAttribute("success_message", "Your account could not be activated, please try again. Else contact your administrator.");
				sbr.append("/security/activateAccount?token=").append(token);
			}
		}catch(Exception e){
			
		}finally{
			username = null;
		}

		
		return sbr.toString();
	}
	
}

package com.protecksoftware.salesman.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.protecksoftware.salesman.application.processor.EmployeeProcessor;
import com.protecksoftware.salesman.application.response.ProspectiveInsuredResponse;
import com.protecksoftware.salesman.application.response.SalesProspectResponse;
import com.protecksoftware.salesman.domain.ProspectiveInsured;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.ProspectiveInsuredDTO;
import com.protecksoftware.salesman.sales.processor.SalesProcessor;
import com.protecksoftware.salesman.utils.PlatformAbstractUtils;
import com.protecksoftware.salesman.utils.ValidationUtils;

@Controller
public class CustomerController  extends PlatformAbstractUtils {

	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);
	
	private @Value("${prospectiveNotFound}") String prospectiveNotFound;
	
	@Autowired
	private SalesProcessor salesProcessor;
	
	@Autowired
	private EmployeeProcessor employeeProcessor;
	
	@RequestMapping(value="/customer/createCustomer", method=RequestMethod.GET)
	public String createCustomer(HttpServletRequest request, Model model, RedirectAttributes redirectAttributess){
		ProspectiveInsured prospective = (ProspectiveInsured) request.getSession(false).getAttribute("prospect");
		model.addAttribute("prospect", prospective);
		logActivity("is navigating to Create New Insurance Proposal page.",request,logger);
		return "Customer/registerCustomer";
	}

	@RequestMapping(value="/insurance/insurancePolicy", method=RequestMethod.GET)
	public String lifeProposal(HttpServletRequest request, Model model){
		ProspectiveInsured prospective = (ProspectiveInsured) request.getSession(false).getAttribute("prospect");
		model.addAttribute("pageTitle", "New Insurance");
		model.addAttribute("prospect", prospective);
		logActivity("is navigating to Create New Insurance Proposal page.",request,logger);
		return "Customer/registerCustomer";
	}
	
	@RequestMapping(value="/insurance/proposal", method=RequestMethod.GET)
	public String proposal(HttpServletRequest request, Model model){
		logActivity("is navigating to New Insurance Proposal page.",request,logger);
		return "Insurance/NewProposal";
	}
	
	@RequestMapping(value="/customer/searchCustomer", method=RequestMethod.GET)
	public String searchCustomer(HttpServletRequest request, Model model){
		model.addAttribute("pageTitle", "Customer Search");
		logActivity("is navigating to Search for Insured information page.",request,logger);
		return "Customer/Search";
	}
	
	@RequestMapping(value="/proposal/submitProposal", method=RequestMethod.POST)
	public String submitProposal(HttpServletRequest request, Model model){
		//Redirect to a confirmation page
		return "Customer/findCustomer";
	} 
	@RequestMapping(value="/proposal/startProposal", method=RequestMethod.POST)
	public String startProposal(HttpServletRequest request, RedirectAttributes redirectAttributess){
		
		String navigator = ValidationUtils.encode(request.getParameter("navigator"));
		logActivity("is saving Prospective Customer:- Navigation -"+navigator,request,logger);
		
		SalesProspectResponse prospective = (SalesProspectResponse) salesProcessor.process(navigator, request);
		StringBuilder sbr = new StringBuilder();
		
		try{
			if((navigator != null && !navigator.isEmpty()) && prospective.isResult()){
				if(navigator.equalsIgnoreCase("continue")){
					addObjectToSession("prospect", prospective.getProspectiveInsured(),request);
					sbr.append("redirect:/customer/assignAgent");
				}else{
					redirectAttributess.addFlashAttribute("infoMessage", "The Prospective Customer has being saved successfully");
					sbr.append("redirect:/customer/assignAgent");
				}
			}else{
				if(navigator == null){
					logActivity("Navigator parameter cannot be null.",request,logger);
				}else{
					throw new PlatformException("An internal error occured. Navigator attribute is null or Prospective instance returned  Null.");
				}
			}
		}catch(PlatformException e){
			logger.error(e.getMessage(),e);
			redirectAttributess.addFlashAttribute("errorMessage", "Your request could not be process...Try again or Contact your Administrator.");
			sbr.append("redirect:/salesman/index");
		}
		return sbr.toString();
	}

	@RequestMapping(value="/proposal/createNewProposal", method=RequestMethod.GET)
	public String createNewProposal(HttpServletRequest request, Model model){
		logActivity("is navigating to Create New Insurance Proposal page.",request,logger);
		return "Customer/CreateNewProposal";
	}
	
	@RequestMapping(value="/customer/submitInsuranceLead", method=RequestMethod.POST)
	public String submitLead(HttpServletRequest request, Model model){
		return "Customer/findCustomer";
	}
	
	@RequestMapping(value="/insurance/leadConfirmation", method=RequestMethod.GET)
	public String leadConfirmation(HttpServletRequest request, Model model){
		return "Customer/LeadConfirmation";
	}
	
	@RequestMapping(value="/insurance/proposalConfirmation", method=RequestMethod.GET)
	public String proposalConfirmation(HttpServletRequest request, Model model){
		return "Customer/ProposalConfirmation";
	}

	
	@RequestMapping(value="/customer/search", method=RequestMethod.POST)
	public String search(HttpServletRequest request, RedirectAttributes redirectAttributessl){
		String search = ValidationUtils.encode(request.getParameter("search"));
		logActivity("is searching for - "+search,request,logger);
		if(search == null){
			redirectAttributessl.addFlashAttribute("message", "No Prospective Customer was found.");
		}else{
			List<ProspectiveInsuredDTO> prospectList = salesProcessor.findLead(search);
			if(prospectList != null){
				redirectAttributessl.addFlashAttribute("prospectList", prospectList);
			}else{
				redirectAttributessl.addFlashAttribute("message", "No Prospective Customer was found.");
				redirectAttributessl.addFlashAttribute("prospectList", null);
			}			
		}
		return "redirect:/customer/search";
	}
	
	@RequestMapping(value="/customer/advanceSearch", method=RequestMethod.POST)
	public String advanceSearch(HttpServletRequest request, RedirectAttributes redirectAttributessl){
		logActivity(" is performing Advance Prospective Customer Search ",request,logger);
		ProspectiveInsuredResponse prospectiveResponse = salesProcessor.doAdvanceSearch(request);
		if(prospectiveResponse != null){
			redirectAttributessl.addFlashAttribute("prospectList", prospectiveResponse.getProspectiveInsuredList());
		}else{
			redirectAttributessl.addFlashAttribute("message", "No Prospective Customer was found.");
			redirectAttributessl.addFlashAttribute("prospectList", null);
		}	
		return "redirect:/customer/search";
	}
	
	@RequestMapping(value="/customer/search", method=RequestMethod.GET)
	public String searchPage(HttpServletRequest request, Model model){
		logActivity("is viewing the Customer Search Page",request,logger);
		return "Customer/Search";
	}
	
	@RequestMapping(value="/customer/assignAgent", method=RequestMethod.GET)
	public String assignAgent(HttpServletRequest request, Model model){
		logActivity("is viewing the Assign Insurance Agent to Prospective Customer Page",request,logger);
		return "Customer/AssignProspect";
	}
	
	
	@RequestMapping(value="/customer/viewProspectiveProfile/{prospectiveId}", method=RequestMethod.GET)
	public String prosptectiveInsuredprofile(@PathVariable("prospectiveId") String prospectiveId,HttpServletRequest request,Model model){
		logActivity("is viewing Profile of Prospective Customer with Id: "+prospectiveId,request,logger);
		String path = "Customer/ProspectiveProfile";
		ProspectiveInsuredResponse prospective = null;
		if(prospectiveId == null){
			model.addAttribute("errorContainer", "Your request cannot be completed");
		}else{
			prospective = salesProcessor.findProspective(prospectiveId,request,true);
			if(prospective.isResult()){
				model.addAttribute("prospective", prospective.getProspectiveInsuredDTO());
			}else{
				model.addAttribute("error_message", prospectiveNotFound);
			}
		}
		return path;
	}
	
	private @Value("${employeeNoteToProspectiveCustomer}") String employeeNoteToProspectiveCustomer;
	
	@RequestMapping(value = "/customer/saveEmployeeNoteToCustomer", method = RequestMethod.POST)
	public @ResponseBody ProspectiveInsuredResponse addEmployeeNoteToCustomerMessage(HttpServletRequest request,Model model){

		ProspectiveInsuredResponse response =  null;
		try{
			response = employeeProcessor.addNotesToProspectiveCustomer(request);
			if(response != null && response.isResult()){
				logger.info("");
				model.addAttribute("prospective", response.getProspectiveInsuredDTO());
			}else{
				if(response == null){
					logger.warn("Prospective Insured Response returned null...");
					response = new ProspectiveInsuredResponse();
				}
				response.setMessage(employeeNoteToProspectiveCustomer);
				model.addAttribute("prospective", response);
			}
		}catch(Exception e){
			
		}
		return response;
	}
}

package com.protecksoftware.salesman.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/appointment")
public class AppoinmentController {
	private static final Logger logger = LoggerFactory.getLogger( AppoinmentController.class.getName());
	
	@PostMapping(value="/setAppointment")
	public String setAppointment(HttpServletRequest request, Model model){
		String path = "";
		return path;
	}
	
	@GetMapping(value="/setAppointment")
	public String createAppointment(HttpServletRequest request, Model model){
		String path = "/Appointment/Create";
		return path;
	}
	
	@GetMapping(value="/viewAppointments")
	public String viewCalendar(HttpServletRequest request, Model model){
		String path = "/Appointment/ViewAppointment";
		return path;
	}
}

package com.protecksoftware.salesman.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.protecksoftware.salesman.application.response.AgentProspectResponse;
import com.protecksoftware.salesman.application.response.InsuranceAgentResponse;
import com.protecksoftware.salesman.application.response.ProspectiveInsuredResponse;
import com.protecksoftware.salesman.application.response.SalesProspectResponse;
import com.protecksoftware.salesman.domain.Employee;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.businessobjects.dto.ProspectiveInsuredDTO;
import com.protecksoftware.salesman.sales.processor.SalesProcessor;
import com.protecksoftware.salesman.sales.processor.SalesmanProcessor;
import com.protecksoftware.salesman.utils.PlatformAbstractUtils;
import com.protecksoftware.salesman.utils.PlatformUtils;
import com.protecksoftware.salesman.utils.ValidationUtils;

@Controller
public class SalesController  extends PlatformAbstractUtils {
	private static final Logger logger = LoggerFactory.getLogger(SalesController.class.getSimpleName());
	
	@Autowired
	private SalesProcessor salesProcessor;
	
	@Autowired
	private SalesmanProcessor salesmanProcessor;
	
	private @Value("${confirmationMessage}") String confirmationMessage;
	
	private @Value("${prospectiveNotFound}") String prospectiveNotFound;
	/**
	 * Used Direct Sales Rep to view the status of a Sales Prospect:
	 * whether the agent has contact the prospect or if prospect has being
	 * converted to a Customer.
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/sales/viewSales", method=RequestMethod.GET)
	public String lifeProposal(HttpServletRequest request, Model model){
		logActivity("is navigating to CSales Lookup page.",request,logger);
		Employee employee = null;
		try{
			employee = getEmployeeInSession(request);
			if(employee != null){
				model.addAttribute("employeeId", employee.getEmployeeId());
			}
		}catch(Exception e){
			logger.error(e.getMessage(), e);
		}
		return "Sales/SalesLookup";
	}
	
	/**
	 * Use this request method to add new prospect info by a Direct Sales Rep
	 * and later assign the new sales prospect to an Insurance Agent
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/sales/AddNewProspect", method=RequestMethod.GET)
	public String salesProspect(HttpServletRequest request, Model model){
		logActivity("is navigating to Add New Prospective Sales page.",request,logger);

		return "Sales/AddNewProspect";
	}
	
	@RequestMapping(value="/sales/saveNewProspect", method=RequestMethod.POST)
	public String saveProspect(HttpServletRequest request, RedirectAttributes redirectAttributess){
		logger.info("Processing new sales prospect.");
		List<String> validationErrors = salesProcessor.validateFields(request);
		if(validationErrors != null && !validationErrors.isEmpty()){
			redirectAttributess.addFlashAttribute("error", validationErrors);
			return "redirect:/sales/AddNewProspect";
		}
		
		String navigator = ValidationUtils.encode(request.getParameter("navigator"));
		logActivity("is saving Prospective Customer:- Navigation -"+navigator,request,logger);
		
		ProspectiveInsuredResponse prospective = (ProspectiveInsuredResponse) salesProcessor.process(navigator, request);
		StringBuilder sbr = new StringBuilder();
		try{
			if( prospective.isResult()){
				if(navigator.equalsIgnoreCase("continue")){
					
					//checking if the employee is an Agent or Sales Rep
					Employee employee = getEmployeeInSession(request);
					if(employee.getEmployeeType().equalsIgnoreCase("InsuranceAgent")){
						sbr.append("redirect:/sales/confirmation");
					}else{
						addObjectToSession("prospect", prospective.getProspectiveInsuredDTO(),request);
						sbr.append("redirect:/sales/assignProspect");
					}
				}else{
					redirectAttributess.addFlashAttribute("infoMessage", "The Prospective Customer has being saved successfully");
					sbr.append("redirect:/sales/assignProspect");
				}
				

			}else{
				if(navigator == null){
					logActivity("Navigator parameter cannot be null.",request,logger);
				}else{
					logger.info(prospective.getMessage());
					throw new PlatformException("An internal error occured. Navigator attribute is null or Prospective instance returned  Null.");
				}
			}
		}catch(PlatformException e){
			logger.error(e.getMessage(),e);
			redirectAttributess.addFlashAttribute("errorMessage", "Your request could not be process...Try again or Contact your Administrator.");
			sbr.append("redirect:/salesman/index");
		}
		logActivity("is being redirected to Add New Prospective Sales page.",request,logger);
		return sbr.toString();	
	}
	
	@RequestMapping(value="/sales/assignProspect", method=RequestMethod.GET)
	public String AssignProspect(HttpServletRequest request, Model model){
		logActivity("is navigating to Assign Prospect to Insurance Agent page.",request,logger);
		ProspectiveInsuredDTO prospective = null;
		try {
			prospective = (ProspectiveInsuredDTO) getObjectFromSession("prospect",request);
			if(prospective != null){
				model.addAttribute("prospective", prospective);
			}
		} catch (PlatformException e) {
			logger.error(e.getMessage(),e);
		}		
		return "Sales/AssignProspect";
	}

	@RequestMapping(value="/sales/assignProspect/{prospectiveId}", method=RequestMethod.GET)
	public String assignSavedProspect(@PathVariable("prospectiveId") String prospectiveId,HttpServletRequest request, Model model){
		logActivity("is navigating to Assign Prospect to Insurance Agent page.",request,logger);
		ProspectiveInsuredDTO prospective = null;
		ProspectiveInsuredResponse prospectiveInsuredResponse = null;
		String path = "Sales/AssignProspect";
		try {
			if(prospectiveId == null){
				model.addAttribute("errorContainer", "Your request cannot be completed");
			}else{
				prospectiveInsuredResponse = salesProcessor.findProspective(prospectiveId,request,false);
				if(prospectiveInsuredResponse.isResult() && prospectiveInsuredResponse.getProspectiveInsuredDTO() != null){
					prospective = prospectiveInsuredResponse.getProspectiveInsuredDTO();
					if(prospective.isHasAssignedAgent()){
						path = "/customer/viewProspectiveProfile/"+prospective.getProspectiveUniqueId();
					}else{
						model.addAttribute("prospect", prospective);
					}
				}else{
					model.addAttribute("errorMessage", prospectiveNotFound);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}		
		return path;
	}
	
	@RequestMapping(value="/sales/saveAssignProspect", method=RequestMethod.POST)
	public String saveAssignedAgent(HttpServletRequest request, Model model, RedirectAttributes redirectAttributes){
		logActivity("is assigning an Insurance Agent to a Prospective Customer.",request,logger);
		List<String> validationErrors = salesProcessor.validateFields(request);
		String redirect = null;
		if(!validationErrors.isEmpty()){
			if(validationErrors != null && !validationErrors.isEmpty()){
				for(String str: validationErrors){
					logger.info(str);
				}
				redirectAttributes.addFlashAttribute("error", validationErrors);
				redirect = "redirect:/sales/assignProspect";
				return redirect;
			}			
		}
		ProspectiveInsuredDTO prospective = null;
		AgentProspectResponse response = null;
		try{
			response = (AgentProspectResponse) salesProcessor.process("assignAgent", request);
			if(response != null && response.isResult()){
				logActivity("has completed asigning Insurance Agent: "+response.getAgentProspect().getInsuranceAgent().toString()+" to Prospective Customer: "+response.getAgentProspect().getProspectiveInsured().toString(),request,logger);
				redirectAttributes.addFlashAttribute("success_message", confirmationMessage);
				removeObjectFromSession("prospect",request);
				addObjectToSession("prospectiveId",response.getAgentProspect().getProspectiveInsured().getProspectiveUniqueId(),request);
				redirect = "redirect:/sales/confirmation";
			}else{
				prospective = (ProspectiveInsuredDTO) getObjectFromSession("prospect",request);
				redirectAttributes.addFlashAttribute("success_message", confirmationMessage);
				redirect = "redirect:/sales/assignProspect/"+prospective.getProspectiveUniqueId();
			}
		}catch(PlatformException e){
			logger.error(e.getMessage(),e);
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}
		return redirect;
	}
	
	@RequestMapping(value="/sales/confirmation", method=RequestMethod.GET)
	public String salesConfirmation(HttpServletRequest request, Model model){
		model.addAttribute("success_message", confirmationMessage);
		return "Sales/SalesConfirmation";
	}

	
	@RequestMapping(value="/sales/getSalesList", method=RequestMethod.GET)
	public @ResponseBody String getSalesList(HttpServletRequest request){
		ProspectiveInsuredResponse response =null;
		response = (ProspectiveInsuredResponse) salesProcessor.process("getEmployeeSales", request);
		String str = null;
		try {
			str = PlatformUtils.convertToJSON(response);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}
		return str;
	}
	
	@RequestMapping(value="/sales/findAgentByLocation", method=RequestMethod.GET)
	public @ResponseBody String findAgentByLocation(HttpServletRequest request){
		InsuranceAgentResponse response =null;
		String location = request.getParameter("customerLocation");
		response = (InsuranceAgentResponse)salesmanProcessor.findInsuranceAgentByLocation(request, location);
		String str = null;
		try {
			str = PlatformUtils.convertToJSON(response);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}
		return str;
	}
}

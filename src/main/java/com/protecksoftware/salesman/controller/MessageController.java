package com.protecksoftware.salesman.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.protecksoftware.salesman.utils.PlatformAbstractUtils;

/**
 * Controller for all messages, alerts and info
 * @author abankwah
 *
 */

@Controller
public class MessageController extends PlatformAbstractUtils{

	private static final Logger logger = LoggerFactory.getLogger(MessageController.class.getName());
	
	@RequestMapping(value="/messages/mailInbox", method=RequestMethod.GET)
	public String mailInbox(HttpServletRequest request,Model model){
		logActivity("is navigating to Mail Box page",request,logger);
		return "Message/mailBox";
	}
	
	@RequestMapping(value="/messages/viewMessage", method=RequestMethod.GET)
	public String viewMessage(HttpServletRequest request,Model model){
		return "Message/viewMessage";
	}
	
	@RequestMapping(value="/messages/createMessage", method=RequestMethod.GET)
	public String createMessage(HttpServletRequest request,Model model){
		logActivity("is navigating to Create Message page",request,logger);
		return "Message/createMessage";
	}
}

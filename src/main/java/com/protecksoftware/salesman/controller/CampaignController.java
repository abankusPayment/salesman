package com.protecksoftware.salesman.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.protecksoftware.salesman.utils.PlatformAbstractUtils;

@Controller
public class CampaignController  extends PlatformAbstractUtils {
	private static final Logger logger = LoggerFactory.getLogger(CampaignController.class);
	
	@RequestMapping(value = "/campaign/createcampaign", method = RequestMethod.GET)
	public String createcampaign(HttpServletRequest request, Model model) {
		logger.info("Navigating to Create Campaign page {}",request.getRemoteAddr());
		return "Campaign/CreateCampaign";
	}
	
	@RequestMapping(value = "/campaign/campaignboard", method = RequestMethod.GET)
	public String campaignboard(HttpServletRequest request, Model model) {
		logger.info("Navigating to Campaign Board page {}",request.getRemoteAddr());
		return "Campaign/CreateCampaign";
	}
}

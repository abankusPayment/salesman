package com.protecksoftware.salesman.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.protecksoftware.salesman.application.response.InsuranceAgentResponse;
import com.protecksoftware.salesman.domain.Address;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.services.SalesmanResponse;
import com.protecksoftware.salesman.sales.processor.SalesProcessor;
import com.protecksoftware.salesman.utils.PlatformAbstractUtils;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController extends PlatformAbstractUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private SalesProcessor salesProcessor;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpServletRequest request, Model model) {
		return "redirect:/salesman/index";
	}
	
	@RequestMapping(value = "/salesman/index", method = RequestMethod.GET)
	public String homepage(HttpServletRequest request, Model model) {
		try {
			loadEmployeeIntoSession(request);
		} catch (PlatformException e) {
			logger.error(e.getMessage(), e);
		}
		return "index";
	}
	
	@RequestMapping(value = "/salesman/findAgentLocation", method = RequestMethod.GET)
	public @ResponseBody InsuranceAgentResponse findAgentLocation(HttpServletRequest request, Model model) {
		InsuranceAgentResponse response = null;
		try {
			response = salesProcessor.findAgentLocation(request);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return response;
	}

}

package com.protecksoftware.salesman.application.response;

import java.util.List;

import javax.ws.rs.core.Response.Status;

import com.protecksoftware.salesman.domain.AgentProspectiveInsured;
import com.protecksoftware.salesman.insurance.services.SalesmanResponse;

public class AgentProspectResponse implements SalesmanResponse {

	private String message;
	
	private boolean result;
	
	//TODO - Change this object to AgentProspectiveInsuredDTO
	private AgentProspectiveInsured agentProspect;
	
	private List<AgentProspectiveInsured> agentProspectiveList;
	
	public AgentProspectiveInsured getAgentProspect() {
		return agentProspect;
	}

	public void setAgentProspectiveInsured(AgentProspectiveInsured agentProspect) {
		this.agentProspect = agentProspect;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	@Override
	public Status getResponseStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getResponseCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setResponseStatus(Status responseStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setResponseCode(int responseCode) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}

	@Override
	public boolean isResult() {
		// TODO Auto-generated method stub
		return result;
	}

	public List<AgentProspectiveInsured> getAgentProspectiveList() {
		return agentProspectiveList;
	}

	public void setAgentProspectiveList(List<AgentProspectiveInsured> agentProspectiveList) {
		this.agentProspectiveList = agentProspectiveList;
	}

}

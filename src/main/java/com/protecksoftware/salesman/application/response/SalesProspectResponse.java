package com.protecksoftware.salesman.application.response;

import java.util.List;

import javax.ws.rs.core.Response.Status;

import com.protecksoftware.salesman.domain.ProspectiveInsured;
import com.protecksoftware.salesman.insurance.businessobjects.dto.ProspectiveInsuredDTO;
import com.protecksoftware.salesman.insurance.services.SalesmanResponse;

public class SalesProspectResponse  implements SalesmanResponse{

	private boolean result;
	
	private String message;
	
	private ProspectiveInsured prospectiveInsured;
	
	private List<ProspectiveInsured> prospectiveInsuredList;

	private ProspectiveInsuredDTO prospectiveInsuredDto;
	
	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ProspectiveInsured getProspectiveInsured() {
		return prospectiveInsured;
	}

	public void setProspectiveInsured(ProspectiveInsured prospectiveInsured) {
		this.prospectiveInsured = prospectiveInsured;
	}

	public List<ProspectiveInsured> getProspectiveInsuredList() {
		return prospectiveInsuredList;
	}

	public void setProspectiveInsuredList(List<ProspectiveInsured> prospectiveInsuredList) {
		this.prospectiveInsuredList = prospectiveInsuredList;
	}

	public ProspectiveInsuredDTO getProspectiveInsuredDto() {
		return prospectiveInsuredDto;
	}

	public void setProspectiveInsuredDto(ProspectiveInsuredDTO prospectiveInsuredDto) {
		this.prospectiveInsuredDto = prospectiveInsuredDto;
	}

	@Override
	public Status getResponseStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getResponseCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setResponseStatus(Status responseStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setResponseCode(int responseCode) {
		// TODO Auto-generated method stub
		
	}
	
	

}

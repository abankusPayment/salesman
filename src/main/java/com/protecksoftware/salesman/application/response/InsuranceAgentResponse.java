package com.protecksoftware.salesman.application.response;

import java.util.List;

import javax.ws.rs.core.Response.Status;

import com.protecksoftware.salesman.domain.Address;
import com.protecksoftware.salesman.domain.Email;
import com.protecksoftware.salesman.domain.Phone;
import com.protecksoftware.salesman.insurance.businessobjects.dto.InsuranceAgentDTO;
import com.protecksoftware.salesman.insurance.services.SalesmanResponse;

public class InsuranceAgentResponse  implements SalesmanResponse {

	private List<InsuranceAgentDTO> agentDTO;
	
	private InsuranceAgentDTO agent;
	
	private boolean result;
	
	private String message;
	
	private Address address;
	
	private Phone phone;
	
	private Email emailAddress;
	
	private int responseCode;
	
	private Status responseStatus;
	
	public List<InsuranceAgentDTO> getAgentDTO() {
		return agentDTO;
	}

	public void setAgentDTO(List<InsuranceAgentDTO> agentDTO) {
		this.agentDTO = agentDTO;
	}

	public InsuranceAgentDTO getAgent() {
		return agent;
	}

	public void setAgent(InsuranceAgentDTO agent) {
		this.agent = agent;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public Status getResponseStatus() {
		return responseStatus;
	}

	@Override
	public int getResponseCode() {
		return responseCode;
	}

	@Override
	public void setResponseStatus(Status responseStatus) {
		this.responseStatus = responseStatus;
	}

	@Override
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
		
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public boolean isResult() {
		return result;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public Email getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(Email emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	

}

package com.protecksoftware.salesman.application.response;

import java.util.List;

import javax.ws.rs.core.Response.Status;

import com.protecksoftware.salesman.domain.Employee;
import com.protecksoftware.salesman.insurance.businessobjects.dto.InsuranceAgentDTO;
import com.protecksoftware.salesman.insurance.services.SalesmanResponse;

public class EmployeeResponse implements SalesmanResponse{

	private int employeeId;
	
	private Status responseStatus;
	
	private int responseCode;

	private boolean result;
	
	private String message;
	
	private Employee employee;
	
	private List<Employee> employeeList;
	
	private List<InsuranceAgentDTO> insuranceAgent;
	
	public Status getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(Status responseStatus) {
		this.responseStatus = responseStatus;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
		
	}
	
	public List<Employee> getEmployeeList(){
		return employeeList;
	}

	public List<InsuranceAgentDTO> getInsuranceAgent() {
		return insuranceAgent;
	}

	public void setInsuranceAgent(List<InsuranceAgentDTO> insuranceAgent) {
		this.insuranceAgent = insuranceAgent;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	
}

package com.protecksoftware.salesman.application.response;

import java.util.List;

import javax.ws.rs.core.Response.Status;

import com.protecksoftware.salesman.domain.InsuredState;
import com.protecksoftware.salesman.domain.ProspectiveInsured;
import com.protecksoftware.salesman.insurance.businessobjects.dto.AgentProspectiveInsuredDTO;
import com.protecksoftware.salesman.insurance.businessobjects.dto.ProspectiveInsuredDTO;
import com.protecksoftware.salesman.insurance.services.SalesmanResponse;

public class ProspectiveInsuredResponse implements SalesmanResponse {
	private ProspectiveInsured prospectiveInsured;
	
	private List<ProspectiveInsuredDTO> prospectiveInsuredList;
	
	private List<InsuredState> insuredStateList;
	
	private ProspectiveInsuredDTO prospectiveInsuredDTO;
	
	private AgentProspectiveInsuredDTO agentProspectiveInsuredDTO;
	
	private boolean status;
	
	private String message;

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ProspectiveInsured getProspectiveInsured() {
		return prospectiveInsured;
	}

	public void setProspectiveInsured(ProspectiveInsured prospectiveInsured) {
		this.prospectiveInsured = prospectiveInsured;
	}

	public List<ProspectiveInsuredDTO> getProspectiveInsuredList() {
		return prospectiveInsuredList;
	}

	public void setProspectiveInsuredList(List<ProspectiveInsuredDTO> prospectiveInsuredList) {
		this.prospectiveInsuredList = prospectiveInsuredList;
	}

	@Override
	public Status getResponseStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getResponseCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setResponseStatus(Status responseStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setResponseCode(int responseCode) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}

	@Override
	public boolean isResult() {
		// TODO Auto-generated method stub
		return status;
	}

	@Override
	public void setResult(boolean result) {
		// TODO Auto-generated method stub
		
	}

	public List<InsuredState> getInsuredStateList() {
		return insuredStateList;
	}

	public void setInsuredStateList(List<InsuredState> insuredStateList) {
		this.insuredStateList = insuredStateList;
	}

	public ProspectiveInsuredDTO getProspectiveInsuredDTO() {
		return prospectiveInsuredDTO;
	}

	public void setProspectiveInsuredDTO(ProspectiveInsuredDTO prospectiveInsuredDTO) {
		this.prospectiveInsuredDTO = prospectiveInsuredDTO;
	}

	public AgentProspectiveInsuredDTO getAgentProspectiveInsuredDTO() {
		return agentProspectiveInsuredDTO;
	}

	public void setAgentProspectiveInsuredDTO(AgentProspectiveInsuredDTO agentProspectiveInsuredDTO) {
		this.agentProspectiveInsuredDTO = agentProspectiveInsuredDTO;
	}

}

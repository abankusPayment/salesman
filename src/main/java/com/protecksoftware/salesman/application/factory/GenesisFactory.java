package com.protecksoftware.salesman.application.factory;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.protecksoftware.salesman.application.request.AgentProspectiveRequest;
import com.protecksoftware.salesman.authentication.AuthenticationEmployeeRequest;
import com.protecksoftware.salesman.domain.Location;
import com.protecksoftware.salesman.domain.ProspectiveInsured;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.utils.PlatformUtils;
import com.protecksoftware.salesman.utils.ValidationUtils;

@Component
public class GenesisFactory {
	private static final Logger logger = LoggerFactory.getLogger(GenesisFactory.class.getName());
	private @Value("${googleMapKey}") String googleGeoCodingKey;
	
	public Genesis10 createInstance(String object,HttpServletRequest request){
		Genesis10 genesis10 = null;
		switch(object){
		case "propectiveInsured" : 
			genesis10 = createProspectiveInsuredInstance(request);
			break;
		case "employeeRequest" :
			genesis10 = createEmployeeInstance(request);
			break;
		case "assignProspect" :
			genesis10 = createAssignAgentProspectiveInstance(request);
		default :
			break;
		}
		
		return genesis10;
	}

	private Genesis10 createAssignAgentProspectiveInstance(HttpServletRequest request) {
		AgentProspectiveRequest req = null;
		String agentId = ValidationUtils.encode(request.getParameter("agentId"));
		String prospectiveId = ValidationUtils.encode(request.getParameter("prospectiveId"));
		if(agentId != null && prospectiveId != null){
			
			try{
				int prospective = Integer.parseInt(prospectiveId);
				int agent = Integer.parseInt(agentId);
				req = new AgentProspectiveRequest();
				req.setProspectiveId(prospective);
				req.setAgentId(agent);
			}catch(NumberFormatException e){
				logger.error(e.getMessage(),e);
			}
		}
		
		return req;
	}

	private Genesis10 createEmployeeInstance(HttpServletRequest request) {
		String firstname = ValidationUtils.encode(request.getParameter("firstname"));
		String lastname = ValidationUtils.encode(request.getParameter("lastname"));
		String address1 = ValidationUtils.encode(request.getParameter("address1"));
		String address2 = ValidationUtils.encode(request.getParameter("address2"));
		String city = ValidationUtils.encode(request.getParameter("city"));
		String region = ValidationUtils.encode(request.getParameter("region"));
		String phoneNumber = ValidationUtils.encode(request.getParameter("phoneNumber"));
		String emailAddress = ValidationUtils.encode(request.getParameter("emailAddress"));
		String gender = ValidationUtils.encode(request.getParameter("gender"));
		String middlename = ValidationUtils.encode(request.getParameter("middlename"));
		String roleId = request.getParameter("employeeRole[]");
		//String rolename = ValidationUtils.encode(request.getParameter("employeeRole[]"));
		String position = ValidationUtils.encode(request.getParameter("position"));
		String[] insuranceType = request.getParameterValues("insuranceType");
		String employeeType = position;
		String agentLocation = ValidationUtils.encode(request.getParameter("location"));
		String agentName = ValidationUtils.encode(request.getParameter("agentName"));
		AuthenticationEmployeeRequest employeeRequest = new AuthenticationEmployeeRequest();

				String val = ValidationUtils.encode(roleId);
				int Id = Integer.parseInt(val);
				employeeRequest.setRoleId(Id);
				employeeRequest.setRoleId(1);

		Integer positionId = null;
		if(position != null){
			positionId = (position == "insuranceAgent") ? 2 : 1;
		}
		employeeRequest.setAgentName(agentName);
		employeeRequest.setInsuranceType(insuranceType);
		employeeRequest.setPositionId(positionId);
		employeeRequest.setFirstname(firstname);
		employeeRequest.setEmailAddress(emailAddress);
		employeeRequest.setLastname(lastname);
		employeeRequest.setAddress1(address1);
		employeeRequest.setPhoneNumber(phoneNumber);
		employeeRequest.setAddress2(address2);
		employeeRequest.setCity(city);
		employeeRequest.setEmailAddress(emailAddress);
		employeeRequest.setRegion(region);
		employeeRequest.setGender(gender);
		employeeRequest.setMiddlename(middlename);
		employeeRequest.setRequestType("employee");
		//employeeRequest.setRole(rolename);
		employeeRequest.setGender(gender);
		employeeRequest.setEmployeeType(employeeType);

		if(employeeType.equalsIgnoreCase("insuranceAgent")){
			Location location;
			try {
				location = PlatformUtils.getAgentLocation(region, agentLocation,googleGeoCodingKey);
				employeeRequest.setLocation(location);
			} catch (PlatformException e) {
				logger.error(e.getMessage(),e);
			}
		}

		
		return employeeRequest;
	}



	private ProspectiveInsured createProspectiveInsuredInstance(HttpServletRequest request) {
		String insuranceType = request.getParameter("typeOfInsurance");
		//String address1 = request.getParameter("txtaddress1");
		//String address2 = request.getParameter("txtaddress2");
		String firstname = request.getParameter("txtfirstname");
		String middlename = request.getParameter("txtmiddlename");
		String lastname = request.getParameter("txtlastname");

		ProspectiveInsured prospect = new ProspectiveInsured();
			prospect.setInsuranceType(ValidationUtils.encode(insuranceType));
			prospect.setFirstname(ValidationUtils.encode(firstname));
			prospect.setMiddlename(ValidationUtils.encode(middlename));
			prospect.setLastname(ValidationUtils.encode(lastname));
			prospect.setProspectiveUniqueId(PlatformUtils.getProspectiveUniqueId());
		return prospect;
	}


}

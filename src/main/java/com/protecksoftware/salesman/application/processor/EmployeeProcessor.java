package com.protecksoftware.salesman.application.processor;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.protecksoftware.salesman.application.factory.Genesis10;
import com.protecksoftware.salesman.application.factory.GenesisFactory;
import com.protecksoftware.salesman.application.request.EmailMessageRequest;
import com.protecksoftware.salesman.application.request.EmployeeNoteRequest;
import com.protecksoftware.salesman.application.response.EmployeeResponse;
import com.protecksoftware.salesman.application.response.ProspectiveInsuredResponse;
import com.protecksoftware.salesman.authentication.AuthenticationEmployeeRequest;
import com.protecksoftware.salesman.domain.AgentGeoLocation;
import com.protecksoftware.salesman.domain.Employee;
import com.protecksoftware.salesman.domain.InsuranceAgent;
import com.protecksoftware.salesman.domain.Location;
import com.protecksoftware.salesman.domain.SalesRep;
import com.protecksoftware.salesman.exception.PlatformException;
import com.protecksoftware.salesman.insurance.services.EmployeeService;
import com.protecksoftware.salesman.insurance.services.Processor;
import com.protecksoftware.salesman.utils.PlatformAbstractUtils;
import com.protecksoftware.salesman.utils.PlatformUtils;
import com.protecksoftware.salesman.utils.RegionUtils;
import com.protecksoftware.salesman.utils.ValidationUtils;

@Component
public class EmployeeProcessor extends PlatformAbstractUtils implements Processor{
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeeProcessor.class.getName());
	
	@Autowired
	private GenesisFactory genesisfactory;
	
	@Autowired
	private EmployeeService employeeServiceImpl;
	
	private @Value("${googleMapKey}") String googleGeoCodingKey;
	@Override
	public EmployeeResponse process(String action,HttpServletRequest request)throws PlatformException {
		EmployeeResponse response = null;
		switch(action){
		case "save":
			response = newEmployee(request);
			break;
		case "getAllEmployee":
			response = findAllEmpployee();
			break;	
		case "findById":
			response = findEmpployeeById(request);
			break;	
		case "findAllEmployeeByPosition":
			response = findAllEmpployeeByPosition(request);
		}
		return response;
	}

	private EmployeeResponse findAllEmpployee() {
		EmployeeResponse response = null;
		List<Employee> employeeList = null;
		employeeList = employeeServiceImpl.getAll();
		if(employeeList != null && !employeeList.isEmpty()){
			response = new EmployeeResponse();
			response.setResult(true);
			response.setEmployeeList(employeeList);
		}else{
			response = new EmployeeResponse();
			response.setResult(false);
			response.setMessage("No Employee was found");
		}
		return response;
	}

	private EmployeeResponse findEmpployeeById(HttpServletRequest request) {
		String empId = request.getParameter("employeeId");
		Integer employeeId = Integer.parseInt(empId);
		EmployeeResponse response = null;
		response = employeeServiceImpl.findBy(employeeId);
		if(response != null){
			response.setResult(true);
		}else{
			response = new EmployeeResponse();
			response.setResult(false);
			response.setMessage("No Employee was found");
		}
		
		return response;
	}

	private EmployeeResponse findAllEmpployee(HttpServletRequest request) {
		String searchType = request.getParameter("searchType");
		String searchParam = request.getParameter("searchParam");
		EmployeeResponse response = null;
		response = employeeServiceImpl.findAllEmployeeByPosition(searchType,searchParam);
		if(response != null){
			response.setResult(true);
		}else{
			response = new EmployeeResponse();
			response.setResult(false);
			response.setMessage("No Employee was found");
		}
		
		return response;
	}
	
	private EmployeeResponse findAllEmpployeeByPosition(HttpServletRequest request) {
		String position = request.getParameter("position");
		EmployeeResponse response = null;
		response = employeeServiceImpl.findAllEmployeeByPosition(position);
		if(response != null){
			response.setResult(true);
		}else{
			response = new EmployeeResponse();
			response.setResult(false);
			response.setMessage("No Employee was found");
		}
		
		return response;
	}

	private EmployeeResponse newEmployee(HttpServletRequest request) {
		AuthenticationEmployeeRequest employeeRequest = (AuthenticationEmployeeRequest) genesisfactory.createInstance("employeeRequest", request);
		EmployeeResponse response = null;
		if(employeeRequest != null){
			
			try{
				Employee employee = getEmployeeInSession(request);
				
				if(employee != null){
					int companyId = employee.getCompanyId();
					employeeRequest.setCompanyId(companyId);
					logger.info("Started sending Employee to Kejetia Service to save.");
					response = employeeServiceImpl.save(employeeRequest);
					logger.info("Completed saving new Employee Information. The status of the operation is: "+response.isResult());
					if(!response.isResult()){
						logger.info("Completed saving new Employee Information. The status of the operation is: "+response.isResult()+" and message associate with it is: "+response.getMessage());
						return response;
					}
				}
				
			}catch(PlatformException e){
				logger.error(e.getMessage(),e);
			}
		}
		return response;
	}


	public EmployeeResponse processMessageFromEmployee(HttpServletRequest request) {
		EmployeeResponse employeeResponse = null;
		String messageCategory = ValidationUtils.encode(request.getParameter("messageCategory"));
		switch(messageCategory){
		case "sendCustomerSMS":
			employeeResponse = sendCustomerSMS(request);
			break;
		case "sendCustomerEmail":
			employeeResponse = sendCustomerEmailMessage(request);
			break;
		case "sendEmployeeNotification":
			employeeResponse = sendEmployeeNotification(request);
			break;			
		default:
			break;
		}
		return employeeResponse;
		
	}

	public ProspectiveInsuredResponse addNotesToProspectiveCustomer(HttpServletRequest request) {
		ProspectiveInsuredResponse prospectiveInsuredResponse = new ProspectiveInsuredResponse();
		try{
			String notes = ValidationUtils.encode(request.getParameter("notes"));
			String prospective = ValidationUtils.encode(request.getParameter("prospectiveId"));
			EmployeeNoteRequest noteRequest = new EmployeeNoteRequest();
			if(prospective != null){
				int prospectiveId = Integer.parseInt(prospective);
				noteRequest.setProspectiveId(prospectiveId);
			}
			Employee currentEmployee = getEmployeeInSession(request);
			int employeeId = currentEmployee.getEmployeeId();
			String employeeNumber = currentEmployee.getEmployeeNumber();
			
			noteRequest.setEmployeeId(employeeId);
			noteRequest.setEmployeeNumber(employeeNumber);
			noteRequest.setMessage(notes);
			
			prospectiveInsuredResponse = employeeServiceImpl.addNotesToProspectiveCustomer(noteRequest);
		}catch(PlatformException e){
			logger.error(e.getMessage(), e);
		}catch(NumberFormatException e){
			logger.error(e.getMessage(), e);
		}
		return prospectiveInsuredResponse;
	}

	private EmployeeResponse sendEmployeeNotification(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	private EmployeeResponse sendCustomerEmailMessage(HttpServletRequest request) {
		EmailMessageRequest emailRequest = new EmailMessageRequest();

		try {
			String[] emailAddress = request.getParameterValues("receipientEmailAddress");
			String subject = ValidationUtils.encode(request.getParameter("emailSubject"));
			String message = ValidationUtils.encode(request.getParameter("emailMessage"));
			String employeeNumber =  getEmployeeInSession(request).getEmployeeNumber();
			String employeeName = getEmployeeInSession(request).toString();
			emailRequest.setEmailAddress(emailAddress);
			emailRequest.setEmployeeFromNumber(employeeNumber);
			emailRequest.setMessage(message);
			emailRequest.setEmployeeFromName(employeeName);
			emailRequest.setSubject(subject);
			emailRequest.setDateCreated(LocalDateTime.now());
			employeeServiceImpl.sendEmailRequest(emailRequest);
		} catch (PlatformException e) {
			logger.error(e.getMessage(), e);
		}
		
		return null;
	}

	private EmployeeResponse sendCustomerSMS(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

}

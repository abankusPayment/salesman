package com.protecksoftware.salesman.application.request;


import com.protecksoftware.salesman.domain.Address;
import com.protecksoftware.salesman.domain.Email;
import com.protecksoftware.salesman.domain.InsuredState;
import com.protecksoftware.salesman.domain.Phone;
import com.protecksoftware.salesman.domain.ProspectiveInsured;

public class ProspectiveInsuredRequest {
	private int prospectiveId;
	
	private String comments;

	private String dateEnquired;

	private String firstname;

	private String insuranceType;

	private String lastname;

	private String middlename;

	private String prospectiveUniqueId;
	
	private String currentState;
	
	private Address addresses;

	private Email email;

	private Phone phone;
	
	private InsuredState insuredState;
	
	private int employeeId;
	
	private boolean newInsured;
	
	private Integer agentId;
	
	private boolean addInsuredState;
	
	private int companyId;

	private boolean assignCustomerToEmployee;
	
	public int getProspectiveId() {
		return prospectiveId;
	}

	public void setProspectiveId(int prospectiveId) {
		this.prospectiveId = prospectiveId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDateEnquired() {
		return dateEnquired;
	}

	public void setDateEnquired(String dateEnquired) {
		this.dateEnquired = dateEnquired;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getProspectiveUniqueId() {
		return prospectiveUniqueId;
	}

	public void setProspectiveUniqueId(String prospectiveUniqueId) {
		this.prospectiveUniqueId = prospectiveUniqueId;
	}

	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public Address getAddresses() {
		return addresses;
	}

	public void setAddresses(Address addresses) {
		this.addresses = addresses;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public InsuredState getInsuredState() {
		return insuredState;
	}

	public void setInsuredState(InsuredState insuredState) {
		this.insuredState = insuredState;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public ProspectiveInsuredRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ProspectiveInsuredRequest(ProspectiveInsured prospectiveInsured) {
		
	}
	
	public ProspectiveInsuredRequest(ProspectiveInsured prospect, InsuredState insuredState, Address address,
			Email emailAddress, Phone phoneNumber) {
		this.currentState = prospect.getCurrentState();
		this.comments = prospect.getComments();
		this.dateEnquired = prospect.getDateEnquired();
		this.firstname = prospect.getFirstname();
		this.middlename = prospect.getMiddlename();
		this.lastname = prospect.getLastname();
		this.insuranceType = prospect.getInsuranceType();
		this.prospectiveUniqueId = prospect.getProspectiveUniqueId();
		this.phone = phoneNumber;
		this.email = emailAddress;
		this.addresses = address;
		this.insuredState = insuredState;
		this.employeeId = prospect.getEmployee().getEmployeeId();
		this.companyId = prospect.getEmployee().getCompanyId();
	}

	public ProspectiveInsured getProspectiveInsured(){
		ProspectiveInsured prospectiveInsured = new ProspectiveInsured();
		prospectiveInsured.setAddresses(addresses);
		prospectiveInsured.addEmail(email);
		prospectiveInsured.addPhone(phone);
		prospectiveInsured.setFirstname(firstname);
		prospectiveInsured.setInsuranceType(insuranceType);
		prospectiveInsured.setLastname(lastname);
		prospectiveInsured.setMiddlename(middlename);
		prospectiveInsured.setProspectiveUniqueId(prospectiveUniqueId);
		prospectiveInsured.setComments(comments);
		return prospectiveInsured;
	}

	public boolean isNewInsured() {
		return newInsured;
	}

	public void setNewInsured(boolean newInsured) {
		this.newInsured = newInsured;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public boolean isAddInsuredState() {
		return addInsuredState;
	}

	public void setAddInsuredState(boolean addInsuredState) {
		this.addInsuredState = addInsuredState;
	}

	public int getCompanId() {
		return companyId;
	}

	public void setCompanId(int companId) {
		this.companyId = companId;
	}

	public boolean isAssignCustomerToEmployee() {
		return assignCustomerToEmployee;
	}

	public void setAssignCustomerToEmployee(boolean assignCustomerToEmployee) {
		this.assignCustomerToEmployee = assignCustomerToEmployee;
	}
	
}

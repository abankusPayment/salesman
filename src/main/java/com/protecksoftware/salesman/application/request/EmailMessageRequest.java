package com.protecksoftware.salesman.application.request;

import java.time.LocalDateTime;

public class EmailMessageRequest {
	private String[] emailAddress;
	
	private String employeeFromName;
	
	private String employeeFromNumber;
	
	private String employeeToNumber;
	
	private String[] prospectiveUniqueId;
	
	private String message;
	
	private String subject;
	
	private LocalDateTime dateCreated;
	
	private LocalDateTime dateSent;

	public String[] getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String[] emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmployeeFromName() {
		return employeeFromName;
	}

	public void setEmployeeFromName(String employeeFromName) {
		this.employeeFromName = employeeFromName;
	}

	public String getEmployeeFromNumber() {
		return employeeFromNumber;
	}

	public void setEmployeeFromNumber(String employeeFromNumber) {
		this.employeeFromNumber = employeeFromNumber;
	}

	public String getEmployeeToNumber() {
		return employeeToNumber;
	}

	public void setEmployeeToNumber(String employeeToNumber) {
		this.employeeToNumber = employeeToNumber;
	}

	public String[] getProspectiveUniqueId() {
		return prospectiveUniqueId;
	}

	public void setProspectiveUniqueId(String[] prospectiveUniqueId) {
		this.prospectiveUniqueId = prospectiveUniqueId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public LocalDateTime getDateSent() {
		return dateSent;
	}

	public void setDateSent(LocalDateTime dateSent) {
		this.dateSent = dateSent;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	
}

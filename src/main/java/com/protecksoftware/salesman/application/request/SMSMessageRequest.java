package com.protecksoftware.salesman.application.request;

import java.time.LocalDateTime;

public class SMSMessageRequest {
	private String[] receipentPhoneNumber;
	
	private String employeeFromName;
	
	private String employeeFromNumber;
	
	private String employeeToNumber;
	
	private String[] prospectiveUniqueId;
	
	private String message;
	
	private LocalDateTime dateCreated;
	
	private LocalDateTime dateSent;

	public String[] getReceipentPhoneNumber() {
		return receipentPhoneNumber;
	}

	public void setReceipentPhoneNumber(String[] receipentPhoneNumber) {
		this.receipentPhoneNumber = receipentPhoneNumber;
	}

	public String getEmployeeFromName() {
		return employeeFromName;
	}

	public void setEmployeeFromName(String employeeFromName) {
		this.employeeFromName = employeeFromName;
	}

	public String getEmployeeFromNumber() {
		return employeeFromNumber;
	}

	public void setEmployeeFromNumber(String employeeFromNumber) {
		this.employeeFromNumber = employeeFromNumber;
	}

	public String getEmployeeToNumber() {
		return employeeToNumber;
	}

	public void setEmployeeToNumber(String employeeToNumber) {
		this.employeeToNumber = employeeToNumber;
	}

	public String[] getProspectiveUniqueId() {
		return prospectiveUniqueId;
	}

	public void setProspectiveUniqueId(String[] prospectiveUniqueId) {
		this.prospectiveUniqueId = prospectiveUniqueId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public LocalDateTime getDateSent() {
		return dateSent;
	}

	public void setDateSent(LocalDateTime dateSent) {
		this.dateSent = dateSent;
	}
	
	

}

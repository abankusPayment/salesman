package com.protecksoftware.salesman.application.request;

import com.protecksoftware.salesman.domain.Employee;

public class AdvanceSearchRequest {

	private final int employeeId;
	
	private final String firstname;
	
	private final String lastname;
	
	private final String middlename;
	
	private final String city;
	
	private final String businessName;
	
	private final String region;
	
	private final String emailAddress;
	
	private final String phoneNumber;

	public AdvanceSearchRequest(Employee emp, String firstname, String lastname, String middlename, String city, String businessName,
			String region, String emailAddress, String phoneNumber) {
		super();
		this.employeeId = emp.getEmployeeId();
		this.firstname = firstname;
		this.lastname = lastname;
		this.middlename = middlename;
		this.city = city;
		this.businessName = businessName;
		this.region = region;
		this.emailAddress = emailAddress;
		this.phoneNumber = phoneNumber;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public String getCity() {
		return city;
	}

	public String getBusinessName() {
		return businessName;
	}

	public String getRegion() {
		return region;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	
}

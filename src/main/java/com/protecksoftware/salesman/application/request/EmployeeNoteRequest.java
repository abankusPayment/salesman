package com.protecksoftware.salesman.application.request;

import java.time.LocalDateTime;

public class EmployeeNoteRequest {
	
	private int employeeId;
	
	private String employeeNumber;
	
	private int prospectiveId;
	
	private String message;
	
	private LocalDateTime dateCreated;
	
	private LocalDateTime dateSent;

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public LocalDateTime getDateSent() {
		return dateSent;
	}

	public void setDateSent(LocalDateTime dateSent) {
		this.dateSent = dateSent;
	}

	public int getProspectiveId() {
		return prospectiveId;
	}

	public void setProspectiveId(int prospectiveId) {
		this.prospectiveId = prospectiveId;
	}
	
	
}

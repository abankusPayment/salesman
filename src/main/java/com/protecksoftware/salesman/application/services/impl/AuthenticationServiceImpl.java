package com.protecksoftware.salesman.application.services.impl;

import java.io.IOException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.protecksoftware.salesman.authentication.AuthenticationActivateEmployee;
import com.protecksoftware.salesman.authentication.AuthenticationChangePasswordResponse;
import com.protecksoftware.salesman.insurance.services.AuthenticationService;
import com.protecksoftware.salesman.utils.PlatformUtils;
import com.protecksoftware.salesman.utils.SecurityUtils;

public class AuthenticationServiceImpl implements AuthenticationService {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);;
	
	private @Value("${auth.activateEmployeeRequestURL}") String activateEmployeeRequestURL;
	
	private @Value("${auth.validatePasswordToken}") String validatePasswordToken;
	
	private @Value("${auth.validateEmployeeRequestURL}") String validateEmployeeRequestURL;
	
	private @Value("${auth.doPhoneNumberExist}") String doPhoneNumberExist;
	
	@Override
	public boolean processActivateEmployeeRequest(String username, String passwd, String tempPasswd,String requestType,String token) {
		logger.info("Beginning the process of activating Employee Account with new Username: "+username+" and password");
		AuthenticationActivateEmployee request = new AuthenticationActivateEmployee();
		request.setTempPassword(tempPasswd);
		request.setUsername(username);
		request.setHashPassword(SecurityUtils.generatePasswordHash(passwd));
		request.setRequestType(requestType);
		request.setAccountEnbaled(true);
		request.setToken(token);
		AuthenticationChangePasswordResponse authenticationResponse = null;
		boolean result = false;
		String authenticationResult = null;
		try{
			Response response = PlatformUtils.getRESTClientInstance().target(activateEmployeeRequestURL)
								.request()
								.accept(MediaType.APPLICATION_JSON)
								.post(Entity.entity(PlatformUtils.convertToJSON(request), MediaType.APPLICATION_JSON));
			if(response.getStatus() == 200){
				authenticationResponse = new AuthenticationChangePasswordResponse();
				authenticationResult = response.readEntity(String.class);
				authenticationResponse = PlatformUtils.convertFromJSON(AuthenticationChangePasswordResponse.class, authenticationResult);
				result = authenticationResponse.isResult();
				logger.info(authenticationResponse.getMessage());
			}
			
		}catch(IOException e){
			logger.error("Error occured while converting Authentication Service response from JSON to Boolean",e);
		}finally{
			request = null;
			authenticationResponse = null;
		}
		return result;
	}

	@Override
	public boolean validateEmployeeToken(String token) {
		boolean isValid = false;
		try{
			Response  response = PlatformUtils.getRESTClientInstance()
					.target(validateEmployeeRequestURL)
					.request()
					.post(Entity.entity(token, MediaType.APPLICATION_JSON));
				if(response.getStatus() == 200){
					String str= response.readEntity(String.class);
					isValid = PlatformUtils.convertFromJSON(Boolean.class,str);
				}
			}catch(Exception e){
				logger.error("Error occured while converting Authentication Service response from JSON to Boolean",e);
			}	
		return isValid;
	}
	
	@Override
	public boolean validatePasswordToken(String token) {
		boolean isValid = false;
		String authenticationResult = null;
		try{
			Response  response = PlatformUtils.getRESTClientInstance().target(validatePasswordToken)
					.request()
					.post(Entity.entity(token, MediaType.APPLICATION_JSON));
				if(response.getStatus() == 200){
					authenticationResult = response.readEntity(String.class);
					isValid= PlatformUtils.convertFromJSON(Boolean.class,authenticationResult);
				}
			}catch(Exception e){
				logger.error("Error occured while converting Authentication Service response from JSON to Boolean",e);
			}finally{
				authenticationResult = null;			
			}		
		return isValid;
	}


	
	@Override
	public boolean phoneNumberUnique(String phoneNumber) {
		boolean result = false;
		String authenticationResult = null;
		try{
			Response response = PlatformUtils.getRESTClientInstance()
					.target(doPhoneNumberExist)
					.queryParam("phoneNumber", phoneNumber)
					.request(MediaType.APPLICATION_JSON)
					.get();
			if(response.getStatus() == 200){
				authenticationResult = response.readEntity(String.class);
				result = PlatformUtils.convertFromJSON(Boolean.class, authenticationResult);
			}
		}catch(IOException e){
			logger.error("Error occured while converting Authentication Service response from JSON to Boolean",e);
		}
		return result;
	}

	@Override
	public boolean processForgotPasswordRequest(String confirmationCode, String requestType, String newPasswd,
			String passwordToken) {
		// TODO Auto-generated method stub
		return false;
	}
}

package com.protecksoftware.salesman.application.services.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Future;

import javax.sql.DataSource;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.protecksoftware.salesman.application.request.EmailMessageRequest;
import com.protecksoftware.salesman.application.request.EmployeeNoteRequest;
import com.protecksoftware.salesman.application.request.EmployeeRequest;
import com.protecksoftware.salesman.application.response.EmployeeResponse;
import com.protecksoftware.salesman.application.response.ProspectiveInsuredResponse;
import com.protecksoftware.salesman.authentication.AuthenticationEmployeeRequest;
import com.protecksoftware.salesman.authentication.AuthenticationResponse;
import com.protecksoftware.salesman.domain.Employee;
import com.protecksoftware.salesman.insurance.businessobjects.dto.EmployeeDTO;
import com.protecksoftware.salesman.insurance.services.EmployeeService;
import com.protecksoftware.salesman.utils.PlatformUtils;

@Component
public class EmployeeServiceImpl implements EmployeeService {
	private static final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class.getName());
	private @Value("${waitTimeNumber}") String waitTimeNumber;
	
	private @Value("${employeeSaveUrl}") String employeeSaveUrl;
	
	private @Value("${getAllEmployeeUrl}") String getAllEmployeeUrl;
	
	private @Value("${getAllEmployeeByPositionUrl}") String getAllEmployeeByPositionUrl;
	
	private @Value("${findEmployeeByIdUrl}") String findEmployeeByIdUrl;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	public EmployeeResponse save(EmployeeRequest employeeRequest) {
		
		EmployeeResponse employeeResponse = null;
		AuthenticationResponse authenticationResponse= null;
		try {
			Client client = PlatformUtils.getRESTClientInstance();
			String entity = PlatformUtils.convertToJSON(employeeRequest);
			logger.info("Sending request to ProSalesman to save new Employee");
			Response response = client.target(employeeSaveUrl)
					.request()
					.post(Entity.entity(entity, MediaType.APPLICATION_JSON), Response.class);
			logger.info("Received response from ProSalesman- Status: "+response.toString());
			if(response.getStatus() == 200){
					String strResponse =  response.readEntity(String.class);
					authenticationResponse = PlatformUtils.convertFromJSON(AuthenticationResponse.class, strResponse);
					employeeResponse = findBy((int)authenticationResponse.getEmployeeId());
			}
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}			
		return employeeResponse;
	}

	@Override
	public List<Employee> findAllBy(String employeeNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employee> getAll() {
		logger.info("Sending request to get all employee information..");
		Client client = PlatformUtils.getRESTClientInstance();
		List<Employee> employeeList = null;
		try {
	
			Response response = client.target(getAllEmployeeUrl)
					.request()
					.accept(MediaType.APPLICATION_JSON_TYPE,MediaType.APPLICATION_XML_TYPE)
					.get();
			if(response.getStatus() == 200){
					String strResponse =  response.readEntity(String.class);
					employeeList = PlatformUtils.convertFromJson(new TypeReference<List<Employee>>() {}, strResponse);
	
			}
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}finally{
			logger.info("Completed processing request for all employee information....");
		}
		return employeeList;
	}

	@Override
	public EmployeeResponse update(EmployeeRequest s) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmployeeResponse findBy(int employeeId) {
		EmployeeResponse employeeResponse = null;
		Session session = getSessionFactory().openSession();
		session.beginTransaction().begin();
		Employee emp = session.get(Employee.class, employeeId);
		if(emp != null){
			employeeResponse = new EmployeeResponse();
			employeeResponse.setEmployee(emp);
			employeeResponse.setResult(true);
		}

		session.close();
		return employeeResponse;
	}

	@Override
	public List<Employee> search(String... str) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmployeeResponse findAllEmployeeByPosition(String searchType,String uniqueId) {
		Client client = PlatformUtils.getRESTClientInstance();
		EmployeeResponse employeeResponse = null;
		Response response = client.target(getAllEmployeeByPositionUrl)
				.queryParam("searchType", searchType)
				.queryParam("uniqueId", uniqueId)
				.request()
				.get(Response.class);
		if(response.getStatus() == 200){
				employeeResponse = (EmployeeResponse) response.getEntity();
				employeeResponse.setResponseCode(response.getStatus());
				employeeResponse.setResult(true);
		}else{
			employeeResponse = new EmployeeResponse();
			employeeResponse.setResult(false);
			employeeResponse.setResponseCode(response.getStatus());		
		}
		return employeeResponse;
	}
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public EmployeeDTO findByEmployeeId(int employeeId) {
		
		EmployeeDTO empDetails = null;
		/**
		try{
		EmployeeResponse employeeResponse = null;
		Session session = getSessionFactory().openSession();
		empDetails = (EmploymentDetail) session.createQuery("From Employee e where e.employee.employeeId =:employeeId")
				.setParameter("employeeId", employeeId)
				.getSingleResult();

		/**
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		EmploymentDetail empDetails = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select e.*,ed.*,p.* from employmentDetails ed JOIN employee e ON ed.employeeId=e.employeeId JOIN position p ON ed.positionId=p.positionId where ed.employeeId =?");
				ps.setInt(1, employeeId);
			rs = ps.executeQuery();
			if(rs.next()){
				Position p = new Position();
				empDetails = new EmploymentDetail();
				Employee employee = new Employee();
				p.setDetails(rs.getString("details"));
				p.setPositionId(rs.getInt("positionId"));
				p.setPositionName(rs.getString("positionName"));
				
				empDetails.setPosition(p);
				empDetails.setEmploymentDetailsId(rs.getInt("employmentDetailsId"));
				empDetails.setDutySummary(rs.getString("dutySummary"));
				
				employee.setAddress1(rs.getString("address1"));
				employee.setAddress2(rs.getString("address2"));
				
				empDetails.setEmployee(employee);
			}
			
		}catch(HibernateException e){
			logger.error(e.getMessage(),e);
		}**/
		return empDetails;
	}
	/**
	@Override
	@Transactional
	public EmployeeDTO saveEmploymentDetails(Employee employee,String employeeType) {

		if(employeeType.equalsIgnoreCase("salesRep")){
			
		}else if(employeeType.equalsIgnoreCase("insuranceAgent")){
			
		}else{
			
		}
		Session session = getSessionFactory().openSession();

		Connection conn = null;
		PreparedStatement ps = null;
		try{
			
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into insuranceAgent (agentId,agentNumber,agentName,employeeId,locationId) values (NULL,?,?,?,?)");
			ps.setString(1, PlatformUtils.generateEmployeeNumber());
			ps.setString(2, employmentDetail.toString());
			ps.setInt(3, employmentDetail.getEmployeeId());
			ps.setInt(4, positionId);
			ps.execute();
			int key = ps.getGeneratedKeys().getInt(0);
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
			
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			try {
				ps.close();
				conn.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(),e);
			}
		}
		return employmentDetail;
	}

	public EmploymentDetail getEmploymentDetailById(Serializable e){
		EmploymentDetail employmentDetail = null;
		if(e != null){
			int employmentDetailId = Integer.parseInt( e.toString());
			employmentDetail = getEmploymentDetailById(employmentDetailId);
		}
		return employmentDetail;
	}
	@Transactional
	public EmploymentDetail getEmploymentDetailById(Integer employmentDetailId){
		EmploymentDetail employmentDetail = null;
		if(employmentDetailId > 0){
			Session session = getSessionFactory().openSession();
			employmentDetail = session.get(EmploymentDetail.class, employmentDetailId);
		}
		return employmentDetail;
	}
**/
	@Override
	public EmployeeResponse findAllEmployeeByPosition(String position) {
		Client client = PlatformUtils.getRESTClientInstance();
		EmployeeResponse employeeResponse = null;
		Response response = client.target(getAllEmployeeByPositionUrl)
				.queryParam("position", position)
				.request()
				.get();
		try{
			if(response.getStatus() == 200){
				String strResponse = response.readEntity(String.class);
				logger.info(strResponse);
				employeeResponse = PlatformUtils.convertFromJSON(EmployeeResponse.class, strResponse);
				employeeResponse.setResponseCode(response.getStatus());
				employeeResponse.setMessage(response.getStatusInfo().getReasonPhrase());
				employeeResponse.setResult(true);
			}else{
				employeeResponse = new EmployeeResponse();
				employeeResponse.setResult(false);
				employeeResponse.setResponseCode(response.getStatus());
				employeeResponse.setMessage(response.getStatusInfo().getReasonPhrase());			
			}
		} catch (JsonParseException e) {
			logger.error(e.getMessage(),e);
		} catch (JsonMappingException e) {
			logger.error(e.getMessage(),e);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}finally{
			
		}
		return employeeResponse;
	}

	@Override
	public EmployeeResponse save(AuthenticationEmployeeRequest employeeRequest) {
		Client client = PlatformUtils.getRESTClientInstance();
		EmployeeResponse employeeResponse = null;
		try {
			String entity = PlatformUtils.convertToJSON(employeeRequest);
			logger.info("Sending request to ProSalesman to save new Employee");
			Response response = client.target(employeeSaveUrl)
					.request()
					.post(Entity.entity(entity, MediaType.APPLICATION_JSON), Response.class);
			logger.info("Received response from ProSalesman- Status: "+response.toString());
			if(response.getStatus() == 200){
					String strResponse =  response.readEntity(String.class);
					employeeResponse = PlatformUtils.convertFromJSON(EmployeeResponse.class, strResponse);	
					
			}else{
				employeeResponse = new EmployeeResponse();
				employeeResponse.setResult(false);
			}
			if(employeeResponse.isResult()){
				
			}else{
				
			}
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}			
		return employeeResponse;
	}

	@Override
	public EmployeeDTO saveEmployeeDetails(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public EmployeeDTO saveEmployeeDetails(Employee employee, String employeeType) {
		EmployeeDTO employeeDTO = null;
		String employeeNumber = null;
		String sql = null;
		if(employeeType.equalsIgnoreCase("salesRep")){
			sql = "insert into salesRep (salesRepId,salesRepNumber,employeeId) values (NULL,?,?,?,?)";
			employeeNumber = PlatformUtils.generateSalesRepNumber();
		}else if(employeeType.equalsIgnoreCase("insuranceAgent")){
			sql = "insert into insuranceAgent (agentId,agentNumber,agentName,employeeId,locationId) values (NULL,?,?,?,?)";
			employeeNumber = PlatformUtils.generateEmployeeNumber();
		}else{
			employeeNumber = PlatformUtils.generateEmployeeNumber();
		}
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, employeeNumber);
			ps.setInt(3, employee.getEmployeeId());
			if(employeeType.equalsIgnoreCase("insuranceAgent")){
				ps.setString(2, employee.toString());
				ps.setInt(4, employee.getPosition().getPositionId());
			}
			ps.execute();
		}catch(SQLException e){
			logger.error(e.getMessage(),e);
			
		}catch(Exception e){
			logger.error(e.getMessage(),e);
		}finally{
			try {
				ps.close();
				conn.close();
			} catch (SQLException e) {
				logger.error(e.getMessage(),e);
			}
		}
		
		return employeeDTO;
	}

	@Override
	public void sendEmailRequest(EmailMessageRequest emailRequest) {
		if(emailRequest != null){
			
		}
	}

	@Override
	public ProspectiveInsuredResponse addNotesToProspectiveCustomer(EmployeeNoteRequest noteRequest) {
		Client client = PlatformUtils.getRESTClientInstance();
		
		ProspectiveInsuredResponse prospectiveInsuredResponse = null;
		try{
			Future<Response> response = client.target("").request()
			.async()
			.post(Entity.entity(PlatformUtils.convertToJSON(noteRequest), MediaType.APPLICATION_JSON_TYPE));
			if(response.isDone()){
				
			}
		}catch(Exception e){
			logger.info(e.getMessage());
		}
		return prospectiveInsuredResponse;
	}
	
}

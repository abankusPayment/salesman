<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
	<jsp:include page="../head.jsp"/>
</head>

<body  class="top-navigation">

<div id="wrapper">
	<div id="header" class="page-wrapper">
		<jsp:include page="../header.jsp"/>
 			<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2>Create New Team</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<c:url value="/" />" >Home</a>
                        </li>
                        <li class="active">
                            <strong>Team</strong>
                        </li>
                    </ol>
                </div>
            </div>
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="wrapper wrapper-content">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h4>New Team</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-7">
                                <form role="form">
                                    <div class="form-group">
                                    	<label>Team name</label> <input type="text" name="teamname" id="teamname" placeholder="Team name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                    	<label>Team Description</label>
                                    	<textarea class="form-control" name="description"  id="description" rows="10"></textarea>
                                    </div>
                                    <div>
                                        <button class="btn btn-md btn-primary m-t-n-xs" type="submit"><strong>Save</strong></button>
                                        <a href="javascript:;" class=" pull-right">Cancel</a>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-5">
								<div class="well">
									Use this form to create Teams for your Sales Team
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                	</div>
            	</div>
			</div>           
        </div>
        <div class="footer-contaner">
			<jsp:include page="../footer.jsp"/>
        </div>

    </div>


</body>

</html>
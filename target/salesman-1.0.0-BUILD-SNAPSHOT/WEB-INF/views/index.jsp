<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>${companyInstance.getCompanyName() } - Sales Partner</title>

    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/font-awesome/css/font-awesome.css"/>" rel="stylesheet">

    <link href="<c:url value="/resources/css/animate.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">

</head>

<body class="">

<div id="wrapper">
	<div id="header">
		<jsp:include page="header.jsp"/>
	</div>
<div id="page-wrapper" class="gray-bg">
	        <div class="row border-bottom">
	            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
	                <div class="navbar-header">
	                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
	                    <form role="search" class="navbar-form-custom" method="post" action="#">
	                        <div class="form-group">
	                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
	                        </div>
	                    </form>
	                </div>
	                <ul class="nav navbar-top-links navbar-right">
	                    <li>
	                        <a href="#">
	                            <i class="fa fa-sign-out"></i> Log out
	                        </a>
	                    </li>
	                </ul>
	
	            </nav>
	        </div>
	       	
 			<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<c:url value="/" />" >Home</a>
                        </li>
                    </ol>
                </div>
            </div>
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="wrapper wrapper-content">
			            <div class="col-md-12">
			
			            </div>
                	</div>
            	</div>
			</div>           
        </div>
        <div class="footer-contaner">
			<jsp:include page="footer.jsp"/>
        </div>
</div>
<!-- Mainly scripts -->
<script src="<c:url value="/resources/js/jquery-2.1.1.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/resources/js/plugins/metisMenu/jquery.metisMenu.js"/>"></script>
<script src="<c:url value="/resources/js/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>

<!-- Custom and plugin javascript -->
<script src="<c:url value="/resources/js/inspinia.js"/>"></script>
<script src="<c:url value="/resources/js/plugins/pace/pace.min.js"/>"></script>


</body>

</html>